/*
 * RetypeParameterParser.h
 *
 *  Created on: 6 Jan 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEPARAMETERPARSER_H_
#define SRC_RETYPEPARAMETERPARSER_H_

namespace retyper {

#include "rose.h"
#include <boost/filesystem.hpp>

class RetypeParameterParser {
public:
	RetypeParameterParser();
	virtual ~RetypeParameterParser();

	static bool path_contain_subpath(boost::filesystem::path projectPath,
			boost::filesystem::path subpath);
	Rose_STL_Container<std::string> parseRetypeParameters(int argc,
			char ** argv, std::string *initialType, std::string *newType,
			std::string * headerFile, std::string * projectPath,
			bool * isStruct, std::vector<std::string> &includedPaths,
			std::string * stFile, std::string *outputFileSolver,
			std::string * castToOld, std::string * castToNew);

	Rose_STL_Container<std::string> parseRetypeParametersSymbolTable(int argc,
			char ** argv, std::string * projectPath,
			std::string * outputFilePath);

	Rose_STL_Container<std::string> parseRetypeParametersAnalyzer(int argc,
			char ** argv, std::string *initialType, std::string *newType,
			std::string * headerFile, std::string * projectPath,
			std::string * outputOpsFile, bool * isStruct,
			std::vector<std::string> &includedPaths, std::string * stFile,
			std::string * outFileOpDefs, bool * isUserDefined);

	Rose_STL_Container<std::string> parseRetypeParametersInferenceEngine(
			int argc, char ** argv, std::string * basisFile,
			std::string * userOpsMappingFile,
			std::string * constraintsOutputFile, std::string * outputFileSolver,
			bool * castOpProvided, std::string * errorOutputFile, std::string * typeY);
};

} /* namespace retyper */

#endif /* SRC_RETYPEPARAMETERPARSER_H_ */
