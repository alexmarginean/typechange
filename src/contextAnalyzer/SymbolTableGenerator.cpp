/*
 * SymbolTableGenerator.cpp
 *
 *  Created on: 7 Jan 2015
 *      Author: alex
 */

#include "SymbolTableGenerator.h"
#include <boost/filesystem.hpp>
#include <rose.h>
#include <RetypeParameterParser.h>
#include <iostream>

namespace ST {

using namespace std;
using namespace retyper;

SymbolTableGenerator::SymbolTableGenerator() {
	this->file = NULL;
}

SymbolTableGenerator::~SymbolTableGenerator() {
	// TODO Auto-generated destructor stub
}

SymbolTableGenerator::SymbolTableGenerator(ofstream *datfile) {
	assert(datfile != NULL);
	file = datfile;
}

void SymbolTableGenerator::visit(SgNode* node) {
	switch (node->variantT()) {
	case V_SgFunctionDeclaration: {
		SgFunctionDeclaration* functionDecl = isSgFunctionDeclaration(node);
		SgDeclarationStatement* definingDecl =
				functionDecl->get_definingDeclaration();

		if (functionDecl == definingDecl) {
#if 0
			std::cout << "DEFINING DECL: " << functionDecl->unparseToCompleteString() << endl;
#endif
			outputPositionInformationFunction(functionDecl);
		}

		break;
	}
	case V_SgVariableDeclaration: {
		SgVariableDeclaration* variableDeclaration = isSgVariableDeclaration(
				node);

		// Look for variable declarations appearing in global scope!
		// if (variableDeclaration != NULL && isSgGlobal(variableDeclaration->get_parent()) != NULL)

		// Do not do this for static / extern definitions
		// In the case of static we will retype anything in the next level compiler !!!

		if (variableDeclaration != NULL
				&& variableDeclaration->get_declarationModifier().get_storageModifier().isStatic()
						== false &&
						variableDeclaration->get_declarationModifier().get_storageModifier().isExtern() == false && isSgGlobal(variableDeclaration->get_scope()) != NULL) {
			SgInitializedNamePtrList::iterator i =
					variableDeclaration->get_variables().begin();
			while (i != variableDeclaration->get_variables().end()) {
				SgInitializedName* initializedName = *i;

				// Check the type and see if it is a class (check for typedefs too)
				SgType* variableType = initializedName->get_type();

				outputPositionInformation(initializedName);
				i++;
			}

			// increment though the variables in the declaration (typically just one)
		}
		break;
	}
	case V_SgVarRefExp: {
		SgVarRefExp * varRef = isSgVarRefExp(node);
		break;
	}
	default: {
		break;
	}
	}
}

void SymbolTableGenerator::outputPositionInformation(
		SgInitializedName* statement) {
	int lineNumber = statement->get_file_info()->get_raw_line();
	string fileName = statement->get_file_info()->get_raw_filename();

	if (RetypeParameterParser::path_contain_subpath(this->projectDefaultPath,
			boost::filesystem::canonical(fileName))) {
#if 0
		printf("Problem Variable Declaration at line = %4d file = %s \n",
				lineNumber, fileName.c_str());
#endif
		ROSE_ASSERT(file != NULL);

		(*file) << "GV  " << statement->get_name().str() << " "
				<< boost::filesystem::canonical(fileName).string() << std::endl;
	}
}

void SymbolTableGenerator::setDefaultProjectPath(std::string path) {
	this->projectDefaultPath = path;
}

void SymbolTableGenerator::outputPositionInformationFunction(
		SgFunctionDeclaration* statement) {
	int lineNumber = statement->get_file_info()->get_raw_line();
	string fileName = statement->get_file_info()->get_raw_filename();

	if (RetypeParameterParser::path_contain_subpath(this->projectDefaultPath,
			boost::filesystem::canonical(fileName))) {
#if 0
		printf("Problem Variable Declaration at line = %4d file = %s \n",
				lineNumber, fileName.c_str());
#endif
		ROSE_ASSERT(file != NULL);

		(*file) << "FD  " << statement->get_name().str() << " "
				<< boost::filesystem::canonical(fileName).string() << std::endl;
	}
}

} /* namespace ST */
