/*
 * SymbolTableGenerator.h
 *
 *  Created on: 7 Jan 2015
 *      Author: alex
 */

#ifndef SRC_ST_SYMBOLTABLEGENERATOR_H_
#define SRC_ST_SYMBOLTABLEGENERATOR_H_

#include <rose.h>

namespace ST {

class SymbolTableGenerator: public AstSimpleProcessing {
 public:
	SymbolTableGenerator();
	SymbolTableGenerator(std::ofstream *datfile);
	virtual ~SymbolTableGenerator();
	void setDefaultProjectPath(std::string path);

 protected:
	void visit(SgNode* node);

 private:
	std::string projectDefaultPath;
	std::ofstream * file;

	void outputPositionInformation(SgInitializedName* statement);
	void outputPositionInformationFunction(SgFunctionDeclaration* statement);
};

} /* namespace ST */

#endif /* SRC_ST_SYMBOLTABLEGENERATOR_H_ */
