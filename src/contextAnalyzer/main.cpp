// #define BOOST_FILESYSTEM_VERSION 3

#include <rose.h>
#include <boost/filesystem.hpp>
#include <RetypeParameterParser.h>
#include "SymbolTableGenerator.h"

#include <sys/wait.h>

using namespace std;

using namespace retyper;
using namespace ST;

#define DEBUG

#define SOURCE_POSITION Sg_File_Info::generateDefaultFileInfoForTransformationNode()

// Build a synthesized attribute for the tree traversal

int main(int argc, char * argv[]) {

	std::string projectPath;
	std::string outputFileName;
	RetypeParameterParser parametersParser;

	Rose_STL_Container<string> l = parametersParser.parseRetypeParametersSymbolTable(argc, argv, &projectPath,
			&outputFileName);

	// Symbol table generator

	ios::sync_with_stdio();     // Syncs C++ and C I/O subsystems!

	ofstream datfile(outputFileName.c_str(), ios::out | ios::app);
	if (datfile.good() == false) {
		printf("File failed to open \n");
	}

	SgProject* project = frontend(l);

	ROSE_ASSERT(project != NULL);

	SymbolTableGenerator traversal(&datfile);
	traversal.setDefaultProjectPath(
			boost::filesystem::canonical(projectPath).string());
	traversal.traverse(project, preorder);

	datfile.close();

#if 0
	generateAstGraph(project, 30000);
	generateDOT(*project);
#endif

	std::cout << "ST Generator completed successful!";

	return backend(project);

	int resultCompile = backend(project);

	/*
	 * Our success code is 999 in these tools.
	 */
	if (!resultCompile) {
		return 999;
	} else {
		return resultCompile;
	}

}
