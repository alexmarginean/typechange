/*
 * RetypeParameterParser.cpp
 *
 *  Created on: 6 Jan 2015
 *      Author: alex
 */

#include <rose.h>
//#include "Retyper.h"
#include <boost/filesystem.hpp>
#include "RetypeParameterParser.h"

namespace retyper {

using namespace std;

RetypeParameterParser::RetypeParameterParser() {
	// TODO Auto-generated constructor stub

}

RetypeParameterParser::~RetypeParameterParser() {
	// TODO Auto-generated destructor stub
}

bool RetypeParameterParser::path_contain_subpath(
		boost::filesystem::path projectPath, boost::filesystem::path subpath) {
	// If dir ends with "/" and isn't the root directory, then the final
	// component returned by iterators will include "." and will interfere
	// with the std::equal check below, so we strip it before proceeding.
	if (projectPath.filename() == ".")
		projectPath.remove_filename();
	// We're also not interested in the file's name.
	//assert(!subpath.has_filename());
	//subpath.remove_filename();

	// If dir has more components than file, then file can't possibly
	// reside in dir.
	int dir_len = std::distance(projectPath.begin(), projectPath.end());
	int file_len = std::distance(subpath.begin(), subpath.end());
	if (dir_len > file_len)
		return false;

	// This stops checking when it reaches dir.end(), so it's OK if file
	// has more directory components afterward. They won't be checked.
	return std::equal(projectPath.begin(), projectPath.end(), subpath.begin());
}

Rose_STL_Container<std::string> RetypeParameterParser::parseRetypeParameters(
		int argc, char ** argv, std::string *initialType, std::string *newType,
		std::string * headerFile, std::string * projectPath, bool * isStruct,
		std::vector<std::string> &includedPaths, std::string * stFile,
		std::string *outputFileSolver, std::string * castToOld,
		std::string * castToNew) {
	Rose_STL_Container<string> l =
			CommandlineProcessing::generateArgListFromArgcArgv(argc, argv);

#if 0
	printf ("Preprocessor (before): argv = \n%s \n",StringUtility::listToString(l).c_str());
#endif

//original type
//get the parameter list and mark the parameter for removal
	Rose_STL_Container<string> initialTypeList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_X", "--removed");
//the size should be 2; first element for the stripped parameter
	cout << "SIZE:: " << initialTypeList.size() << endl;
	ROSE_ASSERT(initialTypeList.size() == 2);
	*initialType = initialTypeList[1];

//new type
//get the parameter list and mark the parameter for removal
	Rose_STL_Container<string> newTypeList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_Y", "--removed");
//the size should be 2; first element for the stripped parameter
	cout << "SIZE:: " << newTypeList.size() << endl;
	ROSE_ASSERT(newTypeList.size() == 2);
	*newType = newTypeList[1];

	// Default project path
	// If not defined then consider it to be the current path
	Rose_STL_Container<string> projectPathList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_project_path", "--removed");
	//the size should be 2; first element for the stripped parameter
	cout << "SIZE:: " << projectPathList.size() << endl;
	//ROSE_ASSERT(initialTypeList.size() == 2);
	//*initialType = initialTypeList[1];
	if (projectPathList.size() == 0) {
		*projectPath = boost::filesystem::current_path().string();
	} else {
		*projectPath = projectPathList[1];
	}

//header file if it is the case for the struct
	Rose_STL_Container<string> headerFileList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_header", "--removed");
	cout << "SIZE:: " << headerFileList.size() << endl;

	if (headerFileList.size() == 0) {
		*isStruct = false;
	} else {
		*isStruct = true;
		ROSE_ASSERT(headerFileList.size() == 2);
		*headerFile = headerFileList[1];
	}

	// Where to place the output serialized
	Rose_STL_Container<string> outputFileSolverList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_solution", "--removed");
	//the size should be 2; first element for the stripped parameter
	if (outputFileSolverList.size() == 0) {
		/*
		 * Error. This parameter is required!
		 */
		assert(false);
	} else {
		*outputFileSolver = outputFileSolverList[1];
	}

	// ST File - required
	//get the parameter list and mark the parameter for removal
	Rose_STL_Container<string> stFileList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_ST", "--removed");
	//the size should be 2; first element for the stripped parameter
	ROSE_ASSERT(stFileList.size() == 2);
	*stFile = stFileList[1];

	// CastToOldFunctionName
	//get the parameter list and mark the parameter for removal
	Rose_STL_Container<string> castToOldTypeParam =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_cast_old", "--removed");
	//the size should be 2; first element for the stripped parameter
	if (castToOldTypeParam.size() == 0) {
		/*
		 * Signal that we do not have a cast to old operator!!!
		 */
		*castToOld = "NON";
	} else {
		*castToOld = castToOldTypeParam[1];
	}

	// CastToNewFunctionName
	//get the parameter list and mark the parameter for removal
	Rose_STL_Container<string> castToNewTypeParam =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_cast_new", "--removed");
	//the size should be 2; first element for the stripped parameter
	if (castToNewTypeParam.size() == 0) {
		/*
		 * Signal that we do not have a cast to old operator!!!
		 */
		*castToNew = "NON";
	} else {
		*castToNew = castToNewTypeParam[1];
	}

//remove the parameters for retype from the gcc parameter list
	CommandlineProcessing::removeArgsWithParameters(l, "--removed");

	//*isStruct = CommandlineProcessing::isOption(l, "--retype_struct", "", true);

#if 0
	printf ("l.size() = %zu \n",l.size());
	printf ("Preprocessor (after): argv = \n%s \n",StringUtility::listToString(l).c_str());
#endif

	// Generate the list of included paths
	Rose_STL_Container<string> includedPathList =
			CommandlineProcessing::generateOptionList(l, "-I");
	for (int i = 0; i < includedPathList.size(); i++) {
#if 0
		std::cout<<includedPathList[i]<<std::endl;
		std::cout<<boost::filesystem::canonical(includedPathList[i]).string()<<std::endl;
		std::cout<<*projectPath<<std::endl;
#endif

		// We should add the path iff it is a subfolder of the project path
		// Otherwise we do not want to modify the header file!!!

		if (path_contain_subpath(*projectPath,
				boost::filesystem::canonical(includedPathList[i]))) {
#if 0
			std::cout<<boost::filesystem::canonical(includedPathList[i]).string()<<std::endl;
#endif
			includedPaths.push_back(
					boost::filesystem::canonical(includedPathList[i]).string());
		}

	}

	return l;

}

Rose_STL_Container<std::string> RetypeParameterParser::parseRetypeParametersAnalyzer(
		int argc, char ** argv, std::string *initialType, std::string *newType,
		std::string * headerFile, std::string * projectPath,
		std::string * outputOpsFile, bool * isStruct,
		std::vector<std::string> &includedPaths, std::string * stFile,
		std::string * outFileOpDefs, bool * isUserDefined) {
	Rose_STL_Container<string> l =
			CommandlineProcessing::generateArgListFromArgcArgv(argc, argv);

#if 0
	printf ("Preprocessor (before): argv = \n%s \n",StringUtility::listToString(l).c_str());
#endif

//original type
//get the parameter list and mark the parameter for removal
	Rose_STL_Container<string> initialTypeList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_X", "--removed");
//the size should be 2; first element for the stripped parameter
	cout << "SIZE:: " << initialTypeList.size() << endl;
	ROSE_ASSERT(initialTypeList.size() == 2);
	*initialType = initialTypeList[1];

//new type
//get the parameter list and mark the parameter for removal
	Rose_STL_Container<string> newTypeList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_Y", "--removed");
//the size should be 2; first element for the stripped parameter
	cout << "SIZE:: " << newTypeList.size() << endl;
	ROSE_ASSERT(newTypeList.size() == 2);
	*newType = newTypeList[1];

	// Default project path
	// If not defined then consider it to be the current path
	Rose_STL_Container<string> projectPathList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_project_path", "--removed");
	//the size should be 2; first element for the stripped parameter
	cout << "SIZE:: " << projectPathList.size() << endl;
	//ROSE_ASSERT(initialTypeList.size() == 2);
	//*initialType = initialTypeList[1];
	if (projectPathList.size() == 0) {
		*projectPath = boost::filesystem::current_path().string();
	} else {
		*projectPath = projectPathList[1];
	}

//header file if it is the case for the struct
	Rose_STL_Container<string> headerFileList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_header", "--removed");
	cout << "SIZE:: " << headerFileList.size() << endl;

	// Output file for operators
	//get the parameter list and mark the parameter for removal
	Rose_STL_Container<string> outputFileList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_out", "--removed");
	//the size should be 2; first element for the stripped parameter
	cout << "SIZE:: " << outputFileList.size() << endl;
	ROSE_ASSERT(outputFileList.size() == 2);
	*outputOpsFile = outputFileList[1];

	if (headerFileList.size() == 0) {
		*isStruct = false;
	} else {
		*isStruct = true;
		ROSE_ASSERT(headerFileList.size() == 2);
		*headerFile = headerFileList[1];
	}

	// ST File - required
	//get the parameter list and mark the parameter for removal
	Rose_STL_Container<string> stFileList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_ST", "--removed");
	//the size should be 2; first element for the stripped parameter
	ROSE_ASSERT(stFileList.size() == 2);
	*stFile = stFileList[1];

	/*!
	 * Output file for the operator definitions -- required
	 */
	Rose_STL_Container<string> opDefsFileList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_op_def", "--removed");
	//the size should be 2; first element for the stripped parameter
	ROSE_ASSERT(opDefsFileList.size() == 2);
	*outFileOpDefs = opDefsFileList[1];

	/*!
	 * Check wheter or not we are currently compiling for
	 * user defined operators. This is important for knowing what types to output.
	 */
	Rose_STL_Container<string> sourceOrInputFileList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_type", "--removed");
	//the size should be 2; first element for the stripped parameter
	ROSE_ASSERT(sourceOrInputFileList.size() == 2);
	if (sourceOrInputFileList[1] == "program") {
		*isUserDefined = false;

	} else if (sourceOrInputFileList[1] == "ops") {
		*isUserDefined = true;
	} else {
		std::cout
				<< "ERROR! Not specified correctly the type of current operation!"
				<< std::endl;
		exit(-1);
	}

//remove the parameters for retype from the gcc parameter list
	CommandlineProcessing::removeArgsWithParameters(l, "--removed");

	//*isStruct = CommandlineProcessing::isOption(l, "--retype_struct", "", true);

#if 0
	printf ("l.size() = %zu \n",l.size());
	printf ("Preprocessor (after): argv = \n%s \n",StringUtility::listToString(l).c_str());
#endif

	// Generate the list of included paths
	Rose_STL_Container<string> includedPathList =
			CommandlineProcessing::generateOptionList(l, "-I");
	for (int i = 0; i < includedPathList.size(); i++) {
#if 0
		std::cout<<includedPathList[i]<<std::endl;
		std::cout<<boost::filesystem::canonical(includedPathList[i]).string()<<std::endl;
		std::cout<<*projectPath<<std::endl;
#endif

		// We should add the path iff it is a subfolder of the project path
		// Otherwise we do not want to modify the header file!!!

		if (path_contain_subpath(*projectPath,
				boost::filesystem::canonical(includedPathList[i]))) {
#if 0
			std::cout<<boost::filesystem::canonical(includedPathList[i]).string()<<std::endl;
#endif
			includedPaths.push_back(
					boost::filesystem::canonical(includedPathList[i]).string());
		}

	}

	return l;

}

Rose_STL_Container<std::string> RetypeParameterParser::parseRetypeParametersSymbolTable(
		int argc, char ** argv, std::string * projectPath,
		std::string * outputFilePath) {
	Rose_STL_Container<string> l =
			CommandlineProcessing::generateArgListFromArgcArgv(argc, argv);

#if 0
	printf ("Preprocessor (before): argv = \n%s \n",StringUtility::listToString(l).c_str());
#endif

	// Default project path
	// If not defined then consider it to be the current path
	Rose_STL_Container<string> projectPathList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_project_path", "--removed");
	//the size should be 2; first element for the stripped parameter
	if (projectPathList.size() == 0) {
		*projectPath = boost::filesystem::current_path().string();
	} else {
		*projectPath = projectPathList[1];
	}

	Rose_STL_Container<string> outputFilePathList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_ST", "--removed");
	if (outputFilePathList.size() == 2) {
		*outputFilePath = outputFilePathList[1];
	} else {
		*outputFilePath = "defaultST.data";
	}

//remove the parameters for retype from the gcc parameter list
	CommandlineProcessing::removeArgsWithParameters(l, "--removed");

#if 0
	printf ("l.size() = %zu \n",l.size());
	printf ("Preprocessor (after): argv = \n%s \n",StringUtility::listToString(l).c_str());
#endif

	// Generate the list of included paths
	Rose_STL_Container<string> includedPathList =
			CommandlineProcessing::generateOptionList(l, "-I");
	for (int i = 0; i < includedPathList.size(); i++) {
#if 0
		std::cout<<includedPathList[i]<<std::endl;
		std::cout<<boost::filesystem::canonical(includedPathList[i]).string()<<std::endl;
		std::cout<<*projectPath<<std::endl;
#endif

		// We should add the path iff it is a subfolder of the project path
		// Otherwise we do not want to modify the header file!!!

	}

	return l;

}

Rose_STL_Container<std::string> RetypeParameterParser::parseRetypeParametersInferenceEngine(
		int argc, char ** argv, std::string * basisFile,
		std::string * userOpsMappingFile, std::string * constraintsOutputFile,
		std::string * outputFileSolver, bool * castOpProvided,
		std::string * errorOutputFile, std::string * typeY) {
	Rose_STL_Container<string> l =
			CommandlineProcessing::generateArgListFromArgcArgv(argc, argv);

#if 0
	printf ("Preprocessor (before): argv = \n%s \n",StringUtility::listToString(l).c_str());
#endif

	// Basis file
	Rose_STL_Container<string> basisFileList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_op_def", "--removed");
	//the size should be 2; first element for the stripped parameter
	if (basisFileList.size() == 0) {
		/*
		 * We should error here! this is a required parameter.
		 */
		assert(false);
	} else {
		*basisFile = basisFileList[1];
	}

	// user operators mapping file
	Rose_STL_Container<string> userOpFileList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_mappings", "--removed");
	//the size should be 2; first element for the stripped parameter
	if (userOpFileList.size() == 0) {
		/*
		 * No mapping provided, so we should go with
		 * the identical mapping!
		 */
		*userOpsMappingFile = "";
	} else {
		*userOpsMappingFile = userOpFileList[1];
	}

	// Constraints output file, for showing proofs and errors.
	Rose_STL_Container<string> constraintsOutputFileList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_constraints", "--removed");
	//the size should be 2; first element for the stripped parameter
	if (constraintsOutputFileList.size() == 0) {
		/*
		 * No constraints output file provided
		 */
		*constraintsOutputFile = "";
	} else {
		*constraintsOutputFile = constraintsOutputFileList[1];
	}

	// Where to place the output serialized
	Rose_STL_Container<string> outputFileSolverList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_solution", "--removed");
	//the size should be 2; first element for the stripped parameter
	if (outputFileSolverList.size() == 0) {
		/*
		 * Error. This parameter is required!
		 */
		assert(false);
	} else {
		*outputFileSolver = outputFileSolverList[1];
	}

	// Check if we have user defined ops!
	Rose_STL_Container<string> castOpProvidedList =
			CommandlineProcessing::generateOptionList(l, "--retype_casts");
	//the size should be 2; first element for the stripped parameter
	if (castOpProvidedList.size() == 0) {
		/*
		 * No user op provided
		 */
		*castOpProvided = false;
	} else {
		/*
		 * user op provided.
		 */
		*castOpProvided = true;
	}

	// Where to place the errors if existing!
	Rose_STL_Container<string> outputFileErrors =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_errors", "--removed");
	//the size should be 2; first element for the stripped parameter
	if (outputFileErrors.size() == 0) {
		*errorOutputFile = "";
	} else {
		*errorOutputFile = outputFileErrors[1];
	}

	//new type
	//get the parameter list and mark the parameter for removal
	Rose_STL_Container<string> newTypeList =
			CommandlineProcessing::generateOptionWithNameParameterList(l,
					"--retype_Y", "--removed");
	//the size should be 2; first element for the stripped parameter
	cout << "SIZE:: " << newTypeList.size() << endl;
	ROSE_ASSERT(newTypeList.size() == 2);
	*typeY = newTypeList[1];

	return l;

}

}
/* namespace retyper */
