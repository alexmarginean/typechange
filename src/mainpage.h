/*
 * mainpage.h
 *
 *  Created on: 10 Mar 2015
 *      Author: alex
 */

#ifndef SRC_MAINPAGE_H_
#define SRC_MAINPAGE_H_

/*! \mainpage Retype Main Documentation Page
 *
 * \section intro_sec Introduction
 *
 * This is the introduction.
 *
 * \section install_sec Installation
 *
 * \subsection step1 Step 1: Opening the box
 *
 * etc...
 * \section usage_sec Usage
 * Usage section ...
 * \section testing_sec Testing
 * Testing section ...
 */

#endif /* SRC_MAINPAGE_H_ */
