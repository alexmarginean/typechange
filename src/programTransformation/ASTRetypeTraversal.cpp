/*
 * ASTRetypeTraversal.cpp
 *
 *  Created on: 9 Dec 2014
 *      Author: alex
 */

#include <RetypePersistantAttribute.h>
#include <rose.h>
#include "ASTRetypeTraversal.h"
#include <iostream>
#include <algorithm>

namespace retyper {

int CastInformation::id = 0;

using namespace SageBuilder;
using namespace SageInterface;
using namespace std;

struct callLocationEqual: std::unary_function<OperatorReplacingMapping*, bool> {
	callLocationEqual(const Sg_File_Info * currentLocation) :
			currentLocation_(currentLocation) {
	}
	bool operator()(OperatorReplacingMapping * arg) const {
		OperatorCallUniqueIdentifier * initialLocationOfCurrent =
				arg->locationInInputProgram();
		return (*initialLocationOfCurrent).operator ==(currentLocation_);
	}
	const Sg_File_Info * currentLocation_;
};

// Simple ROSE traversal class: This allows us to visit all the functions and add
// new code to instrument/record their use. We will retype every variable declaration
// to the new type specified by the user

std::string ASTRetypeTraversal::getCurrentTypeOfVariable(SgType * varType) {
	/*
	 * TODO: modify here if we want pointers to be different types
	 * that the non pointer ones!
	 */
	SgPointerType * pointerDerefExpression = isSgPointerType(varType);
	while (pointerDerefExpression) {
		varType = pointerDerefExpression->get_base_type();
		pointerDerefExpression = isSgPointerType(varType);
	}
	SgClassType* classType = isSgClassType(varType);
	std::string currentType = "";
	if (classType) {
		//user defined initial type
		currentType = classType->get_name();
	} else {
		currentType = varType->unparseToCompleteString();
	}
	//again trim the spaces
	currentType = trimSpaces(currentType);
	return currentType;
}

SgFunctionCallExp* ASTRetypeTraversal::renameFunctionCall(
		SgFunctionCallExp * initialCall,
		OperatorReplacingMapping* currentReplacement) {

	/*
	 * This entire method is a hack!!! TODO: FIX THIS
	 * Currently we create a new function symbol that is not outputted for each
	 * call of a retyped function. In theory, here we should also open the user
	 * provided definitions, and connect the function calls to that ones!!!!
	 * However, since here we have an external fct only, the current solution
	 * might be the right one iff we collect in a vector all the introduced symbols
	 * and reuse them rather than creating a new symbol.
	 */
	SgStatement * enclosingStatement = SageInterface::getEnclosingStatement(
			initialCall);
	SgBasicBlock * block = isSgBasicBlock(enclosingStatement->get_parent());
	SgName name1(currentReplacement->newOperatorName());

	Types retType = currentReplacement->newExpression()->typesOfReturn()[0];

	// TODO: properly handle the types of user defined operators.
	// Curently we are just regenerating them. The definition of an operator should be identified
	// and kept in the operator definition structur

	SgType * retTypeOfOp;

	if (retType == T_o) {
		retTypeOfOp = oldType_;
	} else if (retType == T_n) {
		retTypeOfOp = newType_;
	} else if (retType == T_u) {
		// A random type!
		retTypeOfOp = new SgTypeChar();
	} else {
		/*
		 * We shouldn't ever be here!
		 */
		assert(false);
	}

	/*
	 * Construct the types of actual args!
	 */

	SgExpressionPtrList& actualArgList =
			initialCall->get_args()->get_expressions();

	std::vector<SgType*> arrayOfCurrentTypes;

	/*!
	 * iterate ober the list of formal args.
	 */
	for (SgExpressionPtrList::iterator it = actualArgList.begin();
			it != actualArgList.end(); it++) {

		/*
		 * First push the argument in the array of argument,
		 * for cast fixings!
		 */
		arrayOfCurrentTypes.push_back((*it)->get_type());
	}

	// It is up to the user to link the implementations of these functions link time
	SgFunctionDeclaration *decl_1 =
			SageBuilder::buildNondefiningFunctionDeclaration(name1, retTypeOfOp,
					SageBuilder::buildFunctionParameterList(
							SageBuilder::buildFunctionParameterTypeList(
									initialCall->get_args())), block);

	((decl_1->get_declarationModifier()).get_storageModifier()).setExtern();
	/*
	 * Keep a reference to the current parent of the operand,
	 * for fixing it as the parent of the call function expression!
	 */
	SgNode * currentOperandParent = initialCall->get_parent();
	SgFunctionCallExp * callStmt_1 = SageBuilder::buildFunctionCallExp(name1,
			retTypeOfOp, initialCall->get_args(),
			enclosingStatement->get_scope());
	/*
	 * Now fix the parent of the function call.
	 */
	callStmt_1->set_parent(currentOperandParent);

	/*
	 * Now fix for the parent, to be connected to the new function call
	 * in the place of the binary operator!
	 */

	SgExprListExp * parentNode = isSgExprListExp(currentOperandParent);
	SgBinaryOp * parentBinOp = isSgBinaryOp(currentOperandParent);
	SgExprStatement * parentStatement = isSgExprStatement(currentOperandParent);

	if (parentNode) {
		SgExpressionPtrList::iterator itForCurrent;
		for (SgExpressionPtrList::iterator itExpr =
				parentNode->get_expressions().begin();
				itExpr != parentNode->get_expressions().end(); ++itExpr) {
			if ((*itExpr) == initialCall) {
				*itExpr = callStmt_1;
			}
		}

	} else {

		/*
		 * Binary Op Case:
		 */

		if (parentBinOp) {

			/*
			 * first check if the operand to be casted is lhs or rhs
			 */
			if (parentBinOp->get_rhs_operand() == initialCall) {
				parentBinOp->set_rhs_operand(callStmt_1);
			} else if (parentBinOp->get_lhs_operand() == initialCall) {
				parentBinOp->set_lhs_operand(callStmt_1);
			} else {
				/*
				 * We should never be here!
				 */
				assert(false);
			}

		} else {
			/*
			 * SgStatement Case
			 */
			if (parentStatement) {
				parentStatement->set_expression(callStmt_1);
			} else {
				/*
				 * TODO: Check here what else it could be! Not supported for now...
				 */
				assert(false);
			}
		}
	}

	return callStmt_1;
}

SgFunctionCallExp* ASTRetypeTraversal::addFunctionCallForBinary(
		SgBinaryOp * binOp, OperatorReplacingMapping* currentReplacement) {
	SgStatement * enclosingStatement = SageInterface::getEnclosingStatement(
			binOp);
	SgBasicBlock * block = isSgBasicBlock(enclosingStatement->get_parent());
	SgName name1(currentReplacement->newOperatorName());

	Types retType = currentReplacement->newExpression()->typesOfReturn()[0];

	// TODO: properly handle the types of user defined operators.
	// Curently we are just regenerating them. The definition of an operator should be identified
	// and kept in the operator definition structur

	SgType * retTypeOfOp;

	if (retType == T_o) {
		retTypeOfOp = oldType_;
	} else if (retType == T_n) {
		retTypeOfOp = newType_;
	} else if (retType == T_u) {
		// A random type!
		retTypeOfOp = new SgTypeChar();
	} else {
		/*
		 * We shouldn't ever be here!
		 */
		assert(false);
	}

	/*
	 * This is a replacement for a binary operator. So, we
	 * know that it takes 2 args. Identify their types!
	 */

	SgType * lhsType;
	SgType * rhsType;

	if (currentReplacement->newExpression()->typesOfArgs()[0][0] == T_o) {
		lhsType = oldType_;
	} else if (currentReplacement->newExpression()->typesOfArgs()[0][0]
			== T_n) {
		lhsType = newType_;
	} else if (currentReplacement->newExpression()->typesOfArgs()[0][0]
			== T_u) {
		lhsType = new SgTypeChar();
	} else {
		// We shouldn't ever be here!
		assert(false);
	}

	if (currentReplacement->newExpression()->typesOfArgs()[1][0] == T_o) {
		rhsType = oldType_;
	} else if (currentReplacement->newExpression()->typesOfArgs()[1][0]
			== T_n) {
		rhsType = newType_;
	} else if (currentReplacement->newExpression()->typesOfArgs()[1][0]
			== T_u) {
		rhsType = new SgTypeChar();
	} else {
		// We shouldn't ever be here!
		assert(false);
	}

	// It is up to the user to link the implementations of these functions link time
	SgFunctionDeclaration *decl_1 =
			SageBuilder::buildNondefiningFunctionDeclaration(name1, retTypeOfOp,
					SageBuilder::buildFunctionParameterList(
							SageBuilder::buildFunctionParameterTypeList(lhsType,
									rhsType)), block);

	((decl_1->get_declarationModifier()).get_storageModifier()).setExtern();
	/*
	 * Keep a reference to the current parent of the operand,
	 * for fixing it as the parent of the call function expression!
	 */
	SgNode * currentOperandParent = binOp->get_parent();
	SgFunctionCallExp * callStmt_1 = SageBuilder::buildFunctionCallExp(name1,
			retTypeOfOp,
			SageBuilder::buildExprListExp(binOp->get_lhs_operand(),
					binOp->get_rhs_operand()), enclosingStatement->get_scope());
	/*
	 * Now fix the parent of the function call.
	 */
	callStmt_1->set_parent(currentOperandParent);

	/*
	 * Now fix for the parent, to be connected to the new function call
	 * in the place of the binary operator!
	 */

	SgExprListExp * parentNode = isSgExprListExp(currentOperandParent);
	SgBinaryOp * parentBinOp = isSgBinaryOp(currentOperandParent);
	SgExprStatement * parentStatement = isSgExprStatement(currentOperandParent);

	if (parentNode) {
		SgExpressionPtrList::iterator itForCurrent;
		for (SgExpressionPtrList::iterator itExpr =
				parentNode->get_expressions().begin();
				itExpr != parentNode->get_expressions().end(); ++itExpr) {
			if ((*itExpr) == binOp) {
				*itExpr = callStmt_1;
			}
		}

	} else {

		/*
		 * Binary Op Case:
		 */

		if (parentBinOp) {

			/*
			 * first check if the operand to be casted is lhs or rhs
			 */
			if (parentBinOp->get_rhs_operand() == binOp) {
				parentBinOp->set_rhs_operand(callStmt_1);
			} else if (parentBinOp->get_lhs_operand() == binOp) {
				parentBinOp->set_lhs_operand(callStmt_1);
			} else {
				/*
				 * We should never be here!
				 */
				assert(false);
			}

		} else {
			/*
			 * SgStatement Case
			 */
			if (parentStatement) {
				parentStatement->set_expression(callStmt_1);
			} else {
				/*
				 * TODO: Check here what else it could be! Not supported for now...
				 */
				assert(false);
			}
		}
	}

	return callStmt_1;
}

SgType * addRequiredPointersAtType(SgType * oldType, SgType * newType) {
	SgType * returnType = newType;

	SgPointerType * pointerDerefExpression = isSgPointerType(oldType);
	while (pointerDerefExpression) {
		oldType = pointerDerefExpression->get_base_type();
		pointerDerefExpression = isSgPointerType(oldType);

		returnType = new SgPointerType(returnType);
	}

	return returnType;
}

void ASTRetypeTraversal::visit(SgNode* node) {

#if 0
	//unparse all the nodes of the project
	cout << node->unparseToCompleteString() << endl;
	cout << "OF TYPE: " << node->class_name() << endl;
#endif

	/*
	 * TODO: HANDLE SOMEHOW SIZEOF OPS AND CASTS TO TYPE NAMES!!!!
	 * This is a bug in ROSE!
	 */

	//check the type of the current node
	switch (node->variantT()) {

	case V_SgFunctionDeclaration: {

#if 0
		std::cout << "ENTERED IN FUNCTION DECLARATION:: " << node->unparseToCompleteString() << std::endl;
#endif
		// First check wheter or not the function should be modified
		// If it is not in the modified symbol list then it shouldn't !

		//get the return type of the current function declaration
		SgFunctionDeclaration * declaration = isSgFunctionDeclaration(node);

		std::string currentFunctionName = declaration->get_name().str();
		//declaration->get_name

#if 1
		cout << currentFunctionName << endl;
#endif

		// TODO: We should always retype a static function. However the definition of it should be in
		// the defined symbols list, so no special action is required here. To be checked.

		if (std::find(this->definedSymbolsList.begin(),
				this->definedSymbolsList.end(), currentFunctionName)
				!= this->definedSymbolsList.end()) {
			// The current function is defined in the project, so we whould modify it!

			SgFunctionType * functionType = declaration->get_type();

#if 0
			std::cout << "FCT DECLARATION:: " << declaration->unparseToCompleteString() << std::endl;
			std::cout << "INITIAL FCT TYPE:: " << functionType->unparseToCompleteString() << std::endl;
#endif

			//declaration->set_parameterList(new SgFunctionParameterList(*(declaration->get_parameterList())));

			declaration->get_parameterList()->set_isModified(true);

			//break;

			//if it is a function declaration and we should retype its return type do so
			if (this->retypeFunctionReturn) {

				//check if it is a class declaration
				//and get the name of the original type

				SgType * returnOfCurrentFunctionType =
						functionType->get_return_type();

				string currentType = this->getCurrentTypeOfVariable(
						returnOfCurrentFunctionType);

				//remove the ended spaces of the type name
				//ROSE seams to add a space in the case of simple types and not add it in the case of ADT
				currentType = trimSpaces(currentType);

				bool changedRetType = false;

				if (currentType == type_x) {
					/*
					 * Save the current type as old type!
					 */
					this->oldType_ = returnOfCurrentFunctionType;
					//add an attribut for identifying it later
					AstAttribute * newAttribute = new RetypePersistentAttribute(
							SympleTypeAttribute);
					ROSE_ASSERT(newAttribute != NULL);

					//check for simple attributes
					SgType * newVarType;
					if (type_y == "int") {
						newVarType = new SgTypeInt();
					} else if (type_y == "double") {
						newVarType = new SgTypeDouble();
					} else if (type_y == "float") {
						newVarType = new SgTypeFloat;
					} else if (type_y == "char") {
						newVarType = new SgTypeChar;
					} else {

						//if we are here it means that it is an ADT
						SgClassType * classType =
								new SgClassType(
										this->ADTDeclaration->get_firstNondefiningDeclaration());
						SgNamedType * namedType = classType;
						newVarType = namedType;
					}
					/*
					 * Add the pointer symbol if case
					 */
					newVarType = addRequiredPointersAtType(
							returnOfCurrentFunctionType, newVarType);
					newVarType->addNewAttribute("AddedType", newAttribute);

					/*
					 * First set the return for the symbol, thus other calls
					 * of the function will know the new type.
					 */
					functionType->set_return_type(newVarType);
					/*
					 * Finally fix the return in the actual unparsed output!
					 */
					functionType->set_orig_return_type(newVarType);

					changedRetType = true;
				}
#if 0
				std::cout << "FINAL FCT TYPE:: " << functionType->unparseToCompleteString() << std::endl;
#endif

				if (changedRetType) {
					/*
					 * We retyped the return value of the function's declaration.
					 * Now, we also have to retype the return statement of the function as well!
					 */

					SgFunctionDefinition * currentFctDef =
							declaration->get_definition();

					/*
					 * Only if we have the definition here.
					 */

					if (currentFctDef) {
						SgBasicBlock * functionBody = currentFctDef->get_body();
						for (SgStatementPtrList::iterator itStm =
								functionBody->get_statements().begin();
								itStm != functionBody->get_statements().end();
								++itStm) {
							if (SgReturnStmt * retStmt = isSgReturnStmt(
									(*itStm))) {
								SgExpression * retExpression =
										retStmt->get_expression();
								/*
								 * Since we retyped the return value of the function, we have
								 * to also retype retExpression. If its type
								 * is not the new one, just add a cast. Thus, simply add this
								 * expression to the cast solver!
								 */

								/*
								 * TODO: ADD THIS IN THE INFERENCE ENGINE. FOR NOW,
								 * THIS IS A HACK.
								 * We check if it is primitive type, or not.
								 */

								/*
								 * First check if the type of the return expression
								 * is not already T_n, which should be the case in general.
								 */

								std::string typeNameString;

								/*
								 * TODO: modify here if we want pointers to be different types
								 * that the non pointer ones!
								 */
								SgPointerDerefExp * pointerDerefExpression =
										isSgPointerDerefExp(retExpression);
								while (pointerDerefExpression) {
									retExpression =
											pointerDerefExpression->get_operand();
									pointerDerefExpression =
											isSgPointerDerefExp(retExpression);
								}

								/*
								 * Similar for references!
								 */
								SgAddressOfOp * addresOfVar = isSgAddressOfOp(
										retExpression);
								while (addresOfVar) {
									retExpression = addresOfVar->get_operand();
									addresOfVar = isSgAddressOfOp(
											retExpression);
								}

								/*
								 * Get the current type as string.
								 */

								typeNameString = this->getCurrentTypeOfVariable(
										retExpression->get_type());

								if (typeNameString != this->type_y) {

									SgStatement * enclosingStatementCall =
											SageInterface::getEnclosingStatement(
													retExpression);
									this->enclosingStatementsForCasts_.push_back(
											enclosingStatementCall);

									std::vector<Types> typesOfRet;

									/*
									 * We reached here iff the return of the function is T_o.
									 * Thus, the type of this expression, is T_o, assuming the
									 * input program is correct. Thus, it should become T_n_c_y
									 */
									typesOfRet.push_back(T_n_c_y);

									std::vector<SgExpression*> arrayOfRetExpr;
									arrayOfRetExpr.push_back(retExpression);

									this->typesForCasts_.push_back(typesOfRet);
									this->argumentsForCasts.push_back(
											arrayOfRetExpr);
								}

							}
						}
					}
				}

			}

			if (this->retypeFunctionParameters) {

				//first fix for defining function declaration parameters

				SgFunctionParameterList * paramList =
						declaration->get_parameterList();

				for (SgInitializedNamePtrList::iterator i =
						paramList->get_args().begin();
						i != paramList->get_args().end(); i++) {

					SgType * currentParamType = (*i)->get_type();
					string currentType = this->getCurrentTypeOfVariable(
							currentParamType);
					//again trim the spaces
					currentType = trimSpaces(currentType);

					if (currentType == type_x) {
						/*
						 * Save the old type!!!
						 */
						this->oldType_ = currentParamType;

						// In the case of header files ROSE will see the parameters as compiler generated
						// For outputting them in the generated header file we need to fix this
						// Do this just when we have identified the desired type

						//(*i)->set_file_info(declaration->get_file_info());

						AstAttribute * newAttribute =
								new RetypePersistentAttribute(
										SympleTypeAttribute);
						ROSE_ASSERT(newAttribute != NULL);

						SgType * newVarType;
						if (type_y == "int") {
							newVarType = new SgTypeInt();
						} else if (type_y == "double") {
							newVarType = new SgTypeDouble();
						} else if (type_y == "float") {
							newVarType = new SgTypeFloat;
						} else if (type_y == "char") {
							newVarType = new SgTypeChar;
						} else {

							SgClassType * classType =
									new SgClassType(
											this->ADTDeclaration->get_firstNondefiningDeclaration());
							SgNamedType * namedType = classType;
							newVarType = namedType;

						}
						newVarType = addRequiredPointersAtType(currentParamType,
								newVarType);
						newVarType->addNewAttribute("AddedType", newAttribute);
						(*i)->set_type(newVarType);
					}

				}
			}

			// Now fix for undefining function declaration
			SgFunctionType * fctType = declaration->get_type();
			SgFunctionParameterTypeList * fctParamList =
					fctType->get_argument_list();

			for (SgTypePtrList::iterator i =
					fctParamList->get_arguments().begin();
					i != fctParamList->get_arguments().end(); i++) {

				SgType * currentParamUndefDeclsType = (*i);
				string currentType = this->getCurrentTypeOfVariable(
						currentParamUndefDeclsType);

				//again trim the spaces
				currentType = trimSpaces(currentType);

				if (currentType == type_x) {

					// In the case of header files ROSE will see the parameters as compiler generated
					// For outputting them in the generated header file we need to fix this
					// Do this just when we have identified the desired type

					//(*i)->set_file_info(declaration->get_file_info());

					AstAttribute * newAttribute = new RetypePersistentAttribute(
							SympleTypeAttribute);
					ROSE_ASSERT(newAttribute != NULL);

					SgType * newVarType;
					if (type_y == "int") {
						newVarType = new SgTypeInt();
					} else if (type_y == "double") {
						newVarType = new SgTypeDouble();
					} else if (type_y == "float") {
						newVarType = new SgTypeFloat;
					} else if (type_y == "char") {
						newVarType = new SgTypeChar;
					} else {

						SgClassType * classType =
								new SgClassType(
										this->ADTDeclaration->get_firstNondefiningDeclaration());
						SgNamedType * namedType = classType;
						newVarType = namedType;

					}
					newVarType = addRequiredPointersAtType(
							currentParamUndefDeclsType, newVarType);
					newVarType->addNewAttribute("AddedType", newAttribute);
					(*i) = newVarType;
				}

			}

		} else {
			// The function is not defined in the project so we shouldn't modify it
		}
#if 0
		cout << "EXIT FUNCTION DECLARATION <<<<<<<<<<<<<<<<<<<<<<<<<<<<"
		<< endl;
#endif

		break;

	}
	case V_SgVariableDeclaration: {
		//replace the type in the case of variable declarations

		SgVariableDeclaration* variableDeclaration = isSgVariableDeclaration(
				node);

		SgInitializedNamePtrList::iterator i =
				variableDeclaration->get_variables().begin();

		while (i != variableDeclaration->get_variables().end()) {

			// First check if the variable is global or not
			// If it is global, check if it is defined in the current project
			// Special case for static!!!

			bool shouldRetype = true;

			if (variableDeclaration->get_declarationModifier().get_storageModifier().isStatic()
					== true) {
				// If it is static we should always retype it
				// So nothing to do here
			} else {
				// The declaration is not static
				// First check if it is in global scope
				if (isSgGlobal(variableDeclaration->get_scope()) != NULL) {
					// In global scope, so we should check whether or not we have the definition for it
					// TODO: This should be done just for external declarations, but for now do it in all the cases

					std::string currentVarName = (*i)->get_name().str();
					if (std::find(this->definedSymbolsList.begin(),
							this->definedSymbolsList.end(), currentVarName)
							!= this->definedSymbolsList.end()) {
						// We have a definition for this variable, so we should retype it
					} else {
						// We do not have a definition so we shouldn't retype it
						shouldRetype = false;
					}

				} else {
					// It is not in global scope so we should always retype it
				}

			}

			if (shouldRetype) {
				SgType* variableType = (*i)->get_type();
				ROSE_ASSERT(variableType != NULL);

				string currentType = this->getCurrentTypeOfVariable(
						variableType);
				//trim the empty spaces
				currentType = trimSpaces(currentType);

				if (currentType == type_x) {

					/*
					 * Hold the pointer to the current data type.
					 * This is required for declaring the variables required
					 * by the cast operators.
					 */
					this->oldType_ = variableType;

					AstAttribute * newAttribute = new RetypePersistentAttribute(
							SympleTypeAttribute);
					ROSE_ASSERT(newAttribute != NULL);

					SgType * newVarType;
					if (type_y == "int") {
						newVarType = new SgTypeInt();
					} else if (type_y == "double") {
						newVarType = new SgTypeDouble();
					} else if (type_y == "float") {
						newVarType = new SgTypeFloat;
					} else if (type_y == "char") {
						newVarType = new SgTypeChar;
					} else {
						//we are at a struct type
						SgClassType * classType =
								new SgClassType(
										this->ADTDeclaration->get_firstNondefiningDeclaration());
						SgNamedType * namedType = classType;
						newVarType = namedType;
					}
					newVarType = addRequiredPointersAtType(variableType,
							newVarType);
					newVarType->addNewAttribute("AddedType", newAttribute);
					(*i)->set_type(newVarType);
					//fix the initializer
					SgInitializedName * initName =
							variableDeclaration->get_decl_item(
									(*i)->get_name());
					SgInitializer * currentInitializer =
							initName->get_initializer();
					if (currentInitializer) {
						//nothing here
						// TODO add initializer fixer here!!!
					}
				}
			}
			i++;
		}

		break;
	}
	case V_SgVarRefExp: {
		//in this case we have to change just the declaration
		//so nothing to do here!
		break;
	}
	default: {

		/*
		 * Check if the location informations exists, so if the node
		 * is compiler generated or not!
		 */

		bool compilerGenerated = false;
		Sg_File_Info * currentLocationNode = node->get_file_info();
		if (currentLocationNode
				&& (currentLocationNode->get_raw_filename()
						== "compilerGenerated"
						&& currentLocationNode->get_raw_col() == 0
						&& currentLocationNode->get_raw_line() == 0)) {
			compilerGenerated = true;
		}

		/*
		 * Here we change the operator calls, and add casts as required.
		 * We use the mapping list for this!
		 */
		std::vector<OperatorReplacingMapping*> replacementsVector =
				this->operatorCallMappings_->replacements();
		SgBinaryOp * binaryOp = isSgBinaryOp(node);
		if (binaryOp) {
			/*
			 * First get the location of the node, for locking it
			 * up in the operator replacement maps.
			 */

			Sg_File_Info * currentLocation = binaryOp->get_file_info();

			/*
			 * Now search in the operator replacements the one to replace
			 * the current operator!
			 */
			std::vector<OperatorReplacingMapping*>::iterator currentReplacement;

			if (!(currentLocation->get_raw_filename() == "compilerGenerated"
					&& currentLocation->get_raw_line() == 0
					&& currentLocation->get_raw_col() == 0)
					&& (currentReplacement = find_if(replacementsVector.begin(),
							replacementsVector.end(),
							callLocationEqual(currentLocation)))
							!= replacementsVector.end()) {

				/*
				 * First, construct the final types of arguments,
				 * for checking wheter or not we need the casts.
				 */

				std::vector<std::vector<Types> > typesOfArguments =
						(*currentReplacement)->newExpression()->typesOfArgs();

				/*
				 * Construct the types of current args array.
				 */
				std::vector<Types> typesOfCurrentArgs;
				/*
				 * First get lhs, and rhs.
				 */

				SgExpression * lhsOrig = binaryOp->get_lhs_operand();
				SgExpression * rhsOrig = binaryOp->get_rhs_operand();

				std::string lhsOrigType = getCurrentTypeOfVariable(
						lhsOrig->get_type());
				std::string rhsOrigType = getCurrentTypeOfVariable(
						rhsOrig->get_type());
				/*
				 * Special case here for binary operators, since
				 * strengly ROSE returns the type of its arguments
				 * rather than the type of the return. We look in our
				 * replacement operator structure.
				 */
				if (SgBinaryOp * opTemp = isSgBinaryOp(rhsOrig)) {
					std::vector<OperatorReplacingMapping*>::iterator currentReplacement;
					if (!(currentLocation->get_raw_filename()
							== "compilerGenerated"
							&& currentLocation->get_raw_line() == 0
							&& currentLocation->get_raw_col() == 0)
							&& (currentReplacement = find_if(
									replacementsVector.begin(),
									replacementsVector.end(),
									callLocationEqual(currentLocation)))
									!= replacementsVector.end()) {

						/*
						 * First type is the original one that
						 * does not include casts!
						 */

						if ((*currentReplacement)->initialExpression_->typesOfReturn()[0]
								== T_o) {
							rhsOrigType = this->type_x;
						} else if ((*currentReplacement)->initialExpression_->typesOfReturn()[0]
								== T_n) {
							rhsOrigType = this->type_y;
						} else {
							/*
							 * Nothing here. Just keep whatever type was before.
							 */
						}
					}

				}

				if (SgBinaryOp * opTemp = isSgBinaryOp(lhsOrig)) {
					std::vector<OperatorReplacingMapping*>::iterator currentReplacement;
					if ((currentReplacement = find_if(
							replacementsVector.begin(),
							replacementsVector.end(),
							callLocationEqual(currentLocation)))
							!= replacementsVector.end()) {

						bool isTheSameCall = true;
						if (compilerGenerated) {
							/*
							 * Problem here! We have not any location information.
							 * So, in this case, we compare for now: the name of operator, its string unparsing
							 * TODO: NOT SURE IF THIS IS ALWAYS ENOUGH!!!!!
							 */
							if (((*currentReplacement)->initialExpression_->currentOperatorWithTypes()->getOperatorName()
									!= getVariantName(binaryOp->variantT()))
									|| (binaryOp->unparseToCompleteString()
											!= (*currentReplacement)->initialExpression_->currentOperatorWithTypes()->returnOperatorExpression())) {
								std::cout << node->unparseToCompleteString()
										<< std::endl;
								isTheSameCall = false;
							}

							while (currentReplacement
									!= replacementsVector.end()
									&& !isTheSameCall) {
								currentReplacement = find_if(
										++currentReplacement,
										replacementsVector.end(),
										callLocationEqual(currentLocation));
								if ((currentReplacement
										!= replacementsVector.end())
										&& ((*currentReplacement)->initialExpression_->currentOperatorWithTypes()->getOperatorName()
												== getVariantName(
														binaryOp->variantT()))
										&& (binaryOp->unparseToCompleteString()
												== (*currentReplacement)->initialExpression_->currentOperatorWithTypes()->returnOperatorExpression())) {
									std::cout
											<< node->unparseToCompleteString();
									isTheSameCall = true;
								}

							}

						}

						if (isTheSameCall) {
							/*
							 * Continue here. Otherwise, it was the case that
							 * the location information was missing, for compiler generated nodes.
							 * That were not the same calls actually!!!
							 */

							/*
							 * First type is the original one that
							 * does not include casts!
							 */

							if ((*currentReplacement)->initialExpression_->typesOfReturn()[0]
									== T_o) {
								lhsOrigType = this->type_x;
							} else if ((*currentReplacement)->initialExpression_->typesOfReturn()[0]
									== T_n) {
								lhsOrigType = this->type_y;
							} else {
								/*
								 * Nothing here. Just keep whatever type was before.
								 */
							}
						}

					}
					if (lhsOrigType == this->type_x) {
						typesOfCurrentArgs.push_back(T_o);
					} else if (lhsOrigType == this->type_y) {
						typesOfCurrentArgs.push_back(T_n);
					} else {
						typesOfCurrentArgs.push_back(T_u);
					}

					if (rhsOrigType == this->type_x) {
						typesOfCurrentArgs.push_back(T_o);

					} else if (rhsOrigType == this->type_y) {
						typesOfCurrentArgs.push_back(T_n);
					} else {
						typesOfCurrentArgs.push_back(T_u);
					}

					std::vector<Types> finalTypesOfRetypedProgram;

					int noArg = 0;
					for (std::vector<std::vector<Types> >::iterator itArgs =
							typesOfArguments.begin();
							itArgs != typesOfArguments.end(); ++itArgs) {
						/*
						 * If we have more types for an arg, then
						 * it means that there are to types: that type,
						 * and a cast to it. The first is the type, while
						 * the second one is the cast.
						 */
						if ((*itArgs).size() > 1) {
							/*
							 * First check if we indeed need the casts.
							 * Compare the type of the operator, with
							 * the type of the current argument
							 */
							if ((*itArgs)[0] != typesOfCurrentArgs[noArg]) {
								/*
								 * We need cast! Add the second element in itArgs
								 * in the final types array.
								 */
								finalTypesOfRetypedProgram.push_back(
										(*itArgs)[1]);
							} else {
								/*
								 * We do not need the cast. Just add the normal type.
								 */
								finalTypesOfRetypedProgram.push_back(
										(*itArgs)[0]);
							}
						} else {
							/*
							 * Simply push the type.
							 */
							finalTypesOfRetypedProgram.push_back((*itArgs)[0]);
						}
						noArg++;

					}
					/*
					 * Check if the new name is equal with the old one.
					 * If not, we should retype it. Otherwise, we don't have
					 * to do anything here.
					 */

					if ((*currentReplacement)->oldOperatorName()
							!= (*currentReplacement)->newOperatorName()) {
						/*
						 * Here we should replace the old call with the new one!
						 */
						SgFunctionCallExp * newFctCall =
								addFunctionCallForBinary(binaryOp,
										(*currentReplacement));

						/*
						 * update lhs and rhs expressions, since
						 * add function call for binary might change these two!!!
						 */
						lhsOrig = binaryOp->get_lhs_operand();
						rhsOrig = binaryOp->get_rhs_operand();
						/*
						 * We replaced the entire binary operator, which might already be
						 * in the arguments list, to be passed to the cast fixer.
						 * So, parse that list, and replace the binaryOp with newFctCall!!!
						 *
						 * This is quire inefficient! TODO: find a more efficient way
						 */
						for (std::vector<std::vector<SgExpression *> >::iterator itArgList =
								argumentsForCasts.begin();
								itArgList != argumentsForCasts.end();
								++itArgList) {
							for (std::vector<SgExpression*>::iterator itArguments =
									(*itArgList).begin();
									itArguments != (*itArgList).end();
									++itArguments) {
								if ((*itArguments) == binaryOp) {
									(*itArguments) = newFctCall;
								}
							}
						}

					} else {
						/*
						 * Nothing to do here!
						 */

						std::cout << "GASIT:: "
								<< binaryOp->unparseToCompleteString()
								<< std::endl;
						std::cout << "REPLACEMENT::  "
								<< (*currentReplacement)->newOperatorName()
								<< std::endl;

					}
					/*
					 * Finally, add the casts informations
					 */
					SgStatement * enclosingStatement =
							SageInterface::getEnclosingStatement(binaryOp);
					this->enclosingStatementsForCasts_.push_back(
							enclosingStatement);

					this->typesForCasts_.push_back(finalTypesOfRetypedProgram);

					std::vector<SgExpression*> arrayOfCurrentArgs;
					arrayOfCurrentArgs.push_back(lhsOrig);
					arrayOfCurrentArgs.push_back(rhsOrig);

					this->argumentsForCasts.push_back(arrayOfCurrentArgs);
				}
			}

		} else {
			SgFunctionCallExp * functionCall = isSgFunctionCallExp(node);
			if (functionCall) {

				/*
				 * First get the location of the node, for locking it
				 * up in the operator replacement maps.
				 */

				Sg_File_Info * currentLocation = functionCall->get_file_info();

				/*
				 * Now search in the operator replacements the one to replace
				 * the current operator!
				 */
				std::vector<OperatorReplacingMapping*>::iterator currentReplacement;
				if ((currentReplacement = find_if(replacementsVector.begin(),
						replacementsVector.end(),
						callLocationEqual(currentLocation)))
						!= replacementsVector.end()) {

					bool isTheSameCall = true;
					if (compilerGenerated) {
						/*
						 * Problem here! We have not any location information.
						 * So, in this case, we compare for now: the name of operator, its string unparsing
						 * TODO: NOT SURE IF THIS IS ALWAYS ENOUGH!!!!!
						 */
						if (((*currentReplacement)->initialExpression_->currentOperatorWithTypes()->getOperatorName()
								!= isSgFunctionRefExp(
										functionCall->get_function())->get_symbol()->get_name().str())
								|| (functionCall->unparseToCompleteString()
										!= (*currentReplacement)->initialExpression_->currentOperatorWithTypes()->returnOperatorExpression())) {
							std::cout << node->unparseToCompleteString()
									<< std::endl;
							isTheSameCall = false;
						}

						while (currentReplacement != replacementsVector.end()
								&& !isTheSameCall) {
							currentReplacement = find_if(++currentReplacement,
									replacementsVector.end(),
									callLocationEqual(currentLocation));
							if ((currentReplacement != replacementsVector.end())
									&& ((*currentReplacement)->initialExpression_->currentOperatorWithTypes()->getOperatorName()
											== isSgFunctionRefExp(
													functionCall->get_function())->get_symbol()->get_name().str())
									&& (functionCall->unparseToCompleteString()
											== (*currentReplacement)->initialExpression_->currentOperatorWithTypes()->returnOperatorExpression())) {
								std::cout << node->unparseToCompleteString();
								isTheSameCall = true;
							}

						}

					}

					if (isTheSameCall) {
						/*
						 * Continue here. Otherwise, it was the case that
						 * the location information was missing, for compiler generated nodes.
						 * That were not the same calls actually!!!
						 */

						if ((*currentReplacement)->oldOperatorName()
								!= (*currentReplacement)->newOperatorName()) {
							/*
							 * Here we should replace the old call with the new one!
							 */
							std::string originalCallName =
									(*currentReplacement)->oldOperatorName();
							std::string newCallName =
									(*currentReplacement)->newOperatorName();
							SgFunctionCallExp* newFctCallRetyped =
									renameFunctionCall(functionCall,
											*currentReplacement);
							/*
							 * We replaced the entire binary operator, which might already be
							 * in the arguments list, to be passed to the cast fixer.
							 * So, parse that list, and replace the binaryOp with newFctCall!!!
							 *
							 * This is quire inefficient! TODO: find a more efficient way
							 */
							for (std::vector<std::vector<SgExpression *> >::iterator itArgList =
									argumentsForCasts.begin();
									itArgList != argumentsForCasts.end();
									++itArgList) {
								for (std::vector<SgExpression*>::iterator itArguments =
										(*itArgList).begin();
										itArguments != (*itArgList).end();
										++itArguments) {
									if ((*itArguments) == binaryOp) {
										(*itArguments) = newFctCallRetyped;
									}
								}
							}
						}

						/*
						 * Construct the list of arguments in the operator to replace the current
						 * one, and the list of arguments in the current operator!!!
						 */
						std::vector<std::vector<Types> > typesOfArguments =
								(*currentReplacement)->newExpression()->typesOfArgs();

						std::vector<Types> typesOfCurrentArgs;
						/*!
						 * The list of parameters of the current function.
						 */
						SgExprListExp * actualArguments =
								functionCall->get_args();
						SgExpressionPtrList& actualArgList =
								actualArguments->get_expressions();
						std::vector<SgExpression*> arrayOfCurrentArgs;

						/*!
						 * iterate ober the list of formal args.
						 */
						for (SgExpressionPtrList::iterator it =
								actualArgList.begin();
								it != actualArgList.end(); it++) {
							/*
							 * First push the argument in the array of argument,
							 * for cast fixings!
							 */
							arrayOfCurrentArgs.push_back(*it);

							/*!
							 * Push the current type in the list of args types.
							 */

							SgType * currentType = (*it)->get_type();
							std::string currentStringname =
									getCurrentTypeOfVariable(currentType);
							if (currentStringname == this->type_x) {
								typesOfCurrentArgs.push_back(T_o);
							} else if (currentStringname == this->type_y) {
								typesOfCurrentArgs.push_back(T_n);
							} else {
								typesOfCurrentArgs.push_back(T_u);
							}
						}
						std::vector<Types> finalTypesOfRetypedProgram;
						int noArg = 0;
						for (std::vector<std::vector<Types> >::iterator itArgs =
								typesOfArguments.begin();
								itArgs != typesOfArguments.end(); ++itArgs) {
							/*
							 * If we have more types for an arg, then
							 * it means that there are to types: that type,
							 * and a cast to it. The first is the type, while
							 * the second one is the cast.
							 */
							if ((*itArgs).size() > 1) {
								/*
								 * First check if we indeed need the casts.
								 * Compare the type of the operator, with
								 * the type of the current argument
								 */
								if ((*itArgs)[0] != typesOfCurrentArgs[noArg]) {
									/*
									 * We need cast! Add the second element in itArgs
									 * in the final types array.
									 */
									finalTypesOfRetypedProgram.push_back(
											(*itArgs)[1]);
								} else {
									/*
									 * We do not need the cast. Just add the normal type.
									 */
									finalTypesOfRetypedProgram.push_back(
											(*itArgs)[0]);
								}
							} else {
								/*
								 * Simply push the type.
								 */
								finalTypesOfRetypedProgram.push_back(
										(*itArgs)[0]);
							}
							noArg++;
						}
						/*
						 * Finally, add the casts informations
						 */

						SgStatement * enclosingStatementCall =
								SageInterface::getEnclosingStatement(
										functionCall);
						this->enclosingStatementsForCasts_.push_back(
								enclosingStatementCall);
						this->typesForCasts_.push_back(
								finalTypesOfRetypedProgram);
						this->argumentsForCasts.push_back(arrayOfCurrentArgs);
					}

				} else {
					/*
					 * Nothing here!
					 */
				}

			} else {

				/*
				 * Unary operators!!!
				 */
				SgUnaryOp * unaryOp = isSgUnaryOp(node);
				if (unaryOp) {

					/*
					 * First get the location of the node, for locking it
					 * up in the operator replacement maps.
					 */

					Sg_File_Info * currentLocation = unaryOp->get_file_info();
					std::cout << unaryOp->unparseToCompleteString()
							<< std::endl;
					/*
					 * Now search in the operator replacements the one to replace
					 * the current operator!
					 */
					std::vector<OperatorReplacingMapping*>::iterator currentReplacement;
					/*
					 !(currentLocation->get_raw_filename()
					 == "compilerGenerated"
					 && currentLocation->get_raw_line() == 0
					 && currentLocation->get_raw_col() == 0)
					 &&
					 */
					if ((currentReplacement = find_if(
							replacementsVector.begin(),
							replacementsVector.end(),
							callLocationEqual(currentLocation)))
							!= replacementsVector.end()) {

						bool isTheSameCall = true;
						if (compilerGenerated) {
							/*
							 * Problem here! We have not any location information.
							 * So, in this case, we compare for now: the name of operator, its string unparsing
							 * TODO: NOT SURE IF THIS IS ALWAYS ENOUGH!!!!!
							 */
							if (((*currentReplacement)->initialExpression_->currentOperatorWithTypes()->getOperatorName()
									!= getVariantName(unaryOp->variantT()))
									|| (unaryOp->unparseToCompleteString()
											!= (*currentReplacement)->initialExpression_->currentOperatorWithTypes()->returnOperatorExpression())) {
								std::cout << node->unparseToCompleteString()
										<< std::endl;
								isTheSameCall = false;
							}

							while (currentReplacement
									!= replacementsVector.end()
									&& !isTheSameCall) {
								currentReplacement = find_if(
										++currentReplacement,
										replacementsVector.end(),
										callLocationEqual(currentLocation));
								if ((currentReplacement
										!= replacementsVector.end())
										&& ((*currentReplacement)->initialExpression_->currentOperatorWithTypes()->getOperatorName()
												== getVariantName(
														unaryOp->variantT()))
										&& (unaryOp->unparseToCompleteString()
												== (*currentReplacement)->initialExpression_->currentOperatorWithTypes()->returnOperatorExpression())) {
									std::cout
											<< node->unparseToCompleteString();
									isTheSameCall = true;
								}

							}

						}

						if (isTheSameCall) {
							/*
							 * Continue here. Otherwise, it was the case that
							 * the location information was missing, for compiler generated nodes.
							 * That were not the same calls actually!!!
							 */

							if ((*currentReplacement)->oldOperatorName()
									!= (*currentReplacement)->newOperatorName()) {
								/*
								 * TODO: FIX HERE! We should replace the old
								 * operator!
								 */
								std::cout
										<< "Not supported for now unary operator name change!";
								assert(false);
							}

							SgExpression * currentOperand =
									unaryOp->get_operand();

							/*
							 * Construct the list of arguments in the operator to replace the current
							 * one, and the list of arguments in the current operator!!!
							 */
							std::vector<std::vector<Types> > typesOfArguments =
									(*currentReplacement)->newExpression()->typesOfArgs();

#if 1

							std::cout << "TYPES OF ARGS!!!!!!!!!!!!!: ";
							for (int ii = 0; ii < typesOfArguments[0].size();
									ii++) {
								std::cout << typesOfArguments[0][ii] << "; ";
							}
							std::cout << std::endl;

							std::cout
									<< *((*currentReplacement)->newExpression())
									<< std::endl;

#endif

							std::vector<Types> typesOfCurrentArgs;

							std::vector<SgExpression*> arrayOfCurrentArgs;

							/*
							 * First push the argument in the array of argument,
							 * for cast fixings!
							 */
							arrayOfCurrentArgs.push_back(currentOperand);

							/*!
							 * Push the current type in the list of args types.
							 */

							SgType * currentType = currentOperand->get_type();
							std::string currentStringname =
									getCurrentTypeOfVariable(currentType);
							if (currentStringname == this->type_x) {
								typesOfCurrentArgs.push_back(T_o);
							} else if (currentStringname == this->type_y) {
								typesOfCurrentArgs.push_back(T_n);
							} else {
								typesOfCurrentArgs.push_back(T_u);
							}

							std::vector<Types> finalTypesOfRetypedProgram;
							int noArg = 0;
							for (std::vector<std::vector<Types> >::iterator itArgs =
									typesOfArguments.begin();
									itArgs != typesOfArguments.end();
									++itArgs) {
								/*
								 * If we have more types for an arg, then
								 * it means that there are to types: that type,
								 * and a cast to it. The first is the type, while
								 * the second one is the cast.
								 */
								if ((*itArgs).size() > 1) {
									/*
									 * First check if we indeed need the casts.
									 * Compare the type of the operator, with
									 * the type of the current argument
									 */
									if ((*itArgs)[0]
											!= typesOfCurrentArgs[noArg]) {
										/*
										 * We need cast! Add the second element in itArgs
										 * in the final types array.
										 */
										finalTypesOfRetypedProgram.push_back(
												(*itArgs)[1]);
									} else {
										/*
										 * We do not need the cast. Just add the normal type.
										 */
										finalTypesOfRetypedProgram.push_back(
												(*itArgs)[0]);
									}
								} else {
									/*
									 * Simply push the type.
									 */
									finalTypesOfRetypedProgram.push_back(
											(*itArgs)[0]);
								}
								noArg++;
							}

							/*
							 * Finally, add the casts informations
							 */

							SgStatement * enclosingStatementCall =
									SageInterface::getEnclosingStatement(
											unaryOp);
							this->enclosingStatementsForCasts_.push_back(
									enclosingStatementCall);
							this->typesForCasts_.push_back(
									finalTypesOfRetypedProgram);
							this->argumentsForCasts.push_back(
									arrayOfCurrentArgs);
						}
					}

				} else {
					/*
					 * Nothing here. We just change functions calls: binary operators,
					 * n--arry, and unary operators!
					 */
				}
			}
		}

		break;
	}
	}
}

ASTRetypeTraversal::ASTRetypeTraversal() {
	// constructor with no parameters
	//just initialize all retypes to true and the type_x and type_y to empty
	this->retypeFunctionParameters = true;
	this->retypeFunctionReturn = true;
	this->type_x = "";
	this->type_y = "";
	this->ADTDeclaration = NULL;
	this->operatorCallMappings_ = NULL;
}

ASTRetypeTraversal::ASTRetypeTraversal(SgClassDeclaration* ADTDeclaration,
		std::string orginalType, std::string newType,
		std::vector<std::string> definedSymbols,
		CollectionOfOperatorReplacement* operatorCallMappings,
		std::string castToOld, std::string castToNew, bool retypeFunctionReturn,
		bool retypeFunctionParameters) {

	this->castToOld_ = castToOld;
	this->castToNew_ = castToNew;

	this->type_x = orginalType;
	this->type_y = newType;

	this->oldType_ = NULL;

	this->retypeFunctionReturn = retypeFunctionReturn;
	this->retypeFunctionParameters = retypeFunctionParameters;

	this->ADTDeclaration = ADTDeclaration;

	this->definedSymbolsList = definedSymbols;

	this->operatorCallMappings_ = operatorCallMappings;

	AstAttribute * newAttribute = new RetypePersistentAttribute(
			SympleTypeAttribute);
	ROSE_ASSERT(newAttribute != NULL);

	if (type_y == "int") {
		this->newType_ = new SgTypeInt();
	} else if (type_y == "double") {
		this->newType_ = new SgTypeDouble();
	} else if (type_y == "float") {
		this->newType_ = new SgTypeFloat;
	} else if (type_y == "char") {
		this->newType_ = new SgTypeChar;
	} else {

		SgClassType * classType = new SgClassType(
				this->ADTDeclaration->get_firstNondefiningDeclaration());
		SgNamedType * namedType = classType;
		this->newType_ = namedType;

	}
	this->newType_->addNewAttribute("AddedType", newAttribute);

}

std::string ASTRetypeTraversal::getTypeX() {
	return this->type_x;
}

void ASTRetypeTraversal::setTypeX(std::string originalType) {
	this->type_x = originalType;
}

std::string ASTRetypeTraversal::getTypeY() {
	return type_y;
}

void ASTRetypeTraversal::setTypeY(std::string newType) {
	this->type_y = newType;
}

void ASTRetypeTraversal::fixCastOperators() {

	/*
	 * First construct the structures for the new
	 * type, and old type.
	 */

	/*
	 * The new type.
	 */

	SgType * oldVarType = this->oldType_;

	std::vector<CastInformation*> allRequiredCasts;

	int index = 0;
	for (std::vector<SgStatement*>::iterator it =
			this->enclosingStatementsForCasts_.begin();
			it != this->enclosingStatementsForCasts_.end(); ++it) {

#if 1
		std::cout << "INDEX: " << index << std::endl;
		std::cout << "ENCLOSING STATEMENT: " << (*it)->unparseToCompleteString()
				<< std::endl;
		std::cout << "Arguments for casts: " << std::endl << "         ";
		for (int i = 0; i < this->argumentsForCasts[index].size(); i++) {
			std::cout << "Argument no: " << i << ": "
					<< this->argumentsForCasts[index][i]->unparseToCompleteString()
					<< std::endl;
		}
		std::cout << "Types of args for casts: " << std::endl << "         ";
		for (int i = 0; i < this->typesForCasts_[index].size(); i++) {
			std::cout << "Type no: " << i << ": "
					<< this->typesForCasts_[index][i] << std::endl;
		}
		std::cout << std::endl << std::endl << std::endl << std::endl;
#endif

		CastInformation * newCastInfo = new CastInformation((*it),
				this->typesForCasts_[index], this->argumentsForCasts[index],
				this->castToNew_, this->castToOld_, oldType_, newType_);

		allRequiredCasts.push_back(newCastInfo);

		index++;
	}

	for (std::vector<CastInformation*>::iterator it = allRequiredCasts.begin();
			it != allRequiredCasts.end(); ++it) {
		(*it)->constructRequiredCasts();
	}

#if 0
	//TEMPORARY CHECK ADD SOMETHING BEFORE/ AFTER CURRENT NODE!

	/*
	 * First we need to get the statement containing the current binary expression.
	 */

#if 0
	std::cout
	<< SageInterface::getEnclosingStatement(binaryOp)->unparseToCompleteString()
	<< std::endl;
#endif

	for (std::vector<SgStatement*>::iterator it =
			this->enclosingStatementsForCasts_.begin();
			it != this->enclosingStatementsForCasts_.end(); ++it) {

		SgStatement * enclosingStatement = (*it);

		/*
		 * Next we need the block containing the current enclosing statement.
		 */
		SgScopeStatement * currentScope = SageInterface::getEnclosingScope(
				enclosingStatement);

		//SgExprStatement* assignStmt = buildAssignStatement(buildVarRefExp("i"),
		//		buildIntVal(9));

		//insertStatementBefore(enclosingStatement, assignStmt);

		//std::cout << (binaryOp->unparseToCompleteString()) << std::endl;
		//std::cout << (enclosingStatement->unparseToCompleteString())
		//		<< std::endl;
		//std::cout << (assignStmt->unparseToCompleteString()) << std::endl;

#if 1
		// Mark this as a transformation (required)
		Sg_File_Info* sourceLocation =
		Sg_File_Info::generateDefaultFileInfoForTransformationNode();
		ROSE_ASSERT(sourceLocation != NULL);

		SgType* type = new SgTypeInt();
		ROSE_ASSERT(type != NULL);

		SgName name = "newVariable";

		SgVariableDeclaration* variableDeclaration = new SgVariableDeclaration(
				sourceLocation, name, type);
		ROSE_ASSERT(variableDeclaration != NULL);

		SgInitializedName* initializedName =
		*(variableDeclaration->get_variables().begin());
		initializedName->set_file_info(
				Sg_File_Info::generateDefaultFileInfoForTransformationNode());

		SgBasicBlock * block = isSgBasicBlock(enclosingStatement->get_parent());

		// DQ (6/18/2007): The unparser requires that the scope be set (for name qualification to work).
		initializedName->set_scope(block);

		// Liao (2/13/2008): AstTests requires this to be set
		variableDeclaration->set_firstNondefiningDeclaration(
				variableDeclaration);

		ROSE_ASSERT(block->get_statements().size() > 0);

		SgStatementPtrList::iterator itToEnclosingStatement;

		for (SgStatementPtrList::iterator itStatements =
				block->get_statements().begin();
				itStatements != block->get_statements().end(); itStatements++) {
			if ((*itStatements) == enclosingStatement) {
				itToEnclosingStatement = itStatements;
			}
		}
		//++itToEnclosingStatement;

		block->get_statements().insert(itToEnclosingStatement,
				variableDeclaration);
		variableDeclaration->set_parent(block);

		// Add a symbol to the sybol table for the new variable
		SgVariableSymbol* variableSymbol = new SgVariableSymbol(
				initializedName);
		block->insert_symbol(name, variableSymbol);

#endif
	}
#endif
}

}
