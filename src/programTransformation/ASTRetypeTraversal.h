/*
 * ASTRetypeTraversal.h
 *
 *  Created on: 9 Dec 2014
 *      Author: alex
 */

#ifndef SRC_ASTRETYPETRAVERSAL_H_
#define SRC_ASTRETYPETRAVERSAL_H_

#include <rose.h>

#include <boost/serialization/vector.hpp>
#include <ConstraintError.h>
#include <OperatorReplacingMapping.h>

#include <string>

using namespace analyzer;

//this traversal will change the types of variable declaration
//decorating the RAST (ROSE Ast) according to the modification

namespace retyper {

class CastInformation {
public:
	CastInformation() {
		//id++;
	}
	CastInformation(SgStatement* enclosingStatement,
			std::vector<Types> requiredCasts,
			std::vector<SgExpression*> actualArgs, std::string castToNewType,
			std::string castToOldType, SgType * oldType, SgType * newType) {
		requiredCasts_ = requiredCasts;
		actualArgs_ = actualArgs;
		castToNewType_ = castToNewType;
		castToOldType_ = castToOldType;
		enclosingStatement_ = enclosingStatement;
		oldType_ = oldType;
		newType_ = newType;
		//id++;
	}

	void addCallToCast() {

	}

	SgExprStatement* addCastAfterExpression(std::string argName, SgType* type,
			SgExpression * initializerCast) {

		SgStatement * enclosingStatement = enclosingStatement_;

		/*
		 * Next we need the block containing the current enclosing statement.
		 */
		SgScopeStatement * currentScope = SageInterface::getEnclosingScope(
				enclosingStatement);

		//SgExprStatement* assignStmt = buildAssignStatement(buildVarRefExp("i"),
		//		buildIntVal(9));

		//insertStatementBefore(enclosingStatement, assignStmt);

		// Mark this as a transformation (required)
		Sg_File_Info* sourceLocation =
				Sg_File_Info::generateDefaultFileInfoForTransformationNode();
		ROSE_ASSERT(sourceLocation != NULL);

		ROSE_ASSERT(type != NULL);

		SgName name = argName;

		SgBasicBlock * block = isSgBasicBlock(enclosingStatement->get_parent());

		SgNode * currentEnclosingStatement = enclosingStatement;
		while (!block) {
			currentEnclosingStatement = currentEnclosingStatement->get_parent();
			block = isSgBasicBlock(currentEnclosingStatement->get_parent());
		}

		SgStatementPtrList::iterator itToEnclosingStatement;

		for (SgStatementPtrList::iterator itStatements =
				block->get_statements().begin();
				itStatements != block->get_statements().end(); itStatements++) {
			if ((*itStatements) == currentEnclosingStatement) {
				itToEnclosingStatement = itStatements;
			}
		}
		++itToEnclosingStatement;

		//SageBuilder::buildAssignStatement()

		//SgAssign

		SgExprStatement * newAssignStm = SageBuilder::buildAssignStatement(
				SageBuilder::buildVarRefExp(argName,
						enclosingStatement_->get_scope()), initializerCast);

		block->get_statements().insert(itToEnclosingStatement, newAssignStm);
		newAssignStm->set_parent(block);

		/*
		 * return the newly generated symbol, for
		 * replacing it in the original call!
		 */
		return newAssignStm;

	}

	SgVariableSymbol* addDeclarationWithInitializer(std::string argName,
			SgType* type, SgExpression * initializerCast) {

		SgStatement * enclosingStatement = enclosingStatement_;

		/*
		 * Next we need the block containing the current enclosing statement.
		 */
		SgScopeStatement * currentScope = SageInterface::getEnclosingScope(
				enclosingStatement);

		//SgExprStatement* assignStmt = buildAssignStatement(buildVarRefExp("i"),
		//		buildIntVal(9));

		//insertStatementBefore(enclosingStatement, assignStmt);

		// Mark this as a transformation (required)
		Sg_File_Info* sourceLocation =
				Sg_File_Info::generateDefaultFileInfoForTransformationNode();
		ROSE_ASSERT(sourceLocation != NULL);

		ROSE_ASSERT(type != NULL);

		SgName name = argName;

		SgVariableDeclaration* variableDeclaration = new SgVariableDeclaration(
				sourceLocation, name, type);
		ROSE_ASSERT(variableDeclaration != NULL);

		SgInitializedName* initializedName =
				*(variableDeclaration->get_variables().begin());
		initializedName->set_file_info(
				Sg_File_Info::generateDefaultFileInfoForTransformationNode());

		/*
		 * Here we need to add a declaration with the initializer.
		 * Thus, we have to identify the block, which  for sure should exists!
		 */

		SgBasicBlock * block = isSgBasicBlock(enclosingStatement->get_parent());

		SgNode * currentEnclosingStatement = enclosingStatement;
		while (!block) {
			currentEnclosingStatement = currentEnclosingStatement->get_parent();
			block = isSgBasicBlock(currentEnclosingStatement->get_parent());
		}

		// DQ (6/18/2007): The unparser requires that the scope be set (for name qualification to work).
		initializedName->set_scope(block);

		SgInitializer * myInit = SageBuilder::buildAssignInitializer(
				initializerCast, type);
		initializedName->set_initializer(myInit);

		// Liao (2/13/2008): AstTests requires this to be set
		variableDeclaration->set_firstNondefiningDeclaration(
				variableDeclaration);

		ROSE_ASSERT(block->get_statements().size() > 0);

		SgStatementPtrList::iterator itToEnclosingStatement;

		for (SgStatementPtrList::iterator itStatements =
				block->get_statements().begin();
				itStatements != block->get_statements().end(); itStatements++) {
			if ((*itStatements) == currentEnclosingStatement) {
				itToEnclosingStatement = itStatements;
			}
		}
		//++itToEnclosingStatement;

		block->get_statements().insert(itToEnclosingStatement,
				variableDeclaration);
		variableDeclaration->set_parent(block);

		// Add a symbol to the sybol table for the new variable
		SgVariableSymbol* variableSymbol = new SgVariableSymbol(
				initializedName);
		block->insert_symbol(name, variableSymbol);

		/*
		 * return the newly generated symbol, for
		 * replacing it in the original call!
		 */
		return variableSymbol;

	}

	SgFunctionCallExp* addCastCopy(std::string argName, SgType* type,
			std::string castInputName, std::string castFctName) {

		/*
		 * Note::: When calling this method, the operand should be a variable reference!!!!
		 */

		SgStatement * enclosingStatement = enclosingStatement_;
		SgName name1(castFctName);
		SgFunctionDeclaration *decl_1 =
				SageBuilder::buildNondefiningFunctionDeclaration(name1, type,
						SageBuilder::buildFunctionParameterList(
								SageBuilder::buildFunctionParameterTypeList(
										type)),
						enclosingStatement_->get_scope());

		((decl_1->get_declarationModifier()).get_storageModifier()).setExtern();

		SgFunctionCallExp * callStmt_1 = SageBuilder::buildFunctionCallExp(
				name1, type,
				SageBuilder::buildExprListExp(
						SageBuilder::buildVarRefExp(castInputName,
								enclosingStatement_->get_scope())),
				enclosingStatement_->get_scope());
		return callStmt_1;
	}

	SgFunctionCallExp* addCast(std::string argName, SgType* type,
			SgExpression * operand, std::string castFctName) {
		SgStatement * enclosingStatement = enclosingStatement_;
		SgBasicBlock * block = isSgBasicBlock(enclosingStatement->get_parent());
		SgName name1(castFctName);
		// It is up to the user to link the implementations of these functions link time
		SgFunctionDeclaration *decl_1 =
				SageBuilder::buildNondefiningFunctionDeclaration(name1, type,
						SageBuilder::buildFunctionParameterList(
								SageBuilder::buildFunctionParameterTypeList(
										type)), block);

		((decl_1->get_declarationModifier()).get_storageModifier()).setExtern();
		/*
		 * Keep a reference to the current parent of the operand,
		 * for fixing it as the parent of the call function expression!
		 */
		SgNode * currentOperandParent = operand->get_parent();
		SgFunctionCallExp * callStmt_1 = SageBuilder::buildFunctionCallExp(
				name1, type, SageBuilder::buildExprListExp(operand),
				enclosingStatement_->get_scope());
		/*
		 * Now fix the parent of the function call.
		 */
		callStmt_1->set_parent(currentOperandParent);
		return callStmt_1;
	}

	void constructRequiredCasts() {
		int index = 0;
		for (std::vector<Types>::iterator it = requiredCasts_.begin();
				it != requiredCasts_.end(); ++it) {


#if 1
			SgUnaryOp * parentUnaryOpTTT = isSgUnaryOp(
					this->actualArgs_[index]->get_parent());
			if(parentUnaryOpTTT){
			std::cout << parentUnaryOpTTT->unparseToCompleteString() << std::endl;
			}
#endif


			/*
			 * Here we have 3 cases: cast to new type, cast to old type, or cast
			 * is not required!
			 */
			if ((*it) == T_n_c_y) {
				/*
				 * Cast to the new type.
				 * Add before, and after the statement,
				 * the corresponding casts.
				 */

				/*
				 * Check the type of argument. Only for variables
				 * we have to add the variable declaration. Otherwise,
				 * we just cast in the actual argument!
				 */

				SgVarRefExp * varRef = isSgVarRefExp(this->actualArgs_[index]);

				if (varRef) {
					std::string argName = varRef->get_symbol()->get_name();

					std::string result;
					std::ostringstream convert;
					convert << CastInformation::id;
					result = convert.str();

					SgExpression * castInitializer = addCastCopy("dada",
							newType_,
							this->actualArgs_[index]->unparseToCompleteString(),
							this->castToNewType_);

					argName += result;
					CastInformation::id++;
					SgVariableSymbol* newSymbol = addDeclarationWithInitializer(
							argName, oldType_, castInitializer);

					SgExpression * castAfterExpt = addCastCopy("dada", newType_,
							argName, this->castToOldType_);

					addCastAfterExpression(
							this->actualArgs_[index]->unparseToCompleteString(),
							newType_, castAfterExpt);

				} else {

					/*
					 * Not in a variable reference case!
					 * Here, just add a cast arroung the parameter
					 */

					/*
					 * First construct the cast string
					 */

					/*
					 * Get the reference to the parents of the curent arg here.
					 * The call to the addCast function messes up the parents here!!!!
					 */
					SgExprListExp * parentNode = isSgExprListExp(
							this->actualArgs_[index]->get_parent());
					SgBinaryOp * parentBinOp = isSgBinaryOp(
							this->actualArgs_[index]->get_parent());
					SgReturnStmt * parentReturnStm = isSgReturnStmt(
							this->actualArgs_[index]->get_parent());
					SgUnaryOp * parentUnaryOp = isSgUnaryOp(
							this->actualArgs_[index]->get_parent());

#if 0
					SgNode * parentNodeUnknownTypeInit =
					this->actualArgs_[index]->get_parent();

					std::cout << "Unknown Node of Type BEFORE EXPRESSION CONSTRUCTION!: "
					<< getVariantName(
							parentNodeUnknownTypeInit->variantT())
					<< std::endl;
#endif
					//std::cout << this->actualArgs_[index]->unparseToCompleteString() << std::endl;

					SgExpression * myExpr = isSgExpression(
							this->addCast("dada", newType_,
									this->actualArgs_[index],
									this->castToNewType_));

					if (parentNode) {
						SgExpressionPtrList::iterator itForCurrent;
						for (SgExpressionPtrList::iterator itExpr =
								parentNode->get_expressions().begin();
								itExpr != parentNode->get_expressions().end();
								++itExpr) {
							if ((*itExpr) == this->actualArgs_[index]) {
								*itExpr = myExpr;
								std::cout << "NEW EXP" << std::endl;
								std::cout << myExpr->unparseToCompleteString()
										<< std::endl;
							}
						}

					} else {

						/*
						 * Binary Op Case:
						 */

						if (parentBinOp) {

							/*
							 * first check if the operand to be casted is lhs or rhs
							 */
							if (parentBinOp->get_rhs_operand()
									== this->actualArgs_[index]) {
								parentBinOp->set_rhs_operand(myExpr);
							} else if (parentBinOp->get_lhs_operand()
									== this->actualArgs_[index]) {
								parentBinOp->set_lhs_operand(myExpr);
							} else {
								/*
								 * We should never be here!
								 */
								assert(false);
							}

						} else {

							/*
							 * Return statement!
							 */

							if (parentReturnStm) {
								parentReturnStm->set_expression(myExpr);
							} else {

								/*
								 * Unary operator!
								 */
								if (parentUnaryOp) {
									parentUnaryOp->set_operand(myExpr);
								} else {

									/*
									 * TODO: Check here what else it could be! Not supported for now...
									 */

									SgNode * parentNodeUnknownType =
											this->actualArgs_[index]->get_parent();

									std::cout << "Unknown Node of Type: "
											<< getVariantName(
													parentNodeUnknownType->variantT())
											<< std::endl;

									std::cout << "ORIGINAL EXPRESSION: "
											<< this->actualArgs_[index]->unparseToCompleteString()
											<< std::endl;
									std::cout << "NEW EXPRESSION: "
											<< myExpr->unparseToCompleteString()
											<< std::endl;

									assert(false);
								}
							}
						}
					}

				}

			} else if ((*it) == T_n_c_x) {
				SgVarRefExp * varRef = isSgVarRefExp(this->actualArgs_[index]);

				if (varRef) {
					std::string argName = varRef->get_symbol()->get_name();

					std::string result;
					std::ostringstream convert;
					convert << CastInformation::id;
					result = convert.str();

					argName += result;
					CastInformation::id++;

					SgExpression * castInitializer = addCastCopy("dada",
							oldType_,
							this->actualArgs_[index]->unparseToCompleteString(),
							this->castToOldType_);

					SgVariableSymbol* newSymbol = addDeclarationWithInitializer(
							argName, oldType_, castInitializer);

					SgExpression * castAfterExpt = addCastCopy("dada", oldType_,
							argName, this->castToNewType_);

					addCastAfterExpression(
							this->actualArgs_[index]->unparseToCompleteString(),
							oldType_, castAfterExpt);

					varRef->set_symbol(newSymbol);
				} else {
					/*
					 * Not in a variable reference case!
					 * Here, just add a cast arroung the parameter
					 */

					/*
					 * First construct the cast string
					 */

					/*
					 * Get the reference to the parents of the curent arg here.
					 * The call to the addCast function messes up the parents here!!!!
					 */
					SgExprListExp * parentNode = isSgExprListExp(
							this->actualArgs_[index]->get_parent());
					SgBinaryOp * parentBinOp = isSgBinaryOp(
							this->actualArgs_[index]->get_parent());
					SgUnaryOp * parentUnaryOp = isSgUnaryOp(
							this->actualArgs_[index]->get_parent());
					SgReturnStmt * parentReturnStm = isSgReturnStmt(
							this->actualArgs_[index]->get_parent());

					SgNode * initialParentNode =
							this->actualArgs_[index]->get_parent();

					std::cout << "INITIAL TYPE OF PARENT: "
							<< getVariantName(initialParentNode->variantT())
							<< std::endl;

					SgExpression * myExpr = isSgExpression(
							this->addCast("dada", oldType_,
									this->actualArgs_[index],
									this->castToOldType_));

					if (parentNode) {
						SgExpressionPtrList::iterator itForCurrent;
						for (SgExpressionPtrList::iterator itExpr =
								parentNode->get_expressions().begin();
								itExpr != parentNode->get_expressions().end();
								++itExpr) {
							if ((*itExpr) == this->actualArgs_[index]) {
								*itExpr = myExpr;
								std::cout << "NEW EXP" << std::endl;
								std::cout << myExpr->unparseToCompleteString()
										<< std::endl;
							}
						}

					} else {

						/*
						 * Binary Op Case:
						 */

						if (parentBinOp) {

							/*
							 * first check if the operand to be casted is lhs or rhs
							 */

							std::cout << "Parent: "
									<< parentBinOp->unparseToCompleteString()
									<< std::endl;
							std::cout << "Node: "
									<< this->actualArgs_[index]->unparseToCompleteString()
									<< std::endl;
							std::cout << "Replacement: "
									<< myExpr->unparseToCompleteString()
									<< std::endl;

							if (parentBinOp->get_rhs_operand()
									== this->actualArgs_[index]) {
								parentBinOp->set_rhs_operand(myExpr);
							} else if (parentBinOp->get_lhs_operand()
									== this->actualArgs_[index]) {
								parentBinOp->set_lhs_operand(myExpr);
							} else {
								/*
								 * We should never be here!
								 */
								assert(false);
							}

						} else {

							/*
							 * Return statement!
							 */

							if (parentReturnStm) {
								parentReturnStm->set_expression(myExpr);
							} else {

								/*
								 * Unary operator!
								 */
								if (parentUnaryOp) {
									parentUnaryOp->set_operand(myExpr);
								} else {
									/*
									 * TODO: Check here what else it could be! Not supported for now...
									 */

									SgNode * parentNodeUnknownType =
											initialParentNode;

									std::cout << "Unknown Node of Type: "
											<< getVariantName(
													parentNodeUnknownType->variantT())
											<< std::endl;

									std::cout << "ORIGINAL EXPRESSION: "
											<< this->actualArgs_[index]->unparseToCompleteString()
											<< std::endl;
									std::cout << "NEW EXPRESSION: "
											<< myExpr->unparseToCompleteString()
											<< std::endl;

									assert(false);
								}
							}
						}
					}
				}
			} else {
				/*
				 * cast is not required.
				 * Nothing to do here!
				 */
			}

			//TODO: Add cast back after, if case!

			index++;
		}
	}

private:

	static int id;

	SgStatement * enclosingStatement_;

	std::vector<Types> requiredCasts_;
	std::vector<SgExpression*> actualArgs_;

	std::string castToNewType_;
	std::string castToOldType_;

	std::vector<SgVariableDeclaration*> preStatementCasts_;
	std::vector<SgExpression*> postStatementCasts_;

	SgType * oldType_;
	SgType * newType_;
};

class ASTRetypeTraversal: public SgSimpleProcessing {
public:
	void visit(SgNode* node);
	ASTRetypeTraversal();
	ASTRetypeTraversal(SgClassDeclaration* ADTDeclaration,
			std::string orginalType, std::string newType,
			std::vector<std::string> definedSymbols,
			CollectionOfOperatorReplacement* operatorCallMappings,
			std::string castToOld, std::string castToNew,
			bool retypeFunctionReturn = true, bool retypeFunctionParameters =
					true);

	std::string getCurrentTypeOfVariable(SgType * varType);

	std::string getTypeX();
	void setTypeX(std::string originalType);

	std::string getTypeY();
	void setTypeY(std::string newType);

	void fixCastOperators();

	SgFunctionCallExp* addFunctionCallForBinary(SgBinaryOp * binOp,
			OperatorReplacingMapping* currentReplacement);

	SgFunctionCallExp* renameFunctionCall(SgFunctionCallExp * initialCall,
			OperatorReplacingMapping* currentReplacement);

private:
	bool retypeFunctionReturn;
	bool retypeFunctionParameters;

	std::string type_x;
	std::string type_y;

	/*
	 * Pointer to the old data type.
	 */
	SgType * oldType_;

	/*
	 * Pointer to the new data type
	 */
	SgType * newType_;

	//class declaration to be added in the case of ADT
	SgClassDeclaration* ADTDeclaration;

	std::vector<std::string> definedSymbolsList;

	/*
	 * Vector of enclosing statements that require casts.
	 */
	std::vector<SgStatement*> enclosingStatementsForCasts_;

	/*
	 * Vector of vectors of types for all the statements
	 */
	std::vector<std::vector<Types> > typesForCasts_;

	/*
	 * Vector of vectors of expressions for arguments
	 */
	std::vector<std::vector<SgExpression *> > argumentsForCasts;

	CollectionOfOperatorReplacement* operatorCallMappings_;

	/*
	 * The name of the cast to old operator.
	 */
	std::string castToOld_;

	/*
	 * The name of the cast to the new type
	 */
	std::string castToNew_;
};

}

#endif /* SRC_ASTRETYPETRAVERSAL_H_ */
