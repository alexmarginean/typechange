/*
 * IncludeDirectiveProcessor.cpp
 *
 *  Created on: 18 Dec 2014
 *      Author: alex
 */

#include "IncludeDirectiveProcessor.h"
#include <rose.h>
#include <boost/filesystem.hpp>
using namespace boost::filesystem;

namespace retyper {

void IncludeDirectiveProcessorSynthesizedAttribute::setAdditionalIncludePath(
		std::vector<std::string> includedPathsList) {
	this->includedPathsList = includedPathsList;
}

void IncludeDirectiveProcessorSynthesizedAttribute::parseIncludeList() {
	std::list<PreprocessingInfo*>::const_iterator i =
			this->accumulatedList.begin();

	// First clear the header map
	this->headerMap.clear();

	// Parse all the elements in the identified include instruction
	while (i != accumulatedList.end()) {

#ifdef DEBUG
		cout << "ACTUAL INCLUDE: "
		<< (*i)->get_file_info()->get_physical_filename() << endl;
		cout << "ACTUAL INCLUDE: " << (*i)->get_token_stream() << endl;
		PreprocessingInfo::rose_include_directive *dir =
		(*i)->get_include_directive();
		cout << (*i)->getString() << endl;
#endif

		// Identify included files at string level
		// TODO: Check if this is correct in all the cases
		std::string includeDirective = (*i)->getString();
		std::string lessDelimiter = "<";
		std::string greaterDelimiter = ">";
		std::string quoteDelimiter = "\"";
		std::string includedFile = "";
		if (includeDirective.find(lessDelimiter) != std::string::npos
				&& includeDirective.find(quoteDelimiter) != std::string::npos) {
			if (includeDirective.find(lessDelimiter)
					< includeDirective.find(quoteDelimiter)) {
				includedFile = includeDirective.substr(
						includeDirective.find(lessDelimiter) + 1,
						includeDirective.size());
				includedFile = includedFile.substr(0,
						includedFile.find(greaterDelimiter));
			} else {
				includedFile = includeDirective.substr(
						includeDirective.find(quoteDelimiter) + 1,
						includeDirective.size());
				includedFile = includedFile.substr(0,
						includedFile.find(quoteDelimiter));
			}
		} else if (includeDirective.find(lessDelimiter) != std::string::npos) {
			includedFile = includeDirective.substr(
					includeDirective.find(lessDelimiter) + 1,
					includeDirective.size());
			includedFile = includedFile.substr(0,
					includedFile.find(greaterDelimiter));
		} else if (includeDirective.find(quoteDelimiter) != std::string::npos) {
			includedFile = includeDirective.substr(
					includeDirective.find(quoteDelimiter) + 1,
					includeDirective.size());
			includedFile = includedFile.substr(0,
					includedFile.find(quoteDelimiter));
		} else {
			//this shouldn't be the case
			ROSE_ASSERT(false);
		}

#ifdef DEBUG
		std::cout << includedFile << std::endl;
#endif

		Sg_File_Info * parentFileInfo = (*i)->get_file_info();

#ifdef DEBUG
		std::cout << fInfo->get_filenameString() << std::endl;
#endif

		boost::filesystem::path currentPath(
				parentFileInfo->get_filenameString());
		currentPath = currentPath.parent_path();
		currentPath = boost::filesystem::canonical(currentPath);

#ifdef DEBUG
		std::cout << currentPath.string() << std::endl;
#endif

		std::cout << includedFile << std::endl;

		boost::filesystem::path includedFilePath(includedFile);

		// First check for absolute path or relative paths accesible from the current file location

		if (boost::filesystem::exists(
				boost::filesystem::absolute(includedFilePath, currentPath))) {

			includedFilePath = boost::filesystem::canonical(includedFilePath,
					currentPath);

			//check if it is relative or absolute

			boost::filesystem::path finalPath;

			if (includedFilePath.is_absolute()) {
				// Nothing to be done here.
				finalPath = includedFilePath;
			} else if (includedFilePath.is_relative()) {
				// Combine the current path with the one in the include directive
				currentPath /= includedFilePath;
				finalPath = currentPath;
			}

#ifdef DEBUG
			cout << "FINAL PATH::::::   " << finalPath.string() << endl;
#endif

			headerMap[finalPath.string()] = (*i);

#ifdef DEBUG
			cout
			<< boost::filesystem::canonical(fInfo->get_filenameString()).string()
			<< endl;
			printf("CPP define directive = %s \n", (*i)->getString().c_str());
#endif
		} else {
			// Here look in the include list
			for (int j = 0; j < this->includedPathsList.size(); j++) {
				if (boost::filesystem::exists(
						boost::filesystem::absolute(includedFilePath,
								this->includedPathsList[j]))) {

					includedFilePath = boost::filesystem::canonical(
							includedFilePath, this->includedPathsList[j]);
					boost::filesystem::path finalPath;

					if (includedFilePath.is_absolute()) {
						// Nothing to be done here.
						finalPath = includedFilePath;
					} else if (includedFilePath.is_relative()) {
						// Combine the current path with the one in the include directive
						currentPath /= includedFilePath;
						finalPath = currentPath;
					}
					headerMap[finalPath.string()] = (*i);
					// The first identified existing file should be hold
					// So exist from the loop
					j = this->includedPathsList.size() + 1;
				}
			}
		}
		i++;
	}
}

OriginalNewHeaderType IncludeDirectiveProcessorSynthesizedAttribute::getHeaderMap() {
	return this->headerMap;
}

IncludeDirectiveProcessor::IncludeDirectiveProcessor(SgProject * project,
		std::vector<std::string> includedPaths) {
	this->project = project;
	this->includedPathsList = includedPaths;
}

IncludeDirectiveProcessor::~IncludeDirectiveProcessor() {
	// TODO Auto-generated destructor stub
}

IncludeDirectiveProcessorSynthesizedAttribute IncludeDirectiveProcessor::evaluateSynthesizedAttribute(
		SgNode* n, SynthesizedAttributesList childAttributes) {
	IncludeDirectiveProcessorSynthesizedAttribute localResult;

	// Build the list from children (in reverse order to preserve the final ordering)
	for (SynthesizedAttributesList::reverse_iterator child =
			childAttributes.rbegin(); child != childAttributes.rend();
			child++) {
		localResult.accumulatedList.splice(localResult.accumulatedList.begin(),
				child->accumulatedList);
	}

	// Add in the information from the current node
	SgLocatedNode* locatedNode = isSgLocatedNode(n);
	if (locatedNode != NULL) {
		AttachedPreprocessingInfoType* commentsAndDirectives =
				locatedNode->getAttachedPreprocessingInfo();

		if (commentsAndDirectives != NULL) {
			// printf ("Found attached comments (to IR node at %p of type: %s): \n",locatedNode,locatedNode->class_name().c_str());
			int counter = 0;

			// Use a reverse iterator so that we preserve the order when using push_front to add each directive to the accumulatedList
			AttachedPreprocessingInfoType::reverse_iterator i;
			for (i = commentsAndDirectives->rbegin();
					i != commentsAndDirectives->rend(); i++) {
				// The different classifications of comments and directives are in ROSE/src/frontend/SageIII/rose_attributes_list.h

				if ((*i)->getTypeOfDirective()
						== PreprocessingInfo::CpreprocessorIncludeDeclaration) {

#ifdef DEBUG
					printf(
							"          Attached Comment #%d in file %s (relativePosition=%s): classification %s :\n%s\n",
							counter++,
							(*i)->get_file_info()->get_filenameString().c_str(),
							((*i)->getRelativePosition()
									== PreprocessingInfo::before) ?
							"before" : "after",
							PreprocessingInfo::directiveTypeName(
									(*i)->getTypeOfDirective()).c_str(),
							(*i)->getString().c_str());
#endif
					// use push_front() to end up with source ordering of final list of directives
					localResult.accumulatedList.push_front(*i);
				}
			}
		}
	}
	return localResult;
}

void IncludeDirectiveProcessor::generateHeaderMap() {
	ROSE_ASSERT(project != NULL);
	IncludeDirectiveProcessorSynthesizedAttribute results = this->traverse(
			this->project);
	// Set the additional include path for the synthesized attribute
	results.setAdditionalIncludePath(this->includedPathsList);
	results.parseIncludeList();
	this->headerMap = results.getHeaderMap();
}

OriginalNewHeaderType IncludeDirectiveProcessor::getHeaderMap() {
	return this->headerMap;
}

} /* namespace retyper */
