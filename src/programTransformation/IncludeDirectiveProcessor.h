/*
 * IncludeDirectiveProcessor.h
 *
 *  Created on: 18 Dec 2014
 *      Author: alex
 */

#ifndef SRC_INCLUDEDIRECTIVEPROCESSOR_H_
#define SRC_INCLUDEDIRECTIVEPROCESSOR_H_

#include <rose.h>

namespace retyper {

typedef std::map<std::string, PreprocessingInfo*> OriginalNewHeaderType;

// The synthesized attribute required by the AstBottomUpProcessing
// This class will also generate the map between the include instruction and the header file
class IncludeDirectiveProcessorSynthesizedAttribute {
public:
	// List of #define directives (save the PreprocessingInfo objects
	// so that we have all the source code position information).
	std::list<PreprocessingInfo*> accumulatedList;

	void parseIncludeList();
	OriginalNewHeaderType getHeaderMap();

	void setAdditionalIncludePath(std::vector<std::string> includedPathsList);

private:
	OriginalNewHeaderType headerMap;
	std::vector<std::string> includedPathsList;
};

// The main class of IncludeDirectiveProcessor
// This class will parse a SgProject and will generate the mapping between the include directives
// and the actual absolute path to the included file
class IncludeDirectiveProcessor: public AstBottomUpProcessing<
		IncludeDirectiveProcessorSynthesizedAttribute> {
public:
	IncludeDirectiveProcessor(SgProject * project,
			std::vector<std::string> includedPaths);
	virtual ~IncludeDirectiveProcessor();

	virtual IncludeDirectiveProcessorSynthesizedAttribute evaluateSynthesizedAttribute(
			SgNode* n, SynthesizedAttributesList childAttributes);

	void generateHeaderMap();

	OriginalNewHeaderType getHeaderMap();

private:
	OriginalNewHeaderType headerMap;
	SgProject * project;
	std::vector<std::string> includedPathsList;

};

} /* namespace retyper */

#endif /* SRC_INCLUDEDIRECTIVEPROCESSOR_H_ */
