/*
 * RenamePersistantAttribute.cpp
 *
 *  Created on: 9 Dec 2014
 *      Author: alex
 */

#include "RetypePersistantAttribute.h"

namespace retyper {
RetypePersistentAttribute::RetypePersistentAttribute(AddedDefinitions v) {
	this->value = v;
}
} /* namespace retyper */
