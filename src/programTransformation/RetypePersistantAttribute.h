/*
 * RenamePersistantAttribute.h
 *
 *  Created on: 9 Dec 2014
 *      Author: alex
 */

#ifndef SRC_RETYPEPERSISTANTATTRIBUTE_H_
#define SRC_RETYPEPERSISTANTATTRIBUTE_H_

#include <rose.h>

namespace retyper {

//enum for holding the types of program transformations
enum AddedDefinitions {
	DoubleStructWrapper, InitializerDoubleElement, SympleTypeAttribute
};

//class for holding attributes to the AST, specifying a program transformation

class RetypePersistentAttribute: public AstAttribute {
public:
	AddedDefinitions value;

	RetypePersistentAttribute(AddedDefinitions v);
};

} /* namespace retyper */

#endif /* SRC_RETYPEPERSISTANTATTRIBUTE_H_ */
