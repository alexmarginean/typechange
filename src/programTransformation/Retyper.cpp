/*
 * Retyper.cpp
 *
 *  Created on: 9 Dec 2014
 *      Author: alex
 */

#include "Retyper.h"
#include "ASTRetypeTraversal.h"
#include "RetyperFixOperators.h"
#include <rose.h>
#include "IncludeDirectiveProcessor.h"

// Includes required for overwritting of the unparser in ROSE
// TODO: Try to add this in the ROSE code and check the side effects of it
#include <FileHelper.h>
#include <IncludedFilesUnparser.h>

#include <boost/serialization/vector.hpp>
#include <ConstraintError.h>
#include <OperatorReplacingMapping.h>

namespace retyper {

using namespace SageBuilder;
using namespace SageInterface;
using namespace std;

string originalType;
string newType;

Retyper::Retyper() {
	this->project = NULL;
	this->isADT = false;
	this->ADTHeaderFile = "";
	this->stFile = "";
	this->operatorCallMappings_ = NULL;
}

Retyper::Retyper(SgProject * project, std::vector<std::string> includedPaths,
		std::string stFile, CollectionOfOperatorReplacement* restoredColection,
		std::string castToOld, std::string castToNew, std::string ADTHeaderFile,
		bool isADT, bool newTypeIsPrimitive) {
	this->project = project;
	this->isADT = isADT;
	this->ADTHeaderFile = ADTHeaderFile;
	this->includedPathsList = includedPaths;
	this->stFile = stFile;
	this->operatorCallMappings_ = restoredColection;
	this->castToOldType_ = castToOld;
	this->castToNewType_ = castToNew;
	this->newTypeIsPrimitive_ = newTypeIsPrimitive;
}

Retyper::~Retyper() {
	//nothing here. ROSE takes care of freeing it
}

SgProject* Retyper::getProject() {
	return project;
}

void Retyper::setProject(SgProject * project) {
	this->project = project;
}

void Retyper::retype(std::string type_X, std::string type_Y) {
	//retype the initial program
	if (this->project->numberOfFiles() >= 1) {

		// First generate the list of defined symbols in the project
		if (this->stFile != "") {
			this->generateDefinedSymbolsList();

#if 0
			// Display the Symbol Table
			cout << endl << endl;
			for(int kk = 0; kk < this->definedSymbolsList.size(); kk++) {
				cout << this->definedSymbolsList[kk]<<" ";
			}
			cout << endl << endl;

#endif

		} else {
			return;
		}
		// Retype the variables types in the source and header files
		if (this->isADT) {
			//get a file scope
			SgSourceFile* file = isSgSourceFile((*project)[0]);
			//first build a non defining declaration
			SgClassDeclaration* ADTClassDeclaration =
					buildNondefiningClassDeclaration_nfi(type_Y,
							SgClassDeclaration::e_struct,
							file->get_globalScope()->get_scope(), false, NULL);
			//include the header file specified by the user
			SageInterface::insertHeader(this->ADTHeaderFile,
					PreprocessingInfo::before, false,
					file->get_globalScope()->get_scope());

			ASTRetypeTraversal *retypeTraversal = new ASTRetypeTraversal(
					ADTClassDeclaration, type_X, type_Y,
					this->definedSymbolsList, this->operatorCallMappings_,
					this->castToOldType_, this->castToNewType_, true, true);
			retypeTraversal->traverse(project, preorder);
			retypeTraversal->fixCastOperators();
		} else {
			assert(this->newTypeIsPrimitive_ == true);

			ASTRetypeTraversal * retypeTraversal = new ASTRetypeTraversal(NULL,
					type_X, type_Y, this->definedSymbolsList,
					this->operatorCallMappings_, this->castToOldType_,
					this->castToNewType_, true, true);
			retypeTraversal->traverse(project, preorder);
			/*
			 * Here we are in the primitive data type case.
			 * The method fixCastOperators simply adds explicit casts, wherever
			 * them are required. However, if the casts are implicit, we shouldn't
			 * call this method!. So, first check if the casts are provided by the user.
			 * Our assumptions is that either both the casts are provided, or non of them!
			 */

			if (this->castToNewType_ != "NON"
					|| this->castToOldType_ != "NON") {
				retypeTraversal->fixCastOperators();
			}
		}

		// Retype the operators
		//RetyperFixOperators operatorsFixer;
		//operatorsFixer.traverse(project, preorder);

		// Generate the header files and fix the include instructions
		this->unparseHeaderFiles(NULL, NULL);
		this->fixIncludeDirectives();
		unparseProject(this->project);

	}
}

void Retyper::prependIncludeOptionsToCommandLineMy(SgProject* project,
		const list<string>& includeCompilerOptions) {
	SgStringList argumentList = project->get_originalCommandLineArgumentList();
	//Note: Insert -I options starting from the second argument, because the first argument is the name of the executable.
	argumentList.insert(++argumentList.begin(), includeCompilerOptions.begin(),
			includeCompilerOptions.end());
	project->set_originalCommandLineArgumentList(argumentList);
	const SgFilePtrList& fileList = project->get_fileList();
	for (SgFilePtrList::const_iterator sgFilePtr = fileList.begin();
			sgFilePtr != fileList.end(); sgFilePtr++) {
		argumentList = (*sgFilePtr)->get_originalCommandLineArgumentList();
		argumentList.insert(++argumentList.begin(),
				includeCompilerOptions.begin(), includeCompilerOptions.end());
		(*sgFilePtr)->set_originalCommandLineArgumentList(argumentList);
	}
}

map<string, string> Retyper::unparseHeaderFiles(
		UnparseFormatHelp *unparseFormatHelp,
		UnparseDelegate* unparseDelegate) {
	ROSE_ASSERT(project != NULL);

	// This will map the original file name to the new file name for modified header files
	modifiedHeaderMaps.clear();

	// Proceed only if there are input files and they require header files unparsing.
	if (!project->get_fileList().empty()) {
		if (SgProject::get_verbose() >= 1) {
			cout << endl << "***HEADER FILES UNPARSING***" << endl << endl;
		}
		IncludedFilesUnparser includedFilesUnparser(project);
		includedFilesUnparser.unparse();
		const string& unparseRootPath =
				includedFilesUnparser.getUnparseRootPath();
		const map<string, string>& unparseMap =
				includedFilesUnparser.getUnparseMap();
		const map<string, SgScopeStatement*>& unparseScopesMap =
				includedFilesUnparser.getUnparseScopesMap();

		prependIncludeOptionsToCommandLineMy(project,
				includedFilesUnparser.getIncludeCompilerOptions());

		for (map<string, string>::const_iterator unparseMapEntry =
				unparseMap.begin(); unparseMapEntry != unparseMap.end();
				unparseMapEntry++) {
			SgSourceFile* unparsedFile = new SgSourceFile();
			unparsedFile->set_Cxx_only(true); //TODO: Generalize this hard coded trick.
			const string& originalFileName = unparseMapEntry->first;

			//TODO: Unparse only the files in the header map

			if (!includedFilesUnparser.isInputFile(originalFileName)) { //Unparse here only files that would not be unparsed otherwise.
				if (SgProject::get_verbose() >= 1) {
					cout << "Unparsing included file:" << originalFileName
							<< endl;
				}
				map<string, SgScopeStatement*>::const_iterator unparseScopesMapEntry =
						unparseScopesMap.find(originalFileName);
				ROSE_ASSERT(unparseScopesMapEntry != unparseScopesMap.end());

				unparsedFile->set_sourceFileNameWithoutPath(
						FileHelper::getFileName(originalFileName));
				unparsedFile->set_sourceFileNameWithPath(originalFileName);
				const string& outputFileName = FileHelper::concatenatePaths(
						unparseRootPath, unparseMapEntry->second);

				FileHelper::ensureParentFolderExists(outputFileName);
				unparsedFile->set_unparse_output_filename(outputFileName);

				modifiedHeaderMaps[originalFileName] = outputFileName;

				Sg_File_Info* unparsedFileInfo = new Sg_File_Info(
						originalFileName, 0, 0);
				unparsedFile->set_file_info(unparsedFileInfo);

				//Set SgGlobal to avoid problems with checks during unparsing.
				SgGlobal* fakeGlobal = new SgGlobal();
				fakeGlobal->set_file_info(unparsedFileInfo);
				unparsedFile->set_globalScope(fakeGlobal);

				unparseFile(unparsedFile, unparseFormatHelp, unparseDelegate,
						unparseScopesMapEntry->second);
			}
		}
	}
	return modifiedHeaderMaps;

}

void Retyper::fixIncludeDirectives() {

	IncludeDirectiveProcessor * includesProcessor =
			new IncludeDirectiveProcessor(project, includedPathsList);
	includesProcessor->generateHeaderMap();
	OriginalNewHeaderType includesToFilesMap =
			includesProcessor->getHeaderMap();

	for (map<string, string>::iterator it = modifiedHeaderMaps.begin();
			it != modifiedHeaderMaps.end(); it++) {
#ifdef DEBUG
		cout << "FIRST: " << it->first << " SECOND: " << it->second << endl;
#endif

		map<string, PreprocessingInfo*>::const_iterator replacedHeaderEntry =
				includesToFilesMap.find(it->first);
		if (replacedHeaderEntry != includesToFilesMap.end()) {

#ifdef DEBUG
			cout << "REPLACEMENT FIND FOR: " << replacedHeaderEntry->first<< endl;
			cout << "NEW NAME: " << it->second << endl;
#endif

			replacedHeaderEntry->second->setString(
					"#include \"" + it->second + "\"");
		}
	}
}

void Retyper::generateDefinedSymbolsList() {
	std::ifstream fin(this->stFile.c_str(), ios::in);

	while (!fin.eof()) {
		std::string type, mangledName, file;
		fin >> type >> mangledName >> file;
		this->definedSymbolsList.push_back(mangledName);
	}
}

}

