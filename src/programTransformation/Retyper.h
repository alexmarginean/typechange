/*
 * Retyper.h
 *
 *  Created on: 9 Dec 2014
 *      Author: alex
 */

#include <rose.h>

#ifndef SRC_RETYPER_H_
#define SRC_RETYPER_H_

#include <boost/serialization/vector.hpp>
#include <ConstraintError.h>
#include <OperatorReplacingMapping.h>

using namespace analyzer;

namespace retyper {

class Retyper {
public:
	Retyper();
	Retyper(SgProject * project, std::vector<std::string> includedPaths,
			std::string stFile,
			CollectionOfOperatorReplacement* restoredColection,
			std::string castToOld, std::string castToNew,
			std::string ADTHeaderFile = "", bool isADT = false,
			bool newTypeIsPrimitive = false);
	virtual ~Retyper();
	SgProject * getProject();
	void setProject(SgProject * project);
	void retype(std::string type_X, std::string type_Y);
private:
	SgProject * project;
	bool isADT;
	std::string ADTHeaderFile;
	std::map<std::string, std::string> unparseHeaderFiles(
			UnparseFormatHelp *unparseFormatHelp,
			UnparseDelegate* unparseDelegate);
	void fixIncludeDirectives();
	std::map<std::string, std::string> modifiedHeaderMaps;
	void prependIncludeOptionsToCommandLineMy(SgProject* project,
			const std::list<std::string>& includeCompilerOptions);
	void generateDefinedSymbolsList();

	std::vector<std::string> includedPathsList;
	std::string stFile;
	std::vector<std::string> definedSymbolsList;

	CollectionOfOperatorReplacement* operatorCallMappings_;

	std::string castToOldType_;
	std::string castToNewType_;

	bool newTypeIsPrimitive_;
};

}
#endif /* SRC_RETYPER_H_ */
