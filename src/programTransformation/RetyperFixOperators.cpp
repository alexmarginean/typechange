/*
 * RetyperFixOperators.cpp
 *
 *  Created on: 9 Dec 2014
 *      Author: alex
 */
#include <rose.h>
#include "RetypePersistantAttribute.h"
#include "RetyperFixOperators.h"

using namespace std;

namespace retyper {

class RetyperFixStatements: public SgSimpleProcessing {
public:
	RetyperFixStatements() {
		identified = false;
		needCastOp = false;
	}

	void visit(SgNode* node);
	bool identified;

	bool needCastOp;

};

void RetyperFixStatements::visit(SgNode* node) {
	if (isSgAddOp(node)) {
		// test here for calling a cast !
		SgAddOp * binaryOp = isSgAddOp(node);

		SgExpression* lhsOperand = binaryOp->get_lhs_operand();
		SgExpression* rhsOperand = binaryOp->get_rhs_operand();

		if (SgVarRefExp * theOtherRefExp = isSgVarRefExp(rhsOperand)) {
			//here just replace function call

		} else if (SgValueExp * theOtherValueExp = isSgValueExp(rhsOperand)) {
			//here cast operator
			needCastOp = true;
		}

		// TODO: replace the statement with the new one, according to the function call

		identified = true;

	}
}

void RetyperFixOperators::visit(SgNode* node) {
	int x;
	x = 10;

	SgBasicBlock* block = isSgBasicBlock(node);
	if (block != NULL) {

		bool shouldBeModified;
		bool needCastOp;

		for (SgStatementPtrList::iterator it = block->get_statements().begin();
				it != block->get_statements().end(); it++) {
			cout << (*it)->unparseToCompleteString() << endl;
			SgStatement * currentStatement = (*it);

			RetyperFixStatements identifyStm;
			identifyStm.traverse(currentStatement, preorder);

			if (identifyStm.identified) {
				shouldBeModified = true;
			}
			if (identifyStm.needCastOp) {

				needCastOp = true;

				// Mark this as a transformation (required)
				Sg_File_Info* sourceLocation =
						Sg_File_Info::generateDefaultFileInfoForTransformationNode();
				ROSE_ASSERT(sourceLocation != NULL);

				SgType* type = new SgTypeInt();
				ROSE_ASSERT(type != NULL);

				SgName name = "newVariable";

				SgVariableDeclaration* variableDeclaration =
						new SgVariableDeclaration(sourceLocation, name, type);
				ROSE_ASSERT(variableDeclaration != NULL);

				SgInitializedName* initializedName =
						*(variableDeclaration->get_variables().begin());
				initializedName->set_file_info(
						Sg_File_Info::generateDefaultFileInfoForTransformationNode());

				initializedName->set_scope(block);

				variableDeclaration->set_firstNondefiningDeclaration(
						variableDeclaration);

				ROSE_ASSERT(block->get_statements().size() > 0);

				block->get_statements().insert(it, variableDeclaration);
				variableDeclaration->set_parent(block);

				// Add a symbol to the sybol table for the new variable
				SgVariableSymbol* variableSymbol = new SgVariableSymbol(
						initializedName);
				block->insert_symbol(name, variableSymbol);

				return;

			}
		}

		if (needCastOp) {

		}
	}
}

} /* namespace retyper */

