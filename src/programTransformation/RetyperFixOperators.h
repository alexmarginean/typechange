/*
 * RetyperFixOperators.h
 *
 *  Created on: 9 Dec 2014
 *      Author: alex
 */

#include <rose.h>

#ifndef SRC_RETYPERFIXOPERATORS_H_
#define SRC_RETYPERFIXOPERATORS_H_

namespace retyper {

class RetyperFixOperators: public SgSimpleProcessing {
public:
	void visit(SgNode* node);
};

} /* namespace retyper */

#endif /* SRC_RETYPERFIXOPERATORS_H_ */
