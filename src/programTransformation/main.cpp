#include <rose.h>
#include "Retyper.h"
#include <boost/filesystem.hpp>
#include "RetypeParameterParser.h"

using namespace SageBuilder;
using namespace SageInterface;
using namespace std;
using namespace SageInterface;

#include <boost/serialization/vector.hpp>
#include <ConstraintError.h>
#include <OperatorReplacingMapping.h>

using namespace retyper;
using namespace analyzer;

#define DEBUG

#define SOURCE_POSITION Sg_File_Info::generateDefaultFileInfoForTransformationNode()

// Build a synthesized attribute for the tree traversal

class UnparseHeadersTransformVisitor: public AstSimpleProcessing {
private:
	static const string matchEnding;
	static const size_t matchEndingSize;
	static const string renameEnding;

protected:
	void visit(SgNode* astNode);
};

const string UnparseHeadersTransformVisitor::matchEnding = "_rename_me";
const size_t UnparseHeadersTransformVisitor::matchEndingSize =
		matchEnding.size();
const string UnparseHeadersTransformVisitor::renameEnding = "_renamed";

void UnparseHeadersTransformVisitor::visit(SgNode* node) {
	// Use a pointer to a constant SgVariableDeclaration to be able to call the constant getter variableDeclaration -> get_variables(),
	// which does not mark the node as modified.
	const SgVariableDeclaration* variableDeclaration = isSgVariableDeclaration(
			node);
	if (variableDeclaration != NULL) {
		const SgInitializedNamePtrList& nameList =
				variableDeclaration->get_variables();
		for (SgInitializedNamePtrList::const_iterator nameListIterator =
				nameList.begin(); nameListIterator != nameList.end();
				nameListIterator++) {
			string originalName = ((*nameListIterator)->get_name()).getString();

			// Rename any variable, whose name ends with matchEnding.
			if (originalName.size() >= matchEndingSize
					&& originalName.compare(
							originalName.size() - matchEndingSize,
							matchEndingSize, matchEnding) == 0) {
				SageInterface::set_name(*nameListIterator,
						originalName + renameEnding);
			}
		}
	}
}

int main(int argc, char * argv[]) {

//setting up things for the translator
//we have to check the parameters for retype and the ones for gcc compiler

	std::string initialType;
	std::string newVarType;
	std::string headerFile;
	std::string projectPath;
	std::string stFile;
	bool isStruct = false;
	std::string castToOldType;
	std::string castToNewType;

	std::string operatorMappingFiles;

	// Vector used for holding the incldued path with -I compiler option
	// This is needed for searching for header files that need to be replaced
	std::vector<string> includedPaths;

	RetypeParameterParser parametersParser;

// Parse the parameter list; interpret the ones required for Retyper and remove them before calling the frontend
	Rose_STL_Container<string> l = parametersParser.parseRetypeParameters(argc, argv, &initialType, &newVarType,
			&headerFile, &projectPath, &isStruct, includedPaths, &stFile, &operatorMappingFiles,&castToOldType, &castToNewType);

	bool newTypeIsPrimitive;
	newTypeIsPrimitive = (newVarType == "int") || (newVarType == "float")
			|| (newVarType == "double") || (newVarType == "char") || (newVarType == "short")
			|| (newVarType == "long");
#ifdef DEBUG
	cout << "STRUCT: " << isStruct << endl;
#endif

	analyzer::CollectionOfOperatorReplacement restoredColection;
	analyzer::CollectionOfOperatorReplacement::restore_operator_defs(
			restoredColection, operatorMappingFiles);
#if 1
	std::cout << restoredColection;
	//exit(0);
#endif

#if 0
	// TEST SERIALIZE!
	std::ofstream file2("outputAfter.out", std::ios::out);

	file2 << std::endl << restoredColection << std::endl;
#endif

#ifdef DEBUG
	std::cout << "Initial Type::  " << initialType << endl;
	std::cout << "New Type::  " << newVarType << endl;
	std::cout << "ST File:: " << stFile << endl;
#endif

	SgNode * node = new SgNode();
	SgProject * project = frontend(l);

	//project->set_verbose(20);

#if 0
	project->set_verbose(3);

	SgFilePtrList::iterator fileIterator = project->get_fileList().begin();
	while (fileIterator != project->get_fileList().end()) {
		(*fileIterator)->set_unparseHeaderFiles(true);
		fileIterator++;
	}

#endif

// TODO: Remove this. Is just an example traversal to modify a header file
//UnparseHeadersTransformVisitor transformVisitor;
//transformVisitor.traverse(project, preorder);

// Main retyper
	Retyper * myRetyper = new Retyper(project, includedPaths, stFile,
			&restoredColection, castToOldType, castToNewType, headerFile,
			isStruct, newTypeIsPrimitive);
	myRetyper->retype(initialType, newVarType);

// Additional output for debugging

#if 0
// This should return all the information we require
// However probably because of a bug in the frontend it returns NULL
// I reimplemented this part
	const map<string, set<PreprocessingInfo*> >& preprocIncludesMap =
	project->get_includingPreprocessingInfosMap();
	for (SgStringList::iterator it =
			project->get_includeDirectorySpecifierList().begin();
			it != project->get_includeDirectorySpecifierList().end(); it++) {
		cout << "IncludedFILE: " << (*it) << endl;
	}

	for (map<string, set<PreprocessingInfo*> >::const_iterator it =
			preprocIncludesMap.begin(); it != preprocIncludesMap.end(); it++) {
		cout << "Include: " << it->first << " PreprocINFOS::  ";

		for (set<PreprocessingInfo*>::const_iterator preprocessingInfoPtr =
				it->second.begin(); preprocessingInfoPtr != it->second.end();
				preprocessingInfoPtr++) {
			cout << (*preprocessingInfoPtr)->getString() << " ";
		}
	}
#endif

#if 1
	generateAstGraph(project, 30000);
#endif

	std::cout << "The retype operation was successful!" << std::endl;

	return 0;
}
