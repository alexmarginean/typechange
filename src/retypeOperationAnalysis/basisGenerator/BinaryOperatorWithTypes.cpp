/*
 * BinaryOperatorWithTypes.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: alex
 */

#include <rose.h>
#include <sstream>

#include <BinaryOperatorWithTypes.h>
#include <HighOrderTypeOfTerm.h>
#include <utils.h>
#include <TypeOfTerm.h>
#include <SimpleTypeOfTerm.h>

namespace analyzer {

int BinaryOperatorWithTypes::nextID = 0;

BinaryOperatorWithTypes::BinaryOperatorWithTypes(std::string operatorName,
		TypeOfTerm * returnTypesOfOp, TypeOfTerm* lhs, TypeOfTerm* rhs,
		std::string theBinaryExpression, bool isNewOperator) {
	if (isNewOperator) {
		id = nextID;
		BinaryOperatorWithTypes::nextID++;
	} else {
		id = nextID;
	}

	this->operatorName = operatorName;
	this->returnTypesOfOperator_ = returnTypesOfOp;
	this->lhs = lhs;
	this->rhs = rhs;
	this->theBinaryExpression = theBinaryExpression;
}

std::string BinaryOperatorWithTypes::toStringTypeof() {
	std::string typeofString;
	typeofString = " { TYPE OF: " + this->operatorName;

	std::stringstream ss;
	ss << this->id;

	std::string stringForId;
	ss >> stringForId;
	typeofString += stringForId;
	typeofString += " } ";

	return typeofString;
}

std::string BinaryOperatorWithTypes::toStringTypeofReturn() {
	std::string output = "";
	output += returnTypesOfOperator_->retypeTypesToString(this->getTypeX(),
			this->getTypeY(), this->getTypeCast(), this->getTypeCastOld());
	return output;
}

int BinaryOperatorWithTypes::getUniqueId() {
	return id;
}

std::string BinaryOperatorWithTypes::toString() {
	std::string output;
	output = "Binary Operator: ";

	output += this->returnTypesOfOperator_->toString() + " ";

	output += this->operatorName;

	std::stringstream ss;
	ss << this->id;

	std::string stringForId;
	ss >> stringForId;
	output += stringForId;
	output += " (" + this->lhs->toString() + ", " + this->rhs->toString() + ")";

	output += "  EXPRESSION -> " + this->theBinaryExpression + "  TYPE_X: "
			+ this->getTypeX() + "  TYPE_Y: " + this->getTypeY();

	HighOrderTypeOfTerm * expressionLhs =
			dynamic_cast<HighOrderTypeOfTerm*>(lhs);
	if (expressionLhs) {
		output += "\nEXPRESSION TYPE! LHS: ";\
		output += expressionLhs->typeOf->getUniqueName();
	}

	output += "\n\t\tλ" + this->operatorName + " xy.z : ";
	output += lhs->retypeTypesToString(this->getTypeX(), this->getTypeY(),
			this->getTypeCast(), this->getTypeCastOld());
	output += " → ";
	output += rhs->retypeTypesToString(this->getTypeX(), this->getTypeY(),
			this->getTypeCast(), this->getTypeCastOld());
	output += " → ";

	output += returnTypesOfOperator_->retypeTypesToString(this->getTypeX(),
			this->getTypeY(), this->getTypeCast(), this->getTypeCastOld());

	return output;

}

std::string BinaryOperatorWithTypes::getUniqueName() {
	std::string uniqueName = this->operatorName;

	std::stringstream ss;
	ss << this->id;

	std::string stringForId;
	ss >> stringForId;
	uniqueName += stringForId;

	return uniqueName;

}

bool BinaryOperatorWithTypes::operator==(BinaryOperatorWithTypes &op2) {
	// TODO: Allow operator overloading here!

	return true;

	/*
	 if(this->operatorName == op2.operatorName && *(this->lhs) == *(op2.lhs) && *(this->rhs) == *(op2.rhs)){
	 return true;
	 }
	 else{
	 return false;
	 }*/
}

std::vector<TypeOfTerm*> BinaryOperatorWithTypes::getListOfArguments() {
	std::vector<TypeOfTerm*> listOfArguments;
	listOfArguments.push_back(lhs);
	listOfArguments.push_back(rhs);
	return listOfArguments;
}

} /* namespace analyzer */
