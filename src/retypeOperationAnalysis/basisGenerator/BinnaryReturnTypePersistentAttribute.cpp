/*
 * BinnaryReturnTypePersistentAttribute.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: alex
 */

#include <BinnaryReturnTypePersistentAttribute.h>

namespace analyzer {

BinnaryReturnTypePersistentAttribute::BinnaryReturnTypePersistentAttribute(
		std::vector<std::string> supportedReturnTypes,
		std::string theOtherOperator, bool hasAlternatives,
		OperatorWithTypes * childExpressionType) {
	this->supportedReturnTypes = supportedReturnTypes;
	this->theOtherOperator = theOtherOperator;
	this->hasAlternatives = hasAlternatives;

	this->childExpressionType = childExpressionType;
}

} /* namespace analyzer */
