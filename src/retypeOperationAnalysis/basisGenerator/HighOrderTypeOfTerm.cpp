/*
 * BinaryOperatorExpressionType.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: alex
 */

#include <HighOrderTypeOfTerm.h>
#include <OperatorWithTypes.h>

namespace analyzer {



std::string HighOrderTypeOfTerm::getTypeName() {
	return "HELLO";
}
HighOrderTypeOfTerm::HighOrderTypeOfTerm(OperatorWithTypes * typeOf) {
	this->typeOf = typeOf;
}

HighOrderTypeOfTerm::HighOrderTypeOfTerm(HighOrderTypeOfTerm * newType) {
	this->typeOf = newType->typeOf;
}
HighOrderTypeOfTerm::~HighOrderTypeOfTerm() {

}
std::string HighOrderTypeOfTerm::toString() {
	std::string output;
	/*!
	 * The current type of arguments.
	 */
	output = this->typeOf->toStringTypeof();
	output += ": ";
	/*!
	 * The return type.
	 */
	output += this->typeOf->toStringTypeofReturn();

	return output;
}

std::vector<std::string> HighOrderTypeOfTerm::getTypeArrayOfStrings() {
	/*!
	 * TODO: check this. We shouldn't enter here, but if we are this should still
	 * be correct.
	 */
	return this->typeOf->returnTypesOfOperator()->getTypeArrayOfStrings();
}

bool HighOrderTypeOfTerm::operator==(HighOrderTypeOfTerm t2) {
	// TODO: fix this
	return true;
}

std::string HighOrderTypeOfTerm::retypeTypesToString(std::string typeX,
		std::string typeY, std::string typeCast, std::string typeCastOld) {
	return this->toString();
}


} /* namespace analyzer */
