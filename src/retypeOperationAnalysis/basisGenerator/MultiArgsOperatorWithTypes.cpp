/*
 * MultiArgsOperatorWithTypes.cpp
 *
 *  Created on: Feb 13, 2015
 *      Author: root
 */

#include <rose.h>
#include <sstream>
#include <cstdio>

#include <MultiArgsOperatorWithTypes.h>
#include <TypeOfTerm.h>
#include <utils.h>

namespace analyzer {

int MultiArgsOperatorWithTypes::nextID = 0;

MultiArgsOperatorWithTypes::MultiArgsOperatorWithTypes(std::string operatorName,
		TypeOfTerm* returnTypesOfOp, std::vector<TypeOfTerm*> arguments,
		std::string theExpression) {
	id = nextID;
	MultiArgsOperatorWithTypes::nextID++;

	this->operatorName = operatorName;
	this->returnTypesOfOperator_ = returnTypesOfOp;
	this->arguments = arguments;
	this->theExpression = theExpression;
}

std::string MultiArgsOperatorWithTypes::toStringTypeof() {
	std::string typeofString;
	typeofString = "TYPE OF: " + this->operatorName;

	std::stringstream ss;
	ss << this->id;

	std::string stringForId;
	ss >> stringForId;
	typeofString += stringForId;

	return typeofString;
}

int MultiArgsOperatorWithTypes::getUniqueId() {
	return id;
}

#if 0

std::string BinaryOperatorWithTypes::toString() {
	std::string output;
	output = "Binary Operator: ";
	output += " ( ";
	for (int i = 0; i < this->returnTypesOfOperator_.size() - 1; i++) {
		output += this->returnTypesOfOperator_[i]->toString() + " | ";
	}
	output +=
	this->returnTypesOfOperator_[this->returnTypesOfOperator_.size() - 1]->toString()
	+ " ";
	output += " ) ";

	output += this->operatorName;

	std::stringstream ss;
	ss << this->id;

	std::string stringForId;
	ss >> stringForId;
	output += stringForId;
	output += " (" + this->lhs->toString() + ", " + this->rhs->toString() + ")";

	output += "  EXPRESSION -> " + this->theBinaryExpression + "  TYPE_X: "
	+ this->getTypeX() + "  TYPE_Y: " + this->getTypeY();

	HighOrderTypeOfTerm * expressionLhs =
	dynamic_cast<HighOrderTypeOfTerm*>(lhs);
	if (expressionLhs) {
		output += "\nEXPRESSION TYPE! LHS: ";
		output += expressionLhs->typeOf->getUniqueName();
	}

	output += "\n\t\tλ" + this->operatorName + " xy.z : ";
	output += lhs->retypeTypesToString(this->getTypeX(), this->getTypeY());
	output += " → ";
	output += rhs->retypeTypesToString(this->getTypeX(), this->getTypeY());
	output += " → ";

	if (this->returnTypesOfOperator_.size() > 1) {
		output += "( ";
	}

	for (int i = 0; i < this->returnTypesOfOperator_.size() - 1; i++) {
		if (trimSpaces(this->returnTypesOfOperator_[i]->toString())
				== this->getTypeX()) {
			output += "τ ";
			output += SUBSCRIPT_O;
			output += " ∪ ";
		} else if (trimSpaces(this->returnTypesOfOperator_[i]->toString())
				== this->getTypeY()) {
			output += "τ ";
			output += SUBSCRIPT_N;
			output += " ∪ ";
		} else {
			output += "τ ";
			output += SUBSCRIPT_U;
			output += " ∪ ";
		}
	}

	if (trimSpaces(
					this->returnTypesOfOperator_[this->returnTypesOfOperator_.size() - 1]->toString())
			== this->getTypeX()) {
		output += "τ ";
		output += SUBSCRIPT_O;
	} else if (trimSpaces(
					this->returnTypesOfOperator_[this->returnTypesOfOperator_.size() - 1]->toString())
			== this->getTypeY()) {
		output += "τ ";
		output += SUBSCRIPT_N;
	} else {
		output += "τ ";
		output += SUBSCRIPT_U;
	}
	if (this->returnTypesOfOperator_.size() > 1) {
		output += " )";
	}

	return output;

}
#endif

std::string MultiArgsOperatorWithTypes::toString() {
	std::string output;
	output = "Multi Args Op Operator: ";
	output += this->returnTypesOfOperator_->toString() + " | ";

	output += this->operatorName;

	std::stringstream ss;
	ss << this->id;

	std::string stringForId;
	ss >> stringForId;
	output += stringForId;

	output += " (";

	for (std::vector<TypeOfTerm*>::iterator it = this->arguments.begin();
			it != this->arguments.end(); ++it) {
		output += (*it)->toString() + ", ";
	}

	output += ")";

	output += "  EXPRESSION -> " + this->theExpression;

	output += "\n\t\tλ" + this->operatorName + " ";  //+ " xy.z : ";

	int countArgs = 0;

	for (std::vector<TypeOfTerm*>::iterator it = this->arguments.begin();
			it != this->arguments.end(); ++it) {
		output += "x";

		int aux = countArgs;

		do {
			int digit = aux % 10;
			std::stringstream ssArgs;
			ssArgs << sub[aux];

			std::string stringForArgsId;
			ssArgs >> stringForArgsId;

			output += stringForArgsId;

			aux /= 10;
		} while (aux > 0);

		countArgs++;
	}

	//the return value;
	output += ".o : ";

	for (std::vector<TypeOfTerm*>::iterator it = this->arguments.begin();
			it != this->arguments.end(); ++it) {
		output += (*it)->retypeTypesToString(this->getTypeX(), this->getTypeY(),
				this->getTypeCast(), this->getTypeCastOld());
		output += " → ";
	}

	output += returnTypesOfOperator_->retypeTypesToString(this->getTypeX(),
			this->getTypeY(), this->getTypeCast(), this->getTypeCastOld());

	//output += "\n";
	// ⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻⊻
	// ∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧
	//output += lhs->retypeTypesToString(this->getTypeX(), this->getTypeY());
	//output += " → ";
	//output += rhs->retypeTypesToString(this->getTypeX(), this->getTypeY());
	//output += " → ";

	return output;

}

std::string MultiArgsOperatorWithTypes::getUniqueName() {
	std::string uniqueName = this->operatorName;

	std::stringstream ss;
	ss << this->id;

	std::string stringForId;
	ss >> stringForId;
	uniqueName += stringForId;

	return uniqueName;

}

} /* namespace analyzer */
