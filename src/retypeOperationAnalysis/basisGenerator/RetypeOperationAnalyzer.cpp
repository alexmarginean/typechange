/*
 * SymbolTableGenerator.cpp
 *
 *  Created on: 7 Jan 2015
 *      Author: alex
 */

#include <RetypeOperationAnalyzer.h>
#include <rose.h>
#include <iostream>
#include <boost/filesystem.hpp>
#include <HighOrderTypeOfTerm.h>
#include <SimpleTypeOfTerm.h>
#include <TypeOfTerm.h>

#include <RetypeParameterParser.h>
#include <OperatorWithTypes.h>

#include <BinaryOperatorWithTypes.h>
#include <BinnaryReturnTypePersistentAttribute.h>
#include <MultiArgsOperatorWithTypes.h>

//#include <TypeInferenceConstraintSolver.h>

#include <TypedLambdaExpression.h>
#include <CollectionOfOperatorDefinitions.h>

#include <NewOperatorDefinition.h>
#include <OldOperatorDefinition.h>

using namespace std;
using namespace retyper;

namespace analyzer {

RetypeOperationAnalyzer::RetypeOperationAnalyzer() {
	typeY_cast = "cast_op";
	outfile = NULL;
}

RetypeOperationAnalyzer::~RetypeOperationAnalyzer() {
	// TODO Auto-generated destructor stub
}

void RetypeOperationAnalyzer::outputOperatorList() {
#if 1
	//setTypesForTypeOfTerms();

	std: vector<OperatorWithTypes*>::iterator oldIt =
			this->listOfOldOperatorsWithTypes.begin();

	for (std::vector<OperatorWithTypes*>::iterator newIt =
			this->listOfNewOperatorsWithTypes.begin();
			newIt != this->listOfNewOperatorsWithTypes.end(); ++newIt) {

		/*!
		 * The original list of operators.
		 */
		(*outfile) << "⊕ " << subO << " : " << (*oldIt)->toString()
				<< std::endl;
		oldIt++;

		/*!
		 * The retyped list of operators.
		 */
		(*outfile) << "⊕ " << subN << " : " << (*newIt)->toString()
				<< std::endl;

	}
#endif
}

void RetypeOperationAnalyzer::outputOperatorListJustInitial() {
	//setTypesForTypeOfTerms();
	for (std::vector<OperatorWithTypes*>::iterator it =
			this->listOfOldOperatorsWithTypes.begin();
			it != this->listOfOldOperatorsWithTypes.end(); ++it) {
		(*outfile) << "⊕ " << subO << " : " << (*it)->toString() << std::endl;
	}

}

RetypeOperationAnalyzer::RetypeOperationAnalyzer(ofstream *outfile,
		std::string stFile, std::string typeX, std::string typeY,
		bool isUserDefOps, std::string outputOpsDefsFile) {
	assert(outfile != NULL);
	this->outfile = outfile;
	this->stFile = stFile;
	this->typeX = typeX;
	this->typeY = typeY;
	this->typeY_cast = "cast_op_to_Y";
	this->typeX_cast = "cast_op_to_X";
	typeYIsPrimitive = (typeY == "int") || (typeY == "float")
			|| (typeY == "double") || (typeY == "char") || (typeY == "short")
			|| (typeY == "long");
	this->isUserDefinedOpsSource = isUserDefOps;
	this->outputFileForOpsDefs = outputOpsDefsFile;
	this->generateDefinedSymbolsList();
}

std::vector<SgNode*> RetypeOperationAnalyzer::returnStatementList() {
	return this->statementList;
}

std::string RetypeOperationAnalyzer::getCurrentTypeOfVariable(
		SgType * varType) {
	// TODO: Check here if we should address pointers and references!!!
	/*
	 * Temporary treat here pointers and references. Later, consider them
	 * as a different data type!!! TODO: FIX THIS!!!
	 */

	/*
	 * TODO: modify here if we want pointers to be different types
	 * that the non pointer ones!
	 */
	SgPointerType * pointerDerefExpression = isSgPointerType(varType);
	while (pointerDerefExpression) {
		varType = pointerDerefExpression->get_base_type();
		pointerDerefExpression = isSgPointerType(varType);
	}

	SgClassType* classType = isSgClassType(varType);
	std::string currentType = "";
	if (classType) {
		//user defined initial type
		currentType = classType->get_name();
	} else {
		currentType = varType->unparseToCompleteString();
	}
	//again trim the spaces
	currentType = trimSpaces(currentType);
	return currentType;
}

bool RetypeOperationAnalyzer::checkIfVarIsInRetypeScope(SgVarRefExp* varRef) {
	bool shouldRetype = false;
	SgInitializedName* otherVarInitName =
			varRef->get_symbol()->get_declaration();
	if (otherVarInitName->get_declaration()->get_declarationModifier().get_storageModifier().isStatic()
			== true) {
		// If it is static we should always retype it
		// later we also add typeX
		shouldRetype = true;
	} else if (isSgGlobal(otherVarInitName->get_scope()) != NULL) {

		// The declaration is not static
		// First check if it is in global scope

		// In global scope, so we should check whether or not we have the definition for it
		// TODO: This should be done just for external declarations, but for now do it in all the cases
		std::string currentVarName = otherVarInitName->get_name().str();
		if (std::find(this->definedSymbolsList.begin(),
				this->definedSymbolsList.end(), currentVarName)
				!= this->definedSymbolsList.end()) {
			// We have a definition for this variable, so we should retype it
			shouldRetype = true;
		} else {
			// We do not have a definition so we shouldn't retype it
		}
	} else {
		// It is not in global scope so we should always retype it
		shouldRetype = true;
	}

	return shouldRetype;
}

MultiArgsOperatorWithTypes * RetypeOperationAnalyzer::returnTypeofFctDef(
		SgFunctionDefinition * currentFunction) {
	/*!
	 * This shouldn't be NULL, since we are in V_SgFunctionDefinition node
	 */
	assert(currentFunction != NULL);

	/*!
	 * The list of parameters of the current function.
	 */
	SgFunctionParameterList * paramsList =
			currentFunction->get_declaration()->get_parameterList();
	/*!
	 * This is the list of the formal arguments of a function.
	 */
	SgInitializedNamePtrList initNamePtrList = paramsList->get_args();

	/*!
	 * This vectror will hold the list of parameter types.
	 */
	std::vector<TypeOfTerm*> typeOfParameters;

	/*!
	 * iterate ober the list of formal args.
	 */

	for (SgInitializedNamePtrList::iterator it = initNamePtrList.begin();
			it != initNamePtrList.end(); it++) {
		/*!
		 * Push the current type in the list of args types.
		 */

		SgType * currentType = (*it)->get_type();

		/*
		 * TODO: modify here if we want pointers to be different types
		 * that the non pointer ones!
		 */
		SgPointerType * pointerDerefExpression = isSgPointerType(currentType);
		while (pointerDerefExpression) {
			currentType = pointerDerefExpression->get_base_type();
			pointerDerefExpression = isSgPointerType(currentType);
		}

		std::string currentStringname = this->getCurrentTypeOfVariable(
				currentType);
		typeOfParameters.push_back(new SimpleTypeOfTerm(currentStringname));
	}

	/*!
	 * Generate new operator with type expression for the current one.
	 */

	SgType * retTypeCurrent =
			currentFunction->get_declaration()->get_orig_return_type();
	/*
	 * TODO: modify here if we want pointers to be different types
	 * that the non pointer ones!
	 */
	SgPointerType * pointerDerefExpressionRet = isSgPointerType(retTypeCurrent);
	while (pointerDerefExpressionRet) {
		retTypeCurrent = pointerDerefExpressionRet->get_base_type();
		pointerDerefExpressionRet = isSgPointerType(retTypeCurrent);
	}

	std::string returnTypeC = getCurrentTypeOfVariable(retTypeCurrent);

#if 0
	std::cout << "RETURN TYPE IS: "
	<< currentFunction->get_declaration()->get_type()->unparseToCompleteString()
	<< "     AND FORMATED:::   " << returnTypeC << std::endl;
#endif

	MultiArgsOperatorWithTypes * functionTypeOperator =
			new MultiArgsOperatorWithTypes(
					currentFunction->get_declaration()->get_name().str(),
					new SimpleTypeOfTerm(returnTypeC), typeOfParameters,
					currentFunction->unparseToCompleteString());
	return functionTypeOperator;
}

MultiArgsOperatorWithTypes * RetypeOperationAnalyzer::returnTypeofFctDefForInputProgramDefs(
		SgFunctionDefinition * currentFunction) {
	/*!
	 * This shouldn't be NULL, since we are in V_SgFunctionDefinition node
	 */
	assert(currentFunction != NULL);

	/*!
	 * The list of parameters of the current function.
	 */
	SgFunctionParameterList * paramsList =
			currentFunction->get_declaration()->get_parameterList();
	/*!
	 * This is the list of the formal arguments of a function.
	 */
	SgInitializedNamePtrList initNamePtrList = paramsList->get_args();

	/*!
	 * This vectror will hold the list of parameter types.
	 */
	std::vector<TypeOfTerm*> typeOfParameters;

	/*!
	 * iterate ober the list of formal args.
	 */
	for (SgInitializedNamePtrList::iterator it = initNamePtrList.begin();
			it != initNamePtrList.end(); it++) {
		/*!
		 * Push the current type in the list of args types.
		 */

		SgType * currentType = (*it)->get_type();

		/*
		 * TODO: modify here if we want pointers to be different types
		 * that the non pointer ones!
		 */
		SgPointerType * pointerDerefExpression = isSgPointerType(currentType);
		while (pointerDerefExpression) {
			currentType = pointerDerefExpression->get_base_type();
			pointerDerefExpression = isSgPointerType(currentType);
		}

		std::string currentStringname = this->getCurrentTypeOfVariable(
				currentType);
		/*
		 * Here we check if the current type is equal with t_o.
		 * If this is true, we change it to t_n, since we are in the
		 * initial definitions of the input program's functions!!
		 */
		if (currentStringname == this->typeX) {
			typeOfParameters.push_back(new SimpleTypeOfTerm(this->typeY));

		} else {
			typeOfParameters.push_back(new SimpleTypeOfTerm(currentStringname));

		}
	}

	/*!
	 * Generate new operator with type expression for the current one.
	 */

	/*
	 * First check the return type, if is typeX
	 */

	SgType * retTypeCurrent =
			currentFunction->get_declaration()->get_orig_return_type();
	/*
	 * TODO: modify here if we want pointers to be different types
	 * that the non pointer ones!
	 */
	SgPointerType * pointerDerefExpressionRet = isSgPointerType(retTypeCurrent);
	while (pointerDerefExpressionRet) {
		retTypeCurrent = pointerDerefExpressionRet->get_base_type();
		pointerDerefExpressionRet = isSgPointerType(retTypeCurrent);
	}

	std::string returnTypeC = getCurrentTypeOfVariable(retTypeCurrent);

	//TODO: POTENTIAL BUG HERE! The way in which we take the return type does not seems to be
	//good!!!

	if (returnTypeC == this->typeX) {
		returnTypeC = this->typeY;
	}

	MultiArgsOperatorWithTypes * functionTypeOperator =
			new MultiArgsOperatorWithTypes(
					currentFunction->get_declaration()->get_name().str(),
					new SimpleTypeOfTerm(returnTypeC), typeOfParameters,
					currentFunction->unparseToCompleteString());
	return functionTypeOperator;
}

void RetypeOperationAnalyzer::analyseReversedPreorderJustDefinitions() {
	std::vector<SgNode*>::reverse_iterator rit = this->statementList.rbegin();
	for (; rit != this->statementList.rend(); ++rit) {
		SgNode * node = *rit;

		switch (node->variantT()) {
		case V_SgFunctionDefinition: {

			/*!
			 * Transform the current node into SgFunctionDeclaration.
			 */
			SgFunctionDefinition * currentFunction = isSgFunctionDefinition(
					node);

			MultiArgsOperatorWithTypes * functionTypeOperator =
					returnTypeofFctDef(currentFunction);

			/*!
			 * Push the current identified user provided operator.
			 */
			this->listOfOldOperatorsWithTypes.push_back(functionTypeOperator);
			/*!
			 * Push the location info for the current operator.
			 */
			this->listOfUniqueCallIdentifiers.push_back(
					new OperatorCallUniqueIdentifier(
							currentFunction->get_file_info()->get_raw_line(),
							currentFunction->get_file_info()->get_raw_col(),
							currentFunction->get_file_info()->get_raw_filename()));

			break;
		}
		default: {
			break;
		}
		}
	}

	/*
	 * Finally set the initial types for all the terms!
	 */
	setTypesForTypeOfTerms();
}

void RetypeOperationAnalyzer::setTypesForTypeOfTerms() {

	for (std::vector<OperatorWithTypes*>::iterator oldIt =
			this->listOfOldOperatorsWithTypes.begin();
			oldIt != listOfOldOperatorsWithTypes.end(); ++oldIt) {
		(*oldIt)->setTypeX(this->typeX);
		(*oldIt)->setTypeY(this->typeY);
		(*oldIt)->setTypeCast(this->typeY_cast);
		(*oldIt)->setTypeCastToOld(this->typeX_cast);
	}

	for (std::vector<OperatorWithTypes*>::iterator oldItInitDefs =
			this->listOfInitialDefinitionsFunctions.begin();
			oldItInitDefs != this->listOfInitialDefinitionsFunctions.end();
			++oldItInitDefs) {
		(*oldItInitDefs)->setTypeX(this->typeX);
		(*oldItInitDefs)->setTypeY(this->typeY);
		(*oldItInitDefs)->setTypeCast(this->typeY_cast);
		(*oldItInitDefs)->setTypeCastToOld(this->typeX_cast);
	}

	for (std::vector<OperatorWithTypes*>::iterator newIt =
			this->listOfNewOperatorsWithTypes.begin();
			newIt != this->listOfNewOperatorsWithTypes.end(); ++newIt) {
		(*newIt)->setTypeX(this->typeX);
		(*newIt)->setTypeY(this->typeY);
		(*newIt)->setTypeCast(this->typeY_cast);
		(*newIt)->setTypeCastToOld(this->typeX_cast);
	}
}

std::vector<std::string> RetypeOperationAnalyzer::getTypeArrayForExpression(
		SgExpression * currentExpression) {

	std::string newOperatorTypeString;
	std::vector<std::string> newOperatorTypeStringArray;
	std::string newOperatorTypeForHighOrderOp;

	string currentTypeString = "";

	/*
	 * TODO: modify here if we want pointers to be different types
	 * that the non pointer ones!
	 */
	SgPointerDerefExp * pointerDerefExpression = isSgPointerDerefExp(
			currentExpression);
	while (pointerDerefExpression) {
		currentExpression = pointerDerefExpression->get_operand();
		pointerDerefExpression = isSgPointerDerefExp(currentExpression);
	}

	/*
	 * Similar for references!
	 */
	SgAddressOfOp * addresOfVar = isSgAddressOfOp(currentExpression);
	while (addresOfVar) {
		currentExpression = addresOfVar->get_operand();
		addresOfVar = isSgAddressOfOp(currentExpression);
	}

	/*
	 * First check if this is a cast expression. If this is true, just get its operand
	 */

	if (SgCastExp * currentCastExp = isSgCastExp(currentExpression)) {
		//currentExpression = currentCastExp->get_operand();
	}

	if (SgVarRefExp * theOtherRefExp = isSgVarRefExp(currentExpression)) {
		/*!
		 * First case, when the current node is a variable reference.
		 */
		SgInitializedName * otherVarInitName =
				theOtherRefExp->get_symbol()->get_declaration();

		currentTypeString = getCurrentTypeOfVariable(
				otherVarInitName->get_type());

		// Check if we can retype this type or not

		if (currentTypeString == this->typeX) {
			/*
			 * Check if we are in the scope of retype for the variable
			 * definition.
			 */
			if (checkIfVarIsInRetypeScope(theOtherRefExp)) {
				/*!
				 * We can retype this variable. So we should add typeY
				 * union with cast of typeY, and with cast of type_X for
				 * flow out or for
				 * user preference.!
				 */
				newOperatorTypeString = typeY;
				newOperatorTypeStringArray.push_back(typeY);
				newOperatorTypeStringArray.push_back(typeX_cast);
			} else {
				/*!
				 * We can't retype this argument.
				 * We add typeX, and the cast operator to typeY here.
				 */
				newOperatorTypeString = typeX;
				newOperatorTypeStringArray.push_back(typeX);
				newOperatorTypeStringArray.push_back(typeY_cast);
			}

		} else {
			/*!
			 * This is not a type we intend to retype. Its type is not typeX
			 * So just require a operator for the current type.
			 */
			newOperatorTypeString = currentTypeString;
			newOperatorTypeStringArray.push_back(newOperatorTypeString);
		}

	} else if (SgValueExp * theOtherValueExp = isSgValueExp(
			currentExpression)) {
		/*!
		 * Second case, when the variable is a constant.
		 */
		newOperatorTypeString = trimSpaces(
				theOtherValueExp->get_type()->unparseToCompleteString());
		if (newOperatorTypeString == typeX) {
			/*!
			 * We should add cast operator, and the initial type.
			 */
			newOperatorTypeStringArray.push_back(typeX);
			newOperatorTypeStringArray.push_back(typeY_cast);
		} else {
			/*!
			 * Not typeX, so we shouldn't touch it.
			 */
			newOperatorTypeStringArray.push_back(newOperatorTypeString);
		}
	} else {
		/*!
		 * Third case. It is a higher order expression.
		 */
#if 1
		cout << currentExpression->unparseToCompleteString() << endl;
#endif
		/*!
		 * Here we have again 2 possibilities:
		 * the other operator could be function call or binary op
		 */
		if (SgFunctionCallExp * functCall = isSgFunctionCallExp(
				currentExpression)) {
			/*!
			 * Function call
			 */
			std::string calledFctName = isSgFunctionRefExp(
					functCall->get_function())->get_symbol()->get_name();
			newOperatorTypeString = getCurrentTypeOfVariable(
					currentExpression->get_type());

			if (newOperatorTypeString == typeX) {
				if (std::find(this->definedSymbolsList.begin(),
						this->definedSymbolsList.end(), calledFctName)
						!= this->definedSymbolsList.end()) {
					/*!
					 * This operator is implicitely modified by retype.
					 * We should consider its type to be the new type,
					 * or the cast op to the initial type.
					 */
					newOperatorTypeString = typeY;
					newOperatorTypeStringArray.push_back(typeY);
					newOperatorTypeStringArray.push_back(typeX_cast);

				} else {
					/*!
					 * We do not have a definition so we can't implicitiley retype it
					 * Add cast here, or the original type, since we are here iff
					 * the current type is typeX. This requires user def op!
					 * TODO: CHECK HERE
					 */
					newOperatorTypeStringArray.push_back(typeX);
					newOperatorTypeStringArray.push_back(typeY_cast);
					newOperatorTypeStringArray.push_back(typeY);

				}
			} else {
				/*!
				 * This type shouldn't be modified by us!
				 *
				 */
				newOperatorTypeStringArray.push_back(newOperatorTypeString);
			}
		} else if (SgBinaryOp * theOtherBinOp = isSgBinaryOp(
				currentExpression)) {
			/*!
			 * Binary operator. We consider this to be retypeable
			 * iff typeY is primitive type!
			 * TODO: Check for OO languages here!
			 */

			if (getCurrentTypeOfVariable(currentExpression->get_type())
					== typeX) {
				/*!
				 * We should try to retype this.
				 */

				if (this->typeYIsPrimitive) {
					/*!
					 * The definition is available, for both types
					 * and the user shouldn't provide
					 * any operator for it.
					 * This can take both of the types!
					 * TODO: check here. Maybe we have to analyze
					 * the retypeability of its arguments,
					 * for knowing which type it should take
					 */
					newOperatorTypeString = typeY;
					newOperatorTypeStringArray.push_back(typeY);
					newOperatorTypeStringArray.push_back(typeY_cast);
					newOperatorTypeStringArray.push_back(typeX);
				} else {
					/*!
					 * The definition isn't available. This may be retyped by
					 * user providing operator or by casts, or remain the same
					 * TODO: check here
					 */
					newOperatorTypeString = typeY;
					newOperatorTypeStringArray.push_back(typeY);
					newOperatorTypeStringArray.push_back(typeY_cast);
					newOperatorTypeStringArray.push_back(typeX);
				}
			} else {
				/*!
				 * We shouldn't try to retype this.
				 */
				newOperatorTypeString = getCurrentTypeOfVariable(
						currentExpression->get_type());
				newOperatorTypeStringArray.push_back(newOperatorTypeString);
			}

		} else if (SgUnaryOp * unaryOp = isSgUnaryOp(currentExpression)) {
			/*
			 * very similar with the case of the binary operator here.
			 */
			if (getCurrentTypeOfVariable(currentExpression->get_type())
					== typeX) {
				/*!
				 * We should try to retype this.
				 */

				if (this->typeYIsPrimitive) {
					/*!
					 * The definition is available, for both types
					 * and the user shouldn't provide
					 * any operator for it.
					 * This can take both of the types!
					 * TODO: check here. Maybe we have to analyze
					 * the retypeability of its arguments,
					 * for knowing which type it should take
					 */
					newOperatorTypeString = typeY;
					newOperatorTypeStringArray.push_back(typeY);
					newOperatorTypeStringArray.push_back(typeY_cast);
					newOperatorTypeStringArray.push_back(typeX);
				} else {
					/*!
					 * The definition isn't available. This may be retyped by
					 * user providing operator or by casts, or remain the same
					 * TODO: check here
					 */
					newOperatorTypeString = typeY;
					newOperatorTypeStringArray.push_back(typeY);
					newOperatorTypeStringArray.push_back(typeY_cast);
					newOperatorTypeStringArray.push_back(typeX);
				}
			} else {
				/*!
				 * We shouldn't try to retype this.
				 */
				newOperatorTypeString = getCurrentTypeOfVariable(
						currentExpression->get_type());
				newOperatorTypeStringArray.push_back(newOperatorTypeString);
			}
		} else if (SgSizeOfOp * sizeofCurrentOp = isSgSizeOfOp(
				currentExpression)) {
			/*
			 * Sizeof operator. We considere it to be retypeable, only
			 * for primitive data types!
			 */
			if (getCurrentTypeOfVariable(currentExpression->get_type())
					== typeX) {
				/*!
				 * We should try to retype this.
				 */

				if (this->typeYIsPrimitive) {
					/*!
					 * The definition is available, for both types
					 * and the user shouldn't provide
					 * any operator for it.
					 * This can take both of the types!
					 * TODO: check here. Maybe we have to analyze
					 * the retypeability of its arguments,
					 * for knowing which type it should take
					 */
					newOperatorTypeString = typeY;
					newOperatorTypeStringArray.push_back(typeY);
					newOperatorTypeStringArray.push_back(typeY_cast);
					newOperatorTypeStringArray.push_back(typeX);
				} else {
					/*!
					 * The definition isn't available. This may be retyped by
					 * user providing operator or by casts, or remain the same
					 * TODO: check here
					 */
					newOperatorTypeString = typeY;
					newOperatorTypeStringArray.push_back(typeY);
					newOperatorTypeStringArray.push_back(typeY_cast);
					newOperatorTypeStringArray.push_back(typeX);
				}
			} else {
				/*!
				 * We shouldn't try to retype this.
				 */
				newOperatorTypeString = getCurrentTypeOfVariable(
						currentExpression->get_type());
				newOperatorTypeStringArray.push_back(newOperatorTypeString);
			}
		} else {
			/*!
			 * We shouldn't be here!
			 * Assert a false thing, since this case is not handled yet!!!
			 */
			assert(false);
		}
	}

	return newOperatorTypeStringArray;
}

void RetypeOperationAnalyzer::analyseReversedPreorder() {
	std::vector<SgNode*>::reverse_iterator rit = this->statementList.rbegin();
	for (; rit != this->statementList.rend(); ++rit) {
		SgNode * node = *rit;
		/*
		 * Here we are interested only in function call,
		 * for getting the constraints on the function definition.
		 * We have 2 cases here: binary ops, or n-arry ops.
		 */
		SgBinaryOp * binaryOp = isSgBinaryOp(node);
		if (binaryOp) {
			/*
			 * Binary operator case
			 */
			SgExpression* lhsOperand = binaryOp->get_lhs_operand();
			SgExpression* rhsOperand = binaryOp->get_rhs_operand();

			std::vector<std::string> typesOfLhs = getTypeArrayForExpression(
					lhsOperand);
			std::vector<std::string> typesOfRhs = getTypeArrayForExpression(
					rhsOperand);

			TypeOfTerm * lhsOpClass = new SimpleTypeOfTerm(typesOfLhs);
			TypeOfTerm * rhsOpClass = new SimpleTypeOfTerm(typesOfRhs);

			vector<string> returnTypes;

			string initialReturnStringType = getCurrentTypeOfVariable(
					binaryOp->get_type());
			if (initialReturnStringType == typeX) {
				/*!
				 * This type is of interest for us.
				 * Here we are in the case of binary operator,
				 * so we should check whether or not typeY is primitive type.
				 */

				/*!
				 * The definition of bin ops is unretypeable, excepting the
				 * case of primitives!
				 */
				if (this->typeYIsPrimitive) {
					/*!
					 * The definition is available and the user shouldn't provide
					 * any operator for it. This might return the both types
					 * TODO: check if this is valid
					 */
					returnTypes.push_back(typeY);
					returnTypes.push_back(typeY_cast);
					returnTypes.push_back(typeX);
				} else {
					/*!
					 * The definition isn't available. This may be retyped by
					 * user providing operator or by casts.
					 * So, we should push both of them
					 * TODO: check here.
					 */
					returnTypes.push_back(typeX);
					returnTypes.push_back(typeY_cast);
					returnTypes.push_back(typeY);
				}
			} else {
				/*!
				 * We do not care about this type.
				 * Just keep the original one.
				 */
				returnTypes.push_back(initialReturnStringType);
			}

			/*
			 * Now we have the types for the binary ops args.
			 * So, construct the constraints on the operator.
			 */

			BinaryOperatorWithTypes * originalChildExpressionOperator =
					new BinaryOperatorWithTypes(
							getVariantName(binaryOp->variantT()),
							new SimpleTypeOfTerm(initialReturnStringType),
							new SimpleTypeOfTerm(
									trimSpaces(
											getCurrentTypeOfVariable(
													binaryOp->get_lhs_operand()->get_type()))),
							new SimpleTypeOfTerm(
									trimSpaces(
											getCurrentTypeOfVariable(
													binaryOp->get_rhs_operand()->get_type()))),
							binaryOp->unparseToCompleteString(), false);

			BinaryOperatorWithTypes * newChildExpressionOperator =
					new BinaryOperatorWithTypes(
							getVariantName(binaryOp->variantT()),
							new SimpleTypeOfTerm(returnTypes), lhsOpClass,
							rhsOpClass, binaryOp->unparseToCompleteString());

			// TODO: check if we need any more this attribute!
			// I don't think so... this->typeY here is wrong is we use it
			// anymore.

			AstAttribute * newAttribute =
					new BinnaryReturnTypePersistentAttribute(returnTypes,
							initialReturnStringType, (returnTypes.size() > 1),
							newChildExpressionOperator);

			if (!binaryOp->attributeExists("SupportedTypes")) {

				this->listOfOldOperatorsWithTypes.push_back(
						originalChildExpressionOperator);

				this->listOfNewOperatorsWithTypes.push_back(
						newChildExpressionOperator);

				/*!
				 * The file info is in parentBinOp
				 */
				this->listOfUniqueCallIdentifiers.push_back(
						new OperatorCallUniqueIdentifier(
								binaryOp->get_file_info()->get_raw_line(),
								binaryOp->get_file_info()->get_raw_col(),
								binaryOp->get_file_info()->get_raw_filename()));

				binaryOp->addNewAttribute("SupportedTypes", newAttribute);

			}
		} else {
			SgFunctionCallExp * functionCall = isSgFunctionCallExp(node);

			if (functionCall) {
				/*
				 * Case of being in a function call.
				 * First get the expression list from the function call.
				 */
				SgExprListExp * exprList = functionCall->get_args();

				/*!
				 * It should always be a function call.
				 * The existence of parentExprList means that we are in a function
				 * call.
				 */
#if 0
				std::cout << "SG EXPR LIST EXPR: "
				<< parent->unparseToCompleteString()
				<< std::endl;
#endif
				/*!
				 * The list of the actual parameters.
				 */
				SgExpressionPtrList actualParams = exprList->get_expressions();

				/*!
				 * The array of the types of the actual parameters
				 * for the current call, after retype.
				 */
				std::vector<TypeOfTerm*> typeOfFunctionArgs;

				/*!
				 * The array of initial types, for showing them to the user.
				 */
				std::vector<TypeOfTerm*> initialTypeOfParameters;

				for (SgExpressionPtrList::iterator it = actualParams.begin();
						it != actualParams.end(); ++it) {

					string originalTypeArg = "";
					cout << (*it)->unparseToCompleteString() << "\n";
					std::vector<std::string> typesOfArg =
							getTypeArrayForExpression(*it);
					originalTypeArg = this->getCurrentTypeOfVariable(
							(*it)->get_type());

					initialTypeOfParameters.push_back(
							new SimpleTypeOfTerm(originalTypeArg));

					typeOfFunctionArgs.push_back(
							new SimpleTypeOfTerm(typesOfArg));
				}

				//std::vector<TypeOfTerm*> returnTypesOfOpClass; //returnTypesOfOpForHighOrderOps

				SgType * currentTypeRet = functionCall->get_type();

				/*
				 * TODO: modify here if we want pointers to be different types
				 * that the non pointer ones!
				 */
				SgPointerType * pointerDerefExpression = isSgPointerType(
						currentTypeRet);
				while (pointerDerefExpression) {
					currentTypeRet = pointerDerefExpression->get_base_type();
					pointerDerefExpression = isSgPointerType(currentTypeRet);
				}

				std::string calledFunctionFinalStringType =
						this->getCurrentTypeOfVariable(currentTypeRet);

				std::vector<std::string> returnTypesAsString;

				if (calledFunctionFinalStringType == this->typeX) {
					/*!
					 * We should retype this. So add ass types typeY and typeY_cast
					 */
					returnTypesAsString.push_back(typeY);
					returnTypesAsString.push_back(typeY_cast);
					returnTypesAsString.push_back(typeX);

				} else {
					/*!
					 * We shouldn't retype this (not typeX).
					 * Add just the initial type.
					 */
					returnTypesAsString.push_back(
							calledFunctionFinalStringType);
				}

				SgFunctionCallExp * functionCall2;

				MultiArgsOperatorWithTypes * newChildExpressionOperator =
						new MultiArgsOperatorWithTypes(
								isSgFunctionRefExp(functionCall->get_function())->get_symbol()->get_name().str(),
								new SimpleTypeOfTerm(returnTypesAsString),
								typeOfFunctionArgs,
								functionCall->unparseToCompleteString());

				MultiArgsOperatorWithTypes * originalChildExpressionOperator =
						new MultiArgsOperatorWithTypes(
								isSgFunctionRefExp(functionCall->get_function())->get_symbol()->get_name().str(),
								new SimpleTypeOfTerm(
										getCurrentTypeOfVariable(
												functionCall->get_type())),
								initialTypeOfParameters,
								functionCall->unparseToCompleteString());

				AstAttribute * newAttribute =
						new BinnaryReturnTypePersistentAttribute(
								returnTypesAsString,
								calledFunctionFinalStringType, /* TODO: FIX THIS!!!!*/
								returnTypesAsString.size() > 1,
								newChildExpressionOperator);

				// TODO: check if here we should try to add more times operators!

				if (!functionCall->attributeExists("SupportedTypes")) {

					this->listOfOldOperatorsWithTypes.push_back(
							originalChildExpressionOperator);

					this->listOfNewOperatorsWithTypes.push_back(
							newChildExpressionOperator);

					this->listOfUniqueCallIdentifiers.push_back(
							new OperatorCallUniqueIdentifier(
									functionCall->get_file_info()->get_raw_line(),
									functionCall->get_file_info()->get_raw_col(),
									functionCall->get_file_info()->get_raw_filename()));

					functionCall->addNewAttribute("SupportedTypes",
							newAttribute);
				}

			} else {
				/*
				 * The final case we have to handle:
				 * initial operator definitions, that are to be kept!
				 * Here we just have to replace all the initial types,
				 * to the new type, since we always retype variable definitions in
				 * declarations.
				 */

				/*!
				 * Transform the current node into SgFunctionDeclaration.
				 */
				SgFunctionDefinition * currentFunction = isSgFunctionDefinition(
						node);

				if (currentFunction) {

					// TODO: check if we should call here returnTypeofFctDefForInputProgramDefs,
					// or returnTypeofFctDef. Should we change t_o with t_n here?
					// or just in the usages?
					MultiArgsOperatorWithTypes * functionTypeOperator =
							returnTypeofFctDefForInputProgramDefs(
									currentFunction);

					/*!
					 * Push the current identified user provided operator.
					 */
					this->listOfInitialDefinitionsFunctions.push_back(
							functionTypeOperator);
					/*!
					 * Push the location info for the current operator.
					 */
					this->listOfUniqueCallInitialDefinition.push_back(
							new OperatorCallUniqueIdentifier(
									currentFunction->get_file_info()->get_raw_line(),
									currentFunction->get_file_info()->get_raw_col(),
									currentFunction->get_file_info()->get_raw_filename()));
				} else {

					/*
					 * Unary operators, including the casts here!!!
					 */

					SgUnaryOp * unaryOp = isSgUnaryOp(node);
					if (unaryOp) {

						/*!
						 * The array of the types of the actual parameters
						 * for the current call, after retype.
						 */
						std::vector<TypeOfTerm*> typeOfFunctionArgs;

						/*!
						 * The array of initial types, for showing them to the user.
						 */
						std::vector<TypeOfTerm*> initialTypeOfParameters;

						/*
						 * The unique operand of the unary op!
						 */
						SgExpression * currentOperand = unaryOp->get_operand();

						string originalTypeArg = "";
						cout << currentOperand->unparseToCompleteString()
								<< "\n";
						std::vector<std::string> typesOfArg =
								getTypeArrayForExpression(currentOperand);
						originalTypeArg = this->getCurrentTypeOfVariable(
								currentOperand->get_type());

						initialTypeOfParameters.push_back(
								new SimpleTypeOfTerm(originalTypeArg));

						typeOfFunctionArgs.push_back(
								new SimpleTypeOfTerm(typesOfArg));

						SgType * currentTypeRet = unaryOp->get_type();

						/*
						 * TODO: modify here if we want pointers to be different types
						 * that the non pointer ones!
						 */
						SgPointerType * pointerDerefExpression =
								isSgPointerType(currentTypeRet);
						while (pointerDerefExpression) {
							currentTypeRet =
									pointerDerefExpression->get_base_type();
							pointerDerefExpression = isSgPointerType(
									currentTypeRet);
						}

						std::string calledFunctionFinalStringType =
								this->getCurrentTypeOfVariable(currentTypeRet);

						std::vector<std::string> returnTypesAsString;

						if (calledFunctionFinalStringType == this->typeX) {
							/*!
							 * We should retype this. So add ass types typeY and typeY_cast
							 */
							returnTypesAsString.push_back(typeY);
							returnTypesAsString.push_back(typeY_cast);
							returnTypesAsString.push_back(typeX);

						} else {
							/*!
							 * We shouldn't retype this (not typeX).
							 * Add just the initial type.
							 */
							returnTypesAsString.push_back(
									calledFunctionFinalStringType);
						}

						MultiArgsOperatorWithTypes * newChildExpressionOperator =
								new MultiArgsOperatorWithTypes(
										getVariantName(unaryOp->variantT()),
										new SimpleTypeOfTerm(
												returnTypesAsString),
										typeOfFunctionArgs,
										unaryOp->unparseToCompleteString());

						MultiArgsOperatorWithTypes * originalChildExpressionOperator =
								new MultiArgsOperatorWithTypes(
										getVariantName(unaryOp->variantT()),
										new SimpleTypeOfTerm(
												getCurrentTypeOfVariable(
														unaryOp->get_type())),
										initialTypeOfParameters,
										unaryOp->unparseToCompleteString());

						AstAttribute * newAttribute =
								new BinnaryReturnTypePersistentAttribute(
										returnTypesAsString, "NON", /* TODO: FIX THIS!!!!*/
										returnTypesAsString.size() > 1,
										newChildExpressionOperator);

						if (!unaryOp->attributeExists("SupportedTypes")) {

							this->listOfOldOperatorsWithTypes.push_back(
									originalChildExpressionOperator);

							this->listOfNewOperatorsWithTypes.push_back(
									newChildExpressionOperator);

							this->listOfUniqueCallIdentifiers.push_back(
									new OperatorCallUniqueIdentifier(
											unaryOp->get_file_info()->get_raw_line(),
											unaryOp->get_file_info()->get_raw_col(),
											unaryOp->get_file_info()->get_raw_filename()));

							unaryOp->addNewAttribute("SupportedTypes",
									newAttribute);
						}

					} else {

						/*
						 * Do nothing here. We are in a program construct that
						 * is not interesting for us.
						 */
					}
				}
			}
		}
	}

	/*
	 * Finally set the input types for all terms.
	 */
	setTypesForTypeOfTerms();
}

void RetypeOperationAnalyzer::visit(SgNode* node) {

	statementList.push_back(node);
	return;

#if 0
	this->outputOperator(node->unparseToCompleteString());
	return;
#endif

}

void RetypeOperationAnalyzer::generateDefinedSymbolsList() {
	std::ifstream fin(this->stFile.c_str(), ios::in);

	while (!fin.eof()) {
		std::string type, mangledName, file;
		fin >> type >> mangledName >> file;
		this->definedSymbolsList.push_back(mangledName);
	}
}

void RetypeOperationAnalyzer::outputOperator(std::string outputMessage) {
	assert(this->outfile != NULL);
	(*outfile) << outputMessage;
}

void RetypeOperationAnalyzer::transformToConstraints() {
// TODO: Complete here
}

void RetypeOperationAnalyzer::solveConstraints() {
	/*
	 TypeInferenceConstraintSolver constraintSolver;
	 constraintSolver.testHarness();
	 */
}

std::vector<TypedLambdaExpression *> RetypeOperationAnalyzer::getGeneratedLambdaExpressions(
		std::vector<OperatorWithTypes*> arrayOfOperatorsWithTypes) {
	/*!
	 * First empty the list of lambda expressions!
	 */
	std::vector<OperatorCallUniqueIdentifier *>::iterator itCallId =
			this->listOfUniqueCallIdentifiers.begin();
	this->arrayOfLambdaExpressions.clear();
	for (std::vector<OperatorWithTypes *>::iterator it =
			arrayOfOperatorsWithTypes.begin();
			it != arrayOfOperatorsWithTypes.end(); it++) {
		this->arrayOfLambdaExpressions.push_back(
				new TypedLambdaExpression((*it), (*itCallId), this->typeX,
						this->typeY, this->typeY_cast, this->typeX_cast));
		itCallId++;
	}
	return arrayOfLambdaExpressions;
}

CollectionOfOperatorDefinitions* RetypeOperationAnalyzer::generateListOfIdentifiedOps(
		bool isUserDefinedOpsFile) {
// operators to lamdba expressions
	std::vector<TypedLambdaExpression *> listOfLambdaExp;
	std::vector<TypedLambdaExpression *> listOfLambdaExpInitialDefs;
	std::vector<OldOperatorDefinition *> listOfOperatorUsages;
	std::vector<NewOperatorDefinition *> listOfOldOperatorDefinitions;
	std::vector<NewOperatorDefinition *> listOfNewOperatorDefinitions;

	if (isUserDefinedOpsFile == true) {
		/*!
		 * If the current analyzed source file is the file provided by the user
		 * with the operator definition, then we consider the types as the old ones,
		 * aka the original ones.
		 */
		listOfLambdaExp = this->getGeneratedLambdaExpressions(
				this->listOfOldOperatorsWithTypes);

		/*!
		 * Generate the list of operator definitions. Again, this is dependent on
		 * the kind of analyze we are doing: user provided ops, or the program
		 * under retype
		 */

		/*
		 * Add also the location information!
		 * listOfUniqueCallInitialDefinition
		 */
		listOfNewOperatorDefinitions =
				NewOperatorDefinition::generateOperatorDefsFromLambdaExpressions(
						listOfLambdaExp, listOfUniqueCallIdentifiers);
	} else {
		/*!
		 * If the current source file is one of the program under retyping, then
		 * we need to consider the new generated operators from the new basis.
		 */
		listOfLambdaExp = this->getGeneratedLambdaExpressions(
				this->listOfNewOperatorsWithTypes);

		std::vector<TypedLambdaExpression *> oldUsages =
				this->getGeneratedLambdaExpressions(
						this->listOfOldOperatorsWithTypes);

		/*
		 * Add also the location information!
		 * listOfUniqueCallInitialDefinition
		 */

		/*listOfOperatorUsages =
		 OldOperatorDefinition::generateOperatorDefsFromLambdaExpressions(
		 listOfLambdaExp, listOfUniqueCallIdentifiers);*/

		listOfLambdaExpInitialDefs = this->getGeneratedLambdaExpressions(
				this->listOfInitialDefinitionsFunctions);

		listOfOldOperatorDefinitions =
				NewOperatorDefinition::generateOperatorDefsFromLambdaExpressions(
						listOfLambdaExpInitialDefs,
						listOfUniqueCallInitialDefinition);

		listOfOperatorUsages =
				OldOperatorDefinition::generateOperatorCallsFromLambdaExpressions(
						listOfLambdaExp, listOfUniqueCallInitialDefinition,
						listOfLambdaExpInitialDefs, oldUsages);

	}

//std::vector<OldOperatorDefinition *> listOfAllTheDefinitionsForOldOps

//std::vector<OldOperatorDefinition *> listOfAllTheDefinitionsForOldOps =
//		OldOperatorDefinition::generateOperatorDefsFromLambdaExpressions(
//				listOfLambdaExp);

	CollectionOfOperatorDefinitions * myColection =
			new CollectionOfOperatorDefinitions(listOfOperatorUsages,
					listOfOldOperatorDefinitions, listOfNewOperatorDefinitions);
#if 0
	/*!
	 * Temporary test of serialization mechanism.
	 */
	CollectionOfOperatorDefinitions::save_operator_defs(*myColection,
			"/home/alex/Development/TypeChanger/TEST_SERIALIZE!!!");

	std::stringstream initialColection, restoredColection;
	std::string initialColString, restoredColString;
	initialColection << *myColection;
	initialColString = initialColection.str();

	cout << myColection << endl;

	CollectionOfOperatorDefinitions restoredCol;
	CollectionOfOperatorDefinitions::restore_operator_defs(restoredCol,
			"/home/alex/Development/TypeChanger/TEST_SERIALIZE!!!");
	cout << restoredCol << endl;

	restoredColection << restoredCol;
	restoredColString = restoredColection.str();

	assert(initialColString == restoredColString);
	if (initialColString == restoredColString) {
		cout << "EQUAL!!!!!!" << std::endl;
	} else {
		cout << "NOT EQUAL!!!!!!!" << std::endl;
	}
#endif
	return myColection;
}

} /* namespace ST */
