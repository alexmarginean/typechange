/*
 * BinaryOperatorSimpleType.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: alex
 */

#include <rose.h>

#include <SimpleTypeOfTerm.h>
#include <utils.h>

namespace analyzer {

std::string SimpleTypeOfTerm::getTypeName() {
	return "HELLO";
}
SimpleTypeOfTerm::SimpleTypeOfTerm() {

}
SimpleTypeOfTerm::SimpleTypeOfTerm(std::string type) {
	this->typeArray.push_back(type);
}

SimpleTypeOfTerm::SimpleTypeOfTerm(std::vector<std::string> typeArray) {
	this->typeArray = typeArray;
}

SimpleTypeOfTerm::~SimpleTypeOfTerm() {

}
std::string SimpleTypeOfTerm::toString() {
	std::string output = "";

	if (this->typeArray.size()) {

		for (int i = 0; i < this->typeArray.size() - 1; i++) {
			output += this->typeArray[i];
			output += " ∪ ";
		}
		output += this->typeArray[this->typeArray.size() - 1];
#if 0
		for(std::vector<std::string>::iterator it = this->typeArray.begin(); it != this->typeArray.end(); ++it) {

		}
#endif
	}

	return output;
}

SimpleTypeOfTerm::SimpleTypeOfTerm(SimpleTypeOfTerm * newType) {

	//TODO: Should not enter here!
	assert(false);
	//TODO: Fix here!!!
	this->typeArray = newType->typeArray;
}

bool SimpleTypeOfTerm::operator==(SimpleTypeOfTerm t2) {
	//TODO: Should not enter here!
	assert(false);
	//TODO: Fix here!!!
	return this->typeArray == t2.typeArray;
}

std::string SimpleTypeOfTerm::retypeTypesToString(std::string typeX,
		std::string typeY, std::string typeCast, std::string typeCastOld) {
	std::string output = "";

	if (this->typeArray.size() > 1) {
		output += "( ";
	}

	for (int i = 0; i < this->typeArray.size() - 1; i++) {

#if 1
		output+="(";
		output+= typeArray[i];
		output+= ")";
#endif


		if (trimSpaces(this->typeArray[i]) == typeX) {
			output += "τ";
			output += "_O"; //SUBSCRIPT_O;
			output += " ∪ ";
		} else if (trimSpaces(this->typeArray[i]) == typeY) {
			output += "τ";
			output += "_N"; //SUBSCRIPT_N;
			output += " ∪ ";
		} else if (trimSpaces(this->typeArray[i]) == typeCast) {
			output += "τ";
			output += "_castN"; //SUBSCRIPT_C;
			output += " ∪ ";
		} else if (trimSpaces(this->typeArray[i]) == typeCastOld) {

			output += "τ";
			output += "_castO"; //SUBSCRIPT_C;
			output += " ∪ ";
		} else {
			output += "τ";
			output += "_otherType"; //SUBSCRIPT_U;
			output += " ∪ ";
		}
	}


#if 1
	output+="(";
	output+= typeArray[typeArray.size()-1];
	output+= ")";

#endif

	if (trimSpaces(this->typeArray[this->typeArray.size() - 1]) == typeX) {
		output += "τ";
		output += "_O"; //SUBSCRIPT_O;
	} else if (trimSpaces(this->typeArray[this->typeArray.size() - 1])
			== typeY) {
		output += "τ";
		output += "_N"; //SUBSCRIPT_N;
	} else if (trimSpaces(this->typeArray[this->typeArray.size() - 1])
			== typeCast) {
		output += "τ";
		output += "_castO"; //SUBSCRIPT_C;
	} else if (trimSpaces(this->typeArray[this->typeArray.size() - 1]) == typeCastOld) {

		output += "τ";
		output += "_castO"; //SUBSCRIPT_C;
		output += " ∪ ";
	} else {
		output += "τ";
		output += "_otherType";//SUBSCRIPT_U;
	}

	if (this->typeArray.size() > 1) {
		output += " )";
	}

	return output;
}

} /* namespace analyzer */

