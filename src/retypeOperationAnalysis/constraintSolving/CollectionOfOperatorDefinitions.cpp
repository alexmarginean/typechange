/*
 * CollectionOfOperatorDefinitions.cpp
 *
 *  Created on: 26 Mar 2015
 *      Author: alex
 */
#include <CollectionOfOperatorDefinitions.h>
#include <boost/filesystem.hpp>

#include <OldOperatorDefinition.h>
#include <NewOperatorDefinition.h>
#include <TypedLambdaExpression.h>
#include <HighOrderTypeOfTerm.h>

namespace analyzer {

void CollectionOfOperatorDefinitions::save_operator_defs(
		CollectionOfOperatorDefinitions& s, const std::string filename) {
	// make an archive
	std::ofstream ofs(filename.c_str());
	boost::archive::binary_oarchive oa(ofs);
	oa << s;
}

void CollectionOfOperatorDefinitions::safe_save_operator_defs_append(
		CollectionOfOperatorDefinitions& s, const std::string filename) {

	bool fileExists = false;

	if (boost::filesystem::exists(filename)) {
		fileExists = true;
	}

	if (fileExists) {
		CollectionOfOperatorDefinitions restoredCol;
		CollectionOfOperatorDefinitions::restore_operator_defs(restoredCol,
				filename);
		restoredCol.appendCollection(s);
		CollectionOfOperatorDefinitions::save_operator_defs(restoredCol,
				filename);
	} else {
		CollectionOfOperatorDefinitions::save_operator_defs(s, filename);
	}

	return;
}

void CollectionOfOperatorDefinitions::restore_operator_defs(
		CollectionOfOperatorDefinitions& s, const std::string filename) {
	// open the archive
	std::ifstream ifs(filename.c_str());
	boost::archive::binary_iarchive ia(ifs);
	ia >> s;
}

std::ostream& operator <<(std::ostream& os,
		const CollectionOfOperatorDefinitions& exp) {
	os << "List Of Usages: \n";
	for (int i = 0; i < exp.listOfOpUsages_.size(); i++) {
		os << "\n Operator NAME: " << exp.listOfOpUsages_[i]->getOperatorName()
				<< std::endl;
		if( ((OldOperatorDefinition*) (((exp.listOfOpUsages_[i]))))->getOperatorDef() ) {
			os << "Location of DEF: "
			<< *(((OldOperatorDefinition*) (((exp.listOfOpUsages_[i]))))->getOperatorDef()->uniqueCallIdentifier()) << std::endl;
		}
		os << (*(exp.listOfOpUsages_[i])) << std::endl;
	}
	os << "List Of Old Definitions: \n";
	for (int i = 0; i < exp.listOfOpDefinitionsOld_.size(); i++) {
		os << "\n Operator NAME: "
				<< exp.listOfOpDefinitionsOld_[i]->getOperatorName()
				<< std::endl;
		os << (*(exp.listOfOpDefinitionsOld_[i])) << std::endl;
	}
	os << "List Of New Definitions: \n";
	for (int i = 0; i < exp.listOfOpDefinitionsNew_.size(); i++) {
		os << "\n Operator NAME: "
				<< exp.listOfOpDefinitionsNew_[i]->getOperatorName()
				<< std::endl;
		os << (*(exp.listOfOpDefinitionsNew_[i])) << std::endl;
	}
	return os;
}

CollectionOfOperatorDefinitions::CollectionOfOperatorDefinitions(
		std::vector<OldOperatorDefinition*> listofUsages,
		std::vector<NewOperatorDefinition*> listOfOldDefs,
		std::vector<NewOperatorDefinition*> listOfNewDefs) {
	for (int i = 0; i < listofUsages.size(); i++) {
		this->listOfOpUsages_.push_back(listofUsages[i]);
	}

	for (int i = 0; i < listOfOldDefs.size(); i++) {
		this->listOfOpDefinitionsOld_.push_back(listOfOldDefs[i]);
	}

	for (int i = 0; i < listOfNewDefs.size(); i++) {
		this->listOfOpDefinitionsNew_.push_back(listOfNewDefs[i]);
	}
}

CollectionOfOperatorDefinitions::CollectionOfOperatorDefinitions() {
	return;
}

} /* namespace analyzer */
