/*
 * ConstraintError.cpp
 *
 *  Created on: 14 Dec 2015
 *      Author: alex
 */

#include "ConstraintError.h"

#include <z3++.h>
#include <vector>
#include <string>
#include <map>
#include <TypedLambdaExpression.h>
#include <utils.h>

#include <OperatorDefinition.h>
#include <NewOperatorDefinition.h>
#include <OldOperatorDefinition.h>
#include <UsageOfFunction.h>

namespace analyzer {

ConstraintError::ConstraintError(UsageOfFunction* errorUsage,
		std::vector<UsageOfFunction*> handlers,
		std::vector<UsageOfFunction*> availableDefs) {
	this->errorUsage_ = errorUsage;
	this->handlers_ = handlers;
	this->availableDefs_ = availableDefs;
}

ConstraintError::ConstraintError() {
	/*
	 * Nothing here.
	 */
}

std::ostream& operator <<(std::ostream& os, const ConstraintError& exp) {
	os << "Error in usage: " << *(exp.errorUsage_->operatorDef()) << std::endl;
	if (!exp.handlers_.size()) {
		os
				<< "The above constraint cannot be handled by any operator definitions. Please provide one. Acceptable definitions are:"
				<< std::endl;
		os << *(exp.errorUsage_->getCurrentConstraints());

		os << std::endl
				<< "Currently, there are available the following definitions for this usage, but non of them meet the above constraints:"
				<< std::endl;

		std::vector<UsageOfFunction*> currentDefs = (exp.availableDefs_);

		for (std::vector<UsageOfFunction*>::iterator it = currentDefs.begin();
				it != currentDefs.end(); ++it) {
			os << *((*it)->operatorDef()) << std::endl;
		}
	} else if (exp.handlers_.size() > 1) {
		os
				<< "The above constraint is handled by more operator definitions, with the same priority. Retype cannot decide which one is correct."
				<< std::endl;

		os << "The potential handlers for the above constraints are:"
				<< std::endl;

		std::vector<UsageOfFunction*> currentHandlers = exp.handlers_;
		for (std::vector<UsageOfFunction*>::iterator it =
				currentHandlers.begin(); it != currentHandlers.end(); ++it) {
			os << *((*it)->operatorDef()) << std::endl;
		}

	} else {
		/*
		 * We shouldn't be here!
		 * This is not an error, so assert false
		 */
		assert(false);
	}

	return os;
}

ConstraintError::~ConstraintError() {
	// TODO Auto-generated destructor stub
}

} /* namespace analyzer */
