/*
 * ConstraintError.h
 *
 *  Created on: 14 Dec 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_CONSTRAINTERROR_H_
#define SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_CONSTRAINTERROR_H_

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>


#include <ostream>
#include <vector>
#include <UsageOfFunction.h>

namespace analyzer {

class UsageOfFunction;

class ConstraintError {
public:
	ConstraintError();

	ConstraintError(UsageOfFunction* errorUsage,
			std::vector<UsageOfFunction*> handlers,
			std::vector<UsageOfFunction*> availableDefs);

	friend std::ostream& operator <<(std::ostream& os,
			const ConstraintError& exp);

	virtual ~ConstraintError();

protected:
	UsageOfFunction * errorUsage_;
	std::vector<UsageOfFunction*> handlers_;
	std::vector<UsageOfFunction*> availableDefs_;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar & errorUsage_;
		ar & handlers_;
		ar & availableDefs_;

	}

};

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_CONSTRAINTERROR_H_ */
