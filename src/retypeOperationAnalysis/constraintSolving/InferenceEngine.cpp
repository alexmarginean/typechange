/*
 * InferenceEngine.cpp
 *
 *  Created on: 7 Dec 2015
 *      Author: alex
 */

#include "InferenceEngine.h"
#include "OperatorUsagesWithNewDefs.h"
#include "OperatorReplacingMapping.h"
#include "ConstraintError.h"

#include <CollectionOfOperatorDefinitions.h>
#include <TypedLambdaExpression.h>
#include <NewOperatorDefinition.h>
#include <OldOperatorDefinition.h>
#include <TypeInferenceConstraintSolver.h>

namespace analyzer {

InferenceEngine::InferenceEngine() {
	// TODO Auto-generated constructor stub

}

void InferenceEngine::callSolver(
		std::vector<OperatorUsagesWithNewDefs*> newOpsWithUsages) {

	/*
	 * Create the output file for all the constraints.
	 */
	bool shouldOutputConstraints;
	std::ofstream fout_constraints;
	if (this->constraintsOutput_ != "") {
		fout_constraints.open(this->constraintsOutput_.c_str(), ios::out);
		shouldOutputConstraints = true;
	} else {
		shouldOutputConstraints = false;
	}

	for (std::vector<OperatorUsagesWithNewDefs*>::iterator it =
			newOpsWithUsages.begin(); it != newOpsWithUsages.end(); ++it) {

#if 0
		if ((*it)->newDefinitions().size()) {
			std::cout << *((*it)->newDefinitions()[0]);
		}
#endif

		OperatorUsagesWithNewDefs* currentOp = (*it);

		/*
		 * Check if the current operator is a binary operator.
		 * If it is, than add the cast operator as implicit existing!
		 */
		std::string operatorName =
				currentOp->operatorUsages()->getOperatorName();
		bool castOperatorForCurrentExists = this->castOps_;

		if (!this->castOps_ && this->castOpPrimitiveDataTypes_) {

			/*
			 * The default cast operator exists for all the definitions.
			 * We inherently prefer the user provided ops, since this is viewed as
			 * a normal cast, although it is implicit!.
			 */
			castOperatorForCurrentExists = this->castOpPrimitiveDataTypes_;


#if 0
			/*
			 * Hack here. We check whether or not the name starts with Sg, and if this is true,
			 * we assume the current operator is a binary operator. This is not true in general,
			 * and should be fixed!!!! TODO: FIX THIS.
			 */

			if (!operatorName.compare(0, 2, "Sg")) {
				castOperatorForCurrentExists = this->castOpPrimitiveDataTypes_;
			}

			/*
			 if( (operatorName == "SgAddOp" ) || (operatorName == "SgAssignOp") || (operatorName == "SgDivideOp") || (operatorName == "SgEqualityOp")
			 || (operatorName == "SgGreaterOrEqualOp") || (operatorName == "SgGreaterThanOp") || (operatorName == "SgIntegerDivideOp") || (operatorName == "SgIsNotOp")){
			 castOperatorForCurrentExists = this->castOpBinOps_;
			 }
			 */
#endif

		}

		TypeInferenceConstraintSolver* inferenceSolver =
				new TypeInferenceConstraintSolver(currentOp,
						castOperatorForCurrentExists, &fout_constraints,
						shouldOutputConstraints);

		std::map<TypedLambdaExpression *, std::vector<TypedLambdaExpression *> > handlerUsagesMappings =
				inferenceSolver->getUsagesDefinitionsMappings();

		std::vector<ConstraintError*> currentErrors =
				inferenceSolver->errorVector();

		this->errorVector_.insert(this->errorVector_.end(),
				currentErrors.begin(), currentErrors.end());

		this->augmentHandlerUsageMapping(handlerUsagesMappings, currentOp);

		std::vector<OperatorReplacingMapping*> opReplaceMap =
				OperatorReplacingMapping::createOperatorReplacingMapping(
						handlerUsagesMappings);

		this->operatorReplacings_.insert(this->operatorReplacings_.end(),
				opReplaceMap.begin(), opReplaceMap.end());

	}

}

InferenceEngine::~InferenceEngine() {
	// TODO Auto-generated destructor stub
}

const std::ctype_base::mask* InferenceEngine::tokens::get_table() {
	typedef std::ctype<char> cctype;
	static const cctype::mask* const_rc = cctype::classic_table();
	static cctype::mask rc[cctype::table_size];
	std::memcpy(rc, const_rc, cctype::table_size * sizeof(cctype::mask));
	rc[','] = std::ctype_base::space;
	rc[' '] = std::ctype_base::space;
	rc[':'] = std::ctype_base::space;
	return &rc[0];
}

std::vector<std::string> InferenceEngine::returnOperatorsThatAcceptMappings(
		std::vector<OldOperatorDefinition*> usages) {
	std::vector<std::string> operatorsWithMappings;
	for (std::vector<OldOperatorDefinition*>::iterator it = usages.begin();
			it != usages.end(); it++) {
		if ((*it)->getOperatorDef() == NULL) {
			operatorsWithMappings.push_back((*it)->getOperatorName());
		}
	}
	return operatorsWithMappings;
}

std::map<std::string, std::vector<std::string> > InferenceEngine::generateMapOfOperators() {
	std::map<std::string, std::vector<std::string> > mapOfOperators;
	std::string line;
	while (std::getline(inputMappingFile_, line)) {
		std::string key;
		std::stringstream ss(line);
		ss >> key;
		ss.imbue(std::locale(std::locale(), new tokens()));
		std::istream_iterator<std::string> begin(ss);
		std::istream_iterator<std::string> end;
		std::vector<std::string> vstrings(begin, end);
		//std::copy(vstrings.begin(), vstrings.end(),
		//	std::ostream_iterator<std::string>(std::cout, "\n"));
		mapOfOperators.insert(
				std::pair<std::string, std::vector<std::string> >(key,
						vstrings));
	}
	return mapOfOperators;
}

InferenceEngine::InferenceEngine(
		CollectionOfOperatorDefinitions* operatorCollection,
		std::string mappingFile, bool castOpsDef,
		std::string constraintsOutputFile, bool castOpBinOps) {
	this->constraintsOutput_ = constraintsOutputFile;
	collectionOfOperatorDefinitions_ = operatorCollection;
	inputMappingFile_.open(mappingFile.c_str(), std::ios::in);

	std::map<std::string, std::vector<std::string> > mapOfOperators =
			this->generateMapOfOperators();
	std::vector<OperatorUsagesWithNewDefs*> opUsagesWithDefs =
			OperatorUsagesWithNewDefs::constructArrayOfUsagesForCollection(
					operatorCollection, mapOfOperators);
	this->castOpPrimitiveDataTypes_ = castOpBinOps;

#if 1
	std::ofstream of("/home/alex/Desktop/test_INFERENCE.out", std::ios::out);
	for (std::vector<OperatorUsagesWithNewDefs*>::iterator it =
			opUsagesWithDefs.begin(); it != opUsagesWithDefs.end(); ++it) {
		of << *(*it);
	}
	of.close();
#endif
	this->castOps_ = castOpsDef;

	callSolver(opUsagesWithDefs);
}

void InferenceEngine::augmentHandlerUsageMapping(
		std::map<TypedLambdaExpression*, std::vector<TypedLambdaExpression*> > &handlerUsagesMappings,
		OperatorUsagesWithNewDefs* currentOp) {
	for (std::map<TypedLambdaExpression*, std::vector<TypedLambdaExpression*> >::iterator itMap =
			handlerUsagesMappings.begin(); itMap != handlerUsagesMappings.end();
			++itMap) {

		std::vector<TypedLambdaExpression*> listOfOtherIdenticalUsages;

		for (std::vector<TypedLambdaExpression*>::iterator itVector =
				(*itMap).second.begin(); itVector != (*itMap).second.end();
				++itVector) {
			/*
			 * Here we also have to add all the identical usages.
			 * These are all the typed lambda expressions, of the OldOperatorDefinition,
			 * for which the identicalUsage is the current one!
			 */
			std::vector<TypedLambdaExpression*> allUsages =
					currentOp->operatorUsages()->getArrayOfLambdaExpressions();
			for (std::vector<TypedLambdaExpression*>::iterator itForIdenticalUsages =
					allUsages.begin(); itForIdenticalUsages != allUsages.end();
					++itForIdenticalUsages) {
				if ((*itForIdenticalUsages)->getIdenticalHandledUsage()
						== (*itVector)) {
					listOfOtherIdenticalUsages.push_back(
							(*itForIdenticalUsages));
					/*
					 * Finally, add the model_true from the identical usage!!!
					 */
					(*itForIdenticalUsages)->model_true_ =
							(*itVector)->model_true_;
				}
			}
		}

		(*itMap).second.insert((*itMap).second.begin(),
				listOfOtherIdenticalUsages.begin(),
				listOfOtherIdenticalUsages.end());

		if (listOfOtherIdenticalUsages.size()) {
			for (std::vector<TypedLambdaExpression*>::iterator itTTT =
					(*itMap).second.begin(); itTTT != (*itMap).second.end();
					++itTTT) {
				//std::cout<<*(*itTTT)<<std::endl;
			}
			//std::cout<<*(listOfOtherIdenticalUsages[0])<<std::endl;
		}
	}
}

} /* namespace analyzer */
