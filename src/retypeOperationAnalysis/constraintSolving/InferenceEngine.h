/*
 * InferenceEngine.h
 *
 *  Created on: 7 Dec 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_INFERENCEENGINE_H_
#define SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_INFERENCEENGINE_H_

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

namespace analyzer {

class OldOperatorDefinition;
class CollectionOfOperatorDefinitions;
class OperatorUsagesWithNewDefs;
class TypedLambdaExpression;
class OperatorCallUniqueIdentifier;
class ConstraintError;
class OperatorReplacingMapping;

class InferenceEngine {
public:
	InferenceEngine();

	struct tokens: std::ctype<char> {
		tokens() :
				std::ctype<char>(get_table()) {
		}

		static const std::ctype_base::mask* get_table();
	};

	std::map<std::string, std::vector<std::string> > generateMapOfOperators();
	InferenceEngine(CollectionOfOperatorDefinitions* operatorCollection,
			std::string mappingFile, bool castOpsDef,
			std::string constraintOutputFile, bool castOpBinOps);

	static std::vector<std::string> returnOperatorsThatAcceptMappings(
			std::vector<OldOperatorDefinition*> usages);

	void callSolver(std::vector<OperatorUsagesWithNewDefs*> newOpsWithUsages);

	virtual ~InferenceEngine();

	void augmentHandlerUsageMapping(
			std::map<TypedLambdaExpression*, std::vector<TypedLambdaExpression*> > &handlerUsagesMappings,
			OperatorUsagesWithNewDefs* currentOp);

	std::vector<ConstraintError*> errorVector() {
		return this->errorVector_;
	}
	std::vector<OperatorReplacingMapping*> operatorReplacings() {
		return this->operatorReplacings_;
	}
private:
	CollectionOfOperatorDefinitions * collectionOfOperatorDefinitions_;
	std::ifstream inputMappingFile_;
	bool castOps_;
	std::string constraintsOutput_;

	std::vector<ConstraintError*> errorVector_;
	std::vector<OperatorReplacingMapping*> operatorReplacings_;
	bool castOpPrimitiveDataTypes_;

};

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_INFERENCEENGINE_H_ */
