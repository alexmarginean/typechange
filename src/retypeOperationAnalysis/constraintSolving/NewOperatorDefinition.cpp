/*
 * NewOperatorDefinition.cpp
 *
 *  Created on: 24 Mar 2015
 *      Author: alex
 */

#include "NewOperatorDefinition.h"
#include <TypedLambdaExpression.h>

namespace analyzer {

NewOperatorDefinition::NewOperatorDefinition(TypedLambdaExpression* callerExp,
		OperatorCallUniqueIdentifier * location) {
	this->lambdaExpression_ = callerExp;
	this->set_typesOfArgs(callerExp->typesOfArgs());
	this->failureMoreUsages_ = false;
	this->operatorLocation_ = location;
}

NewOperatorDefinition::NewOperatorDefinition(
		std::vector<std::vector<Types> > typesOfArgs,
		TypedLambdaExpression* currentExpression) :
		OperatorDefinition(typesOfArgs, NULL) {
	lambdaExpression_ = currentExpression;
	failureMoreUsages_ = false;
}

void NewOperatorDefinition::appendLambdaExpression(
		TypedLambdaExpression* lambdaExpr) {

	failureMoreUsages_ = true;
}

void NewOperatorDefinition::appendArrayOfLambdaExpressions(
		std::vector<TypedLambdaExpression*> lambdaExpr) {

	failureMoreUsages_ = true;
}

void NewOperatorDefinition::print(std::ostream& os) const {
	os << *lambdaExpression_;
}

} /* namespace analyzer */
