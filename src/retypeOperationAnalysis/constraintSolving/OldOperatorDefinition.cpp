/*
 * OldOperatorDefinition.cpp
 *
 *  Created on: 24 Mar 2015
 *      Author: alex
 */

#include "OldOperatorDefinition.h"
#include <TypedLambdaExpression.h>
#include <NewOperatorDefinition.h>
#include <HighOrderTypeOfTerm.h>

#include <boost/archive/tmpdir.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>

namespace analyzer {

OldOperatorDefinition::OldOperatorDefinition(
		std::vector<TypedLambdaExpression*> usages,
		TypedLambdaExpression* operatorDef, std::string operatorName) {
	this->operatorDef_ = operatorDef;
	for (std::vector<TypedLambdaExpression*>::iterator it = usages.begin();
			it != usages.end(); ++it) {
		this->callerExpressions_.push_back(*it);
	}
	this->operatorLocation_ = NULL;
	if (operatorDef) {
		this->set_typesOfArgs(operatorDef->typesOfArgs());
	}
	this->setOperatorName(operatorName);
}

OldOperatorDefinition::OldOperatorDefinition(TypedLambdaExpression * callerExp,
		OperatorCallUniqueIdentifier * location) {
	this->callerExpressions_.push_back(callerExp);
	this->operatorLocation_ = location;
	this->set_typesOfArgs(callerExp->typesOfArgs());
}

OldOperatorDefinition::OldOperatorDefinition(
		std::vector<std::vector<Types> > typesOfArgs,
		std::vector<TypedLambdaExpression*> callerExp) :
		OperatorDefinition(typesOfArgs, NULL) {
	this->callerExpressions_ = callerExp;
}

void OldOperatorDefinition::appendLambdaExpression(
		TypedLambdaExpression* lambdaExpr) {
	callerExpressions_.push_back(lambdaExpr);
}

void OldOperatorDefinition::appendArrayOfLambdaExpressions(
		std::vector<TypedLambdaExpression*> lambdaExpr) {
	for (std::vector<TypedLambdaExpression*>::iterator it = lambdaExpr.begin();
			it != lambdaExpr.end(); it++) {
		this->callerExpressions_.push_back((*it));
	}
}

void OldOperatorDefinition::print(std::ostream& os) const {
	os << "BEGIN USAGE:>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> \n";
	os << "INITIAL DEFINITION:\n";
	if (operatorDef_) {
		os << *operatorDef_ << std::endl << "USAGES:\n";
	} else {
		os << "EXTERNAL FUNCTION!" << std::endl << "USAGES:\n";

	}

	for (int i = 0; i < callerExpressions_.size(); i++) {
		os << *(callerExpressions_[i]) << " ";
	}
	if (this->operatorLocation_) {
		os << "\n Location: " << this->operatorLocation_->filename_ << " Line: "
				<< this->operatorLocation_->line_ << " Column: "
				<< this->operatorLocation_->column_ << "\n";
	} else {
		os << "\n NO LOCATION!!!!";
	}
	os << "END USAGE >>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;
}

struct operatorNameEqual: std::unary_function<TypedLambdaExpression*, bool> {
	operatorNameEqual(const std::string currentName) :
			currentName_(currentName) {
	}
	bool operator()(TypedLambdaExpression * arg) const {
		return arg ?
				arg->currentOperatorWithTypes()->getOperatorName()
						== currentName_ :
				false;
	}
	const std::string currentName_;
};

std::vector<OldOperatorDefinition*> OldOperatorDefinition::generateOperatorCallsFromLambdaExpressions(
		std::vector<TypedLambdaExpression*> arrayOfLambdaExpressions,
		std::vector<OperatorCallUniqueIdentifier*> definitionLocation,
		std::vector<TypedLambdaExpression*> arrayOfInitialDefinitions,
		std::vector<TypedLambdaExpression*> arrayOfOldUsages) {
	std::vector<OldOperatorDefinition*> arrayOfOpDefs;
	std::vector<TypedLambdaExpression*> tempArrayOfCalls;
	tempArrayOfCalls = arrayOfLambdaExpressions;

	/*
	 * First add the old definitions to each TypedLambdaExpression
	 * in arrayOfLambdaExpressions. These are in arrayOfOldUsage, in
	 * the same order as in the arrayOfLambdaExpressions!
	 */

	std::vector<TypedLambdaExpression*>::iterator itOldUsages =
			arrayOfOldUsages.begin();
	for (std::vector<TypedLambdaExpression*>::iterator itNewUsages =
			arrayOfLambdaExpressions.begin();
			itNewUsages != arrayOfLambdaExpressions.end(); ++itNewUsages) {

		(*itNewUsages)->setInitialTypeLambdaExpr(*itOldUsages);

		/*
		 * Annotate the current expression as not user defined!
		 */
		(*itOldUsages)->setIsUserDefinedOp(false);

		++itOldUsages;
	}

	for (std::vector<TypedLambdaExpression *>::iterator it =
			tempArrayOfCalls.begin(); it != tempArrayOfCalls.end(); it++) {

		if (*it) {
			std::vector<TypedLambdaExpression*>::iterator iter = it + 1;
			std::vector<TypedLambdaExpression*> currentCalls;
			currentCalls.push_back(*it);
			std::string currentOpName =
					(*it)->currentOperatorWithTypes()->getOperatorName();
			*it = NULL;
			while ((iter = std::find_if(tempArrayOfCalls.begin(),
					tempArrayOfCalls.end(), operatorNameEqual(currentOpName)))
					!= tempArrayOfCalls.end()) {
				currentCalls.push_back(*iter);
				(*iter) = NULL;
				iter++;
			}

			TypedLambdaExpression * currentFctDefinition;
			std::vector<TypedLambdaExpression*>::iterator iteratorCurrentFunction;
			if ((iteratorCurrentFunction = std::find_if(
					arrayOfInitialDefinitions.begin(),
					arrayOfInitialDefinitions.end(),
					operatorNameEqual(currentOpName)))
					!= arrayOfInitialDefinitions.end()) {
				currentFctDefinition = (*iteratorCurrentFunction);

			} else {
				currentFctDefinition = NULL;
			}

			arrayOfOpDefs.push_back(
					new OldOperatorDefinition(currentCalls,
							currentFctDefinition, currentOpName));

		}
	}
	return arrayOfOpDefs;
}

} /* namespace analyzer */
