/*
 * OperatorDefinition.cpp
 *
 *  Created on: 24 Mar 2015
 *      Author: alex
 */

#include "OperatorDefinition.h"

namespace analyzer {

OperatorDefinition::OperatorDefinition(OperatorDefinition& otherOpDef) {
	this->typesOfArgs_ = otherOpDef.typesOfArgs();
	this->operatorLocation_ = otherOpDef.operatorLocation_;
	this->operatorName_ = otherOpDef.operatorName_;
}

} /* namespace analyzer */
