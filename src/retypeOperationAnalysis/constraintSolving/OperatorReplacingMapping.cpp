/*
 * OperatorReplacingMapping2.cpp
 *
 *  Created on: 14 Dec 2015
 *      Author: alex
 */

#include "OperatorReplacingMapping.h"

#include "OperatorUsagesWithNewDefs.h"

#include <CollectionOfOperatorDefinitions.h>
#include <TypedLambdaExpression.h>
#include <NewOperatorDefinition.h>
#include <OldOperatorDefinition.h>
#include <TypeInferenceConstraintSolver.h>
#include <ConstraintError.h>

namespace analyzer {

std::ostream& operator <<(std::ostream& os,
		const OperatorReplacingMapping& exp) {
	os << std::endl << "INITIAL EXPRESSION:" << std::endl
			<< *(exp.initialExpression_) << "OPERATOR NAME: "
			<< exp.oldOperatorName_ << std::endl << "AT LOCATION: "
			<< *(exp.locationInInputProgram_) << std::endl << "NEW EXPRESSION:"
			<< std::endl << *(exp.newExpression_) << "OPERATOR NAME: "
			<< exp.newOperatorName_ << std::endl << "AT LOCATION: "
			<< *(exp.locationOfNewDefinition_) << std::endl
			<< "USER DEFINED OPERATOR: " << exp.userDefinedOppReplace_
			<< std::endl << "MODEL:::: " << std::endl;

	//if (exp.initialExpression_->model_true_.size()) {
	for (int i = 0; i < exp.initialExpression_->model_true_.size() - 1; i++) {

		if (exp.initialExpression_->model_true_[i] == T_o) {
			os << "ARG " << i << " = " << "T_o" << std::endl;

		} else if (exp.initialExpression_->model_true_[i] == T_n) {
			os << "ARG " << i << " = " << "T_n" << std::endl;
		} else if (exp.initialExpression_->model_true_[i] == T_n_c_y) {
			os << "ARG " << i << " = " << "T_n_c_n" << std::endl;
		} else if (exp.initialExpression_->model_true_[i] == T_u) {
			os << "ARG " << i << " = " << "T_u" << std::endl;
		} else if (exp.initialExpression_->model_true_[i] == T_n_c_x) {
			os << "ARG " << i << " = " << "T_n_c_o" << std::endl;
		} else {
			/*
			 * This should never be reached!
			 */
			assert(false);
		}

	}

	if (exp.initialExpression_->model_true_[exp.initialExpression_->model_true_.size()
			- 1] == T_o) {
		os << "RETURN:  " << " = " << "T_o" << std::endl;

	} else if (exp.initialExpression_->model_true_[exp.initialExpression_->model_true_.size()
			- 1] == T_n) {
		os << "RETURN " << " = " << "T_n" << std::endl;
	} else if (exp.initialExpression_->model_true_[exp.initialExpression_->model_true_.size()
			- 1] == T_n_c_y) {
		os << "RETURN " << " = " << "T_n_c_n" << std::endl;
	} else if (exp.initialExpression_->model_true_[exp.initialExpression_->model_true_.size()
			- 1] == T_u) {
		os << "RETURN " << " = " << "T_u" << std::endl;
	} else if (exp.initialExpression_->model_true_[exp.initialExpression_->model_true_.size()
			- 1] == T_n_c_x) {
		os << "RETURN " << " = " << "T_n_c_o" << std::endl;
	} else {
		/*
		 * This should never be reached!
		 */
		assert(false);
	}
	//}

	return os;
}

std::vector<OperatorReplacingMapping*> OperatorReplacingMapping::createOperatorReplacingMapping(
		std::map<TypedLambdaExpression*, std::vector<TypedLambdaExpression*> > handlerUsagesMappings) {
	std::vector<OperatorReplacingMapping*> returnValue;
	for (std::map<TypedLambdaExpression*, std::vector<TypedLambdaExpression*> >::iterator itMap =
			handlerUsagesMappings.begin(); itMap != handlerUsagesMappings.end();
			++itMap) {
		for (std::vector<TypedLambdaExpression*>::iterator itVector =
				(*itMap).second.begin(); itVector != (*itMap).second.end();
				++itVector) {
			returnValue.push_back(
					new OperatorReplacingMapping(*itVector, itMap->first,
							(*itVector)->uniqueCallIdentifier(),
							itMap->first->uniqueCallIdentifier(),
							(*itVector)->currentOperatorWithTypes()->getOperatorName(),
							itMap->first->currentOperatorWithTypes()->getOperatorName(),
							itMap->first->getIsUserDefinedOp()));

#if 0

			OperatorReplacingMapping * temp = new OperatorReplacingMapping(
					*itVector, itMap->first,
					(*itVector)->uniqueCallIdentifier(),
					itMap->first->uniqueCallIdentifier(),
					(*itVector)->currentOperatorWithTypes()->getOperatorName(),
					itMap->first->currentOperatorWithTypes()->getOperatorName(),
					itMap->first->getIsUserDefinedOp());

			std::cout << "BEGIN<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
					<< std::endl << *temp
					<< "END>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;

#endif
		}
	}
	return returnValue;
}

CollectionOfOperatorReplacement::CollectionOfOperatorReplacement(
		std::vector<OperatorReplacingMapping*> map,
		std::vector<ConstraintError*> errors) {
	this->replacements_ = map;
	this->errors_ = errors;
	if (this->errors_.size()) {
		this->successful_ = false;
	} else {
		this->successful_ = true;
	}
}

std::ostream& operator <<(std::ostream& os,
		const CollectionOfOperatorReplacement& exp) {
	std::vector<OperatorReplacingMapping*> opReplaceMap = exp.replacements_;
	std::vector<ConstraintError*> engineErrors = exp.errors_;
	if (!exp.successful_) {
		os
				<< "Errors in the retype operations! Please fix them, otherwise Retype cannot proceed. The errors are:"
				<< std::endl;
		for (std::vector<ConstraintError*>::iterator itError =
				engineErrors.begin(); itError != engineErrors.end();
				++itError) {
			os << *(*itError) << endl;
		}
	} else {
		os << "Retype successfully infered the new types! No error signaled."
				<< std::endl;
		os << "OPERATOR BEGIN >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n";
		for (std::vector<OperatorReplacingMapping*>::iterator it =
				opReplaceMap.begin(); it != opReplaceMap.end(); ++it) {
			os << *(*it) << std::endl;
		}
		os
				<< "OPERATOR END<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n";
	}
	return os;
}

} /* namespace analyzer */
