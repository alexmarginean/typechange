/*
 * OperatorReplacingMapping2.h
 *
 *  Created on: 14 Dec 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_OPERATORREPLACINGMAPPING_H_
#define SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_OPERATORREPLACINGMAPPING_H_

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <HighOrderTypeOfTerm.h>

namespace analyzer {

class OldOperatorDefinition;
class CollectionOfOperatorDefinitions;
class OperatorUsagesWithNewDefs;
class TypedLambdaExpression;
class OperatorCallUniqueIdentifier;
class ConstraintError;

class OperatorReplacingMapping {
public:
	OperatorReplacingMapping() {
		this->initialExpression_ = NULL;
		this->newExpression_ = NULL;
		this->locationInInputProgram_ = NULL;
		this->locationOfNewDefinition_ = NULL;
		this->oldOperatorName_ = "";
		this->newOperatorName_ = "";
		userDefinedOppReplace_ = true;
	}

	OperatorReplacingMapping(TypedLambdaExpression* initial,
			TypedLambdaExpression* newExp,
			OperatorCallUniqueIdentifier* locInput,
			OperatorCallUniqueIdentifier* locNewDef, std::string oldOp,
			std::string newOp, bool userDef = true) :
			initialExpression_(initial), newExpression_(newExp), locationInInputProgram_(
					locInput), locationOfNewDefinition_(locNewDef), oldOperatorName_(
					oldOp), newOperatorName_(newOp), userDefinedOppReplace_(
					userDef) {
	}

	static std::vector<OperatorReplacingMapping*> createOperatorReplacingMapping(
			std::map<TypedLambdaExpression*, std::vector<TypedLambdaExpression*> > handlerUsagesMappings);

	friend std::ostream& operator <<(std::ostream& os,
			const OperatorReplacingMapping& exp);

	OperatorCallUniqueIdentifier * locationInInputProgram() {
		return this->locationInInputProgram_;
	}
	OperatorCallUniqueIdentifier * locationOfNewDefinition() {
		return this->locationOfNewDefinition_;
	}

	std::string newOperatorName() {
		return this->newOperatorName_;
	}
	std::string oldOperatorName() {
		return this->oldOperatorName_;
	}

	bool isUserDefinedOppReplace() {
		return this->userDefinedOppReplace_;
	}

	TypedLambdaExpression* newExpression() {
		return this->newExpression_;
	}

public:
	TypedLambdaExpression* initialExpression_;
	TypedLambdaExpression* newExpression_;

	OperatorCallUniqueIdentifier * locationInInputProgram_;
	OperatorCallUniqueIdentifier * locationOfNewDefinition_;

	std::string oldOperatorName_;
	std::string newOperatorName_;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar & initialExpression_;
		ar & newExpression_;
		ar & locationInInputProgram_;
		ar & locationOfNewDefinition_;
		ar & oldOperatorName_;
		ar & newOperatorName_;
		ar & userDefinedOppReplace_;

	}

protected:

	bool userDefinedOppReplace_;

};

class CollectionOfOperatorReplacement {
public:
	CollectionOfOperatorReplacement(std::vector<OperatorReplacingMapping*> map,
			std::vector<ConstraintError*> errors);

	CollectionOfOperatorReplacement() {
		successful_ = false;
	}

	std::vector<OperatorReplacingMapping*> replacements();

	bool successful();
	std::vector<ConstraintError*> errors();

	friend std::ostream& operator <<(std::ostream& os,
			const CollectionOfOperatorReplacement& exp);

	static void save_operator_defs_remove_old_file(
			CollectionOfOperatorReplacement& s, const std::string filename) {
		// make an archive
		std::ofstream ofs(filename.c_str());
		boost::archive::binary_oarchive oa(ofs);
		oa << s;
	}

	static void restore_operator_defs(CollectionOfOperatorReplacement& s,
			const std::string filename) {
		// open the archive
		std::ifstream ifs(filename.c_str());
		boost::archive::binary_iarchive ia(ifs);
		ia >> s;
	}

protected:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar & replacements_;
		ar & successful_;
		ar & errors_;

	}

	std::vector<OperatorReplacingMapping*> replacements_;
	bool successful_;
	std::vector<ConstraintError*> errors_;

};

inline bool CollectionOfOperatorReplacement::successful() {
	return this->successful_;
}

inline std::vector<OperatorReplacingMapping*> CollectionOfOperatorReplacement::replacements() {
	return this->replacements_;
}

inline std::vector<ConstraintError*> CollectionOfOperatorReplacement::errors() {
	return this->errors_;
}

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_OPERATORREPLACINGMAPPING_H_ */
