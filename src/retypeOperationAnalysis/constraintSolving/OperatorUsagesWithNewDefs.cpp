/*
 * OperatorUsagesWithNewDefs1.cpp
 *
 *  Created on: 9 Dec 2015
 *      Author: alex
 */

#include "OperatorUsagesWithNewDefs.h"

namespace analyzer {

OperatorUsagesWithNewDefs::OperatorUsagesWithNewDefs(
		OldOperatorDefinition* usages) {
	this->operatorUsages_ = usages;
}

OperatorUsagesWithNewDefs::OperatorUsagesWithNewDefs(
		std::vector<NewOperatorDefinition*> newDefinitions,
		OldOperatorDefinition* usages) {
	for (std::vector<NewOperatorDefinition*>::iterator it =
			newDefinitions.begin(); it != newDefinitions.end(); ++it) {
		this->newDefinitions_.push_back(*it);
	}
	this->operatorUsages_ = usages;
}

OperatorUsagesWithNewDefs::operatorNameEqual2::operatorNameEqual2(
		const std::string currentName) :
		currentName_(currentName) {
}

bool OperatorUsagesWithNewDefs::operatorNameEqual2::operator ()(
		NewOperatorDefinition* arg) const {
	return arg ? arg->getOperatorName() == currentName_ : false;
}

std::vector<OperatorUsagesWithNewDefs*> OperatorUsagesWithNewDefs::constructArrayOfUsagesForCollection(
		CollectionOfOperatorDefinitions* allDefinitions,
		std::map<std::string, std::vector<std::string> > nameSpaceMapping) {
	std::vector<OldOperatorDefinition*> initialUsage =
			allDefinitions->listOfOpUsages();
	std::vector<NewOperatorDefinition*> newDefs =
			allDefinitions->listOfOpDefinitionsNew();
	std::vector<OperatorUsagesWithNewDefs*> listOfOpUsagesWithDefs;
	for (std::vector<OldOperatorDefinition*>::iterator it =
			initialUsage.begin(); it != initialUsage.end(); ++it) {

		std::map<std::string, std::vector<std::string> >::iterator itMap;

		if ((itMap = nameSpaceMapping.find((*it)->getOperatorName()))
				!= nameSpaceMapping.end()) {

			std::vector<NewOperatorDefinition*> newDefsForCurrentUsage;

			for (std::vector<std::string>::iterator itNewMappings =
					(*itMap).second.begin();
					itNewMappings != (*itMap).second.end(); ++itNewMappings) {
				std::vector<NewOperatorDefinition*>::iterator itNewOps;
				if ((itNewOps = std::find_if(newDefs.begin(), newDefs.end(),
						operatorNameEqual2(*itNewMappings))) != newDefs.end()) {
					newDefsForCurrentUsage.push_back(*itNewOps);

				} else {
					/*
					 * A mapping was required by the user, but its definition
					 * not provided!
					 */
					std::cout << "A definition specified in the mapping file is not provided!"<<std::endl;
					assert(false);
				}

			}
			OperatorUsagesWithNewDefs * newOperator =
					new OperatorUsagesWithNewDefs(newDefsForCurrentUsage, *it);
			newOperator->fixNumberOfArgsForSolver();
			listOfOpUsagesWithDefs.push_back(newOperator);

		} else {
			/*
			 * The current usages do not have a mapping provided by the user.
			 * Thus, we keep the original one in terms of names, and we do not
			 * add nothing here, as the new mapping!
			 */
			OperatorUsagesWithNewDefs * newOperator =
					new OperatorUsagesWithNewDefs(*it);
			newOperator->fixNumberOfArgsForSolver();
			listOfOpUsagesWithDefs.push_back(newOperator);
		}

	}
	return listOfOpUsagesWithDefs;
}

std::ostream& operator <<(std::ostream& os,
		const OperatorUsagesWithNewDefs& exp) {
	os << "Operators With New Definitions: >>>>>>>>>>>>>>>>>>>>\n" << "Usages:"
			<< std::endl;
	os << *(exp.operatorUsages_);

	os << std::endl << "Definitions::" << std::endl;
	std::vector<NewOperatorDefinition*> newOps = exp.newDefinitions_;
	if (!newOps.size()) {
		os << "No Definition Provided!" << std::endl;
	}
	for (std::vector<NewOperatorDefinition*>::iterator it = newOps.begin();
			it != newOps.end(); ++it) {
		os << *(*it);
	}
	os << std::endl << "END OPERATOR>>>>>>>>>>>>>>>>>>>>" << std::endl
			<< std::endl << std::endl << std::endl << std::endl;
	return os;
}

}
