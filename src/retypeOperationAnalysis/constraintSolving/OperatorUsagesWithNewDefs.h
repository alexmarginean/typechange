/*
 * OperatorUsagesWithNewDefs1.h
 *
 *  Created on: 9 Dec 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_OPERATORUSAGESWITHNEWDEFS_H_
#define SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_OPERATORUSAGESWITHNEWDEFS_H_

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <CollectionOfOperatorDefinitions.h>
#include <TypedLambdaExpression.h>
#include <NewOperatorDefinition.h>
#include <OldOperatorDefinition.h>

namespace analyzer {

class OperatorWithTypes;
class OperatorDefinition;
//class CollectionOfOperatorDefinitions;

/*! \brief Class that holds all the definitions and usages for an operator.
 *
 */
class OperatorUsagesWithNewDefs {
public:
	OperatorUsagesWithNewDefs();
	OperatorUsagesWithNewDefs(
			std::vector<NewOperatorDefinition*> newDefinitions,
			OldOperatorDefinition* usages);

	OperatorUsagesWithNewDefs(OldOperatorDefinition* usages);

	friend std::ostream& operator <<(std::ostream& os,
			const OperatorUsagesWithNewDefs& exp);

	struct operatorNameEqual2: std::unary_function<NewOperatorDefinition*, bool> {
		operatorNameEqual2(const std::string currentName);
		bool operator ()(NewOperatorDefinition* arg) const;
		const std::string currentName_;
	};

	static std::vector<OperatorUsagesWithNewDefs*> constructArrayOfUsagesForCollection(
			CollectionOfOperatorDefinitions* allDefinitions,
			std::map<std::string, std::vector<std::string> > nameSpaceMapping);

	int getNumberOfArgs() {
		return this->numberOfArgs_;
	}
	void setNumberOfArgs(int noArgs) {
		this->numberOfArgs_ = noArgs;
	}

	void fixNumberOfArgsForSolver() {
		int maxNoOfArgs = -1;
		// TODO: separate operators with different number of args! or add empty args at the end
		// and just consider the maximum number of arguments, as a new type that the operator
		// should have. Decide which way to take here!
		// NOTE: This is required only for OO languages! Otherwise, the number is the same
		// for all operators with the same name

		/*
		 * For now, we assume that the number of args is the same. For OO
		 * we have to add a new data type for missing args in the case
		 * of overloading with a lower number of operators than the one of the max args!
		 */

		std::vector<TypedLambdaExpression *> arrayOfExpressions =
				this->operatorUsages_->getArrayOfLambdaExpressions();

		for (std::vector<TypedLambdaExpression*>::iterator it =
				arrayOfExpressions.begin(); it != arrayOfExpressions.end();
				++it) {
			int x = ((*it)->typesOfArgs().size());
			if ( x > maxNoOfArgs) {
				maxNoOfArgs = (*it)->typesOfArgs().size();
			}
		}

		for (std::vector<TypedLambdaExpression*>::iterator it =
				arrayOfExpressions.begin(); it != arrayOfExpressions.end();
				++it) {
			int x = ((*it)->typesOfArgs().size());
			if (x != maxNoOfArgs) {
				/*
				 * Different number of arguments. For now, just assert false.
				 * For OO languages, we should add additional VOID parameters
				 * to all the ones lower then max, up to max.
				 * TODO: Fix for OO.
				 */
				assert(false);
			}
		}

		//TODO: Fix for OO for the new definitions, and the old ones!

		this->setNumberOfArgs(maxNoOfArgs);

	}

	std::vector<NewOperatorDefinition*> newDefinitions(){
		return this->newDefinitions_;
	}

	OldOperatorDefinition* operatorUsages(){
		return this->operatorUsages_;
	}

protected:
	std::vector<NewOperatorDefinition*> newDefinitions_;
	OldOperatorDefinition* operatorUsages_;
	int numberOfArgs_;
};

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_OPERATORUSAGESWITHNEWDEFS_H_ */
