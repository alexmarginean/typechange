/*
 * TypeInferenceConstraintSolver.cpp
 *
 *  Created on: 17 Mar 2015
 *      Author: alex
 */

#include <TypeInferenceConstraintSolver.h>
#include <cassert>

#include "UsageOfFunction.h"
#include "ConstraintError.h"
#include <SimpleTypeOfTerm.h>
#include <HighOrderTypeOfTerm.h>
#include <OperatorWithTypes.h>
#include <BinaryOperatorWithTypes.h>
#include <MultiArgsOperatorWithTypes.h>

#include <OperatorUsagesWithNewDefs.h>

namespace analyzer {

#if 0
void TypeInferenceConstraintSolver::test1() {
	// Overloading
	numberOfInputs_ = 7;
	std::vector<std::string> typeNamesString;
	typeNamesString.push_back("T_o");
	typeNamesString.push_back("T_n");
	typeNamesString.push_back("T_n_c");
	typeNamesString.push_back("T_u");
	createContextTypeAndEnumForSolver(typeNamesString, 1);
	std::vector<std::vector<Types> > allOperatorsConstraints;
	std::vector<Types> cArg1;
	cArg1.push_back(T_n);
	cArg1.push_back(T_n_c_y);
	std::vector<Types> cArg2;
	cArg2.push_back(T_o);
	std::vector<Types> cArg3;
	cArg3.push_back(T_n_c_y);
	std::vector<Types> cArg4;
	cArg4.push_back(T_n);
	std::vector<Types> cArg5;
	cArg5.push_back(T_n);
	std::vector<Types> cArg6;
	cArg6.push_back(T_u);
	std::vector<Types> cArg7;
	cArg7.push_back(T_u);
	std::vector<Types> cRet;
	cRet.push_back(T_n);
	allOperatorsConstraints.push_back(cArg1);
	allOperatorsConstraints.push_back(cArg2);
	allOperatorsConstraints.push_back(cArg3);
	allOperatorsConstraints.push_back(cArg4);
	allOperatorsConstraints.push_back(cArg5);
	allOperatorsConstraints.push_back(cArg6);
	allOperatorsConstraints.push_back(cArg7);
	allOperatorsConstraints.push_back(cRet);

	OldOperatorDefinition *myOp = new OldOperatorDefinition();
	//myOp->set_idOfDefinitions(1);
	myOp->set_typesOfArgs(allOperatorsConstraints);

	pushUsageInArrayOfUsages(myOp,
			this->arrayOfUsagesOfFunction_);
	cArg1.clear();
	cArg2.clear();
	cArg3.clear();
	cArg4.clear();
	cArg5.clear();
	cArg6.clear();
	cArg7.clear();
	cRet.clear();
	allOperatorsConstraints.clear();
	cArg1.push_back(T_n);
	cArg2.push_back(T_n);
	cArg3.push_back(T_n_c_y);
	cArg4.push_back(T_n);
	cArg5.push_back(T_n);
	cArg6.push_back(T_n);
	cArg7.push_back(T_u);
	cRet.push_back(T_n);
	allOperatorsConstraints.push_back(cArg1);
	allOperatorsConstraints.push_back(cArg2);
	allOperatorsConstraints.push_back(cArg3);
	allOperatorsConstraints.push_back(cArg4);
	allOperatorsConstraints.push_back(cArg5);
	allOperatorsConstraints.push_back(cArg6);
	allOperatorsConstraints.push_back(cArg7);
	allOperatorsConstraints.push_back(cRet);

	OldOperatorDefinition *myOp2 = new OldOperatorDefinition();
	//myOp->set_idOfDefinitions(1);
	myOp2->set_typesOfArgs(allOperatorsConstraints);

	pushUsageInArrayOfUsages(myOp2,
			this->arrayOfUsagesOfFunction_);
	solveConstraints();
	this->outputListOfInitialUsageOfFunctions(arrayOfUsagesOfFunction_);
}
#endif

TypeInferenceConstraintSolver::TypeInferenceConstraintSolver(
		OperatorUsagesWithNewDefs* currentOp, bool castOperatorProvided,
		std::ofstream * constraintsOutput, bool outputConstraints) {

	/*
	 * Set the output file for constraints, if existing!
	 */
	if (outputConstraints) {
		this->constraintsOutputFile_ = constraintsOutput;
		this->constraintsFileProvided = true;
	} else {
		this->constraintsFileProvided = false;
	}
	/*
	 * First set the number of arguments.
	 */
	this->numberOfInputs_ = currentOp->getNumberOfArgs();

	/*
	 * Initialize the solver!
	 */

	std::vector<std::string> typeNamesString;
	typeNamesString.push_back("T_o");
	typeNamesString.push_back("T_n");
	typeNamesString.push_back("T_n_c_n");
	typeNamesString.push_back("T_u");
	typeNamesString.push_back("T_n_c_o");

	/*
	 * NOTE: VERY IMPORTANT! The order in which we push the types here,
	 * should be exactly in the same way as the enum for types, used in the
	 * Type class. We index in the typeNameString array according to that enums.
	 * We should fix this! Currently the order is:
	 * T_o = 0, T_n = 1, T_n_c_y = 2, T_u = 3, T_n_c_x = 4
	 *
	 * For the purpose of constraint solver does not matter. However,
	 * the order should be respected for allowing the user to understand the
	 * outputs in the case of an error!
	 */

	createContextTypeAndEnumForSolver(typeNamesString, 1);

	/*
	 * Push the usages from lambda expressions into constraints!
	 */
	pushOperatorInArrayOfUsages(currentOp->operatorUsages(),
			this->arrayOfUsagesOfFunction_);

	std::vector<NewOperatorDefinition*> newOpDefs = currentOp->newDefinitions();

	for (std::vector<NewOperatorDefinition*>::iterator it = newOpDefs.begin();
			it != newOpDefs.end(); ++it) {
		pushOperatorDefInArrayOfDefs((*it), this->arrayOfUserProvidedFunctions_);
	}

	/*
	 * Finally, also push the initial definition, that would still
	 * be available in the input program! We can use casts for it.
	 */
	std::vector<TypedLambdaExpression *> initialUniqueUsages =
			returnUniqueInitialDefinitions(currentOp->operatorUsages());

	for (std::vector<TypedLambdaExpression *>::iterator itUniqueInit =
			initialUniqueUsages.begin();
			itUniqueInit != initialUniqueUsages.end(); ++itUniqueInit) {
		pushDefinitionInArrayOfDefs(*itUniqueInit,
				this->arrayOfUserProvidedFunctions_);
	}

	this->castOperatorProvided_ = castOperatorProvided;

	this->checkConsistencyBetweenUserProvidedOperatorsAndB(
			arrayOfUsagesOfFunction_, arrayOfUserProvidedFunctions_);

	solveCastOperators(arrayOfUsagesOfFunction_, arrayOfUserProvidedFunctions_);

	generateConstraintsForTheMatchedDefinitionWithUsages(
			arrayOfUsagesOfFunction_);

	if (this->constraintsFileProvided) {
		*this->constraintsOutputFile_
				<< "RESULTS OF CONSTRAINTS!!!!!!!!!!!!!!!!!>>>>>>>>>>>>>>>>>>>>>>>>>>>"
				<< endl;
	}

	// TODO: check if we need to call this method!
	// I don't think we need. This method basically checks the constraints for
	// all the usages. But we checked this previously, for each individual
	// usage in: generateConstraintsForTheMatchedDefinitionWithUsages
	solveConstraints();

	//solveConstraints();
	//this->outputListOfInitialUsageOfFunctions(arrayOfUsagesOfFunction_);

}

TypeInferenceConstraintSolver::TypeInferenceConstraintSolver() {
	// nothing for now
	// this shouldn't be called!
	assert(false);
}

z3::expr TypeInferenceConstraintSolver::get_expr(context &c,
		Z3_func_decl constant) //get expression from constant
		{
	func_decl temp = to_func_decl(c, constant);
	return (to_expr(c, Z3_mk_app(c, temp, 0, 0)));
}

z3::sort TypeInferenceConstraintSolver::createEnumTypes(context &c,
		char const *dtypename, int count,
		std::vector<std::string> typeNamesString, func_decl_vector& consts) {
	int i;
	char itemname[10];
	array<Z3_symbol> names(count);
	array<Z3_func_decl> _testers(count);
	array<Z3_func_decl> _consts(count);

	for (i = 0; i < count; i++) {
		sprintf(itemname, "%s", typeNamesString[i].c_str());
		names[i] = Z3_mk_string_symbol(c, itemname);
	}
	Z3_symbol enum_nm = Z3_mk_string_symbol(c, dtypename);
	Z3_sort _s = Z3_mk_enumeration_sort(c, enum_nm, count, names.ptr(),
			_consts.ptr(), _testers.ptr());
	for (i = 0; i < count; ++i) {
		consts.push_back(to_func_decl(c, _consts[i]));
	}
	z3::sort s = to_sort(c, _s);

	return (s);
}

void TypeInferenceConstraintSolver::createContextTypeAndEnumForSolver(
		std::vector<std::string> typeNamesString, int typeOfReturn) {

	//z3::context solverContext;

	//func_decl_vector localTypeNames(solverContext);
	typeNames_ = new func_decl_vector(solverContext_);

	//new (&*typeNames) z3::func_decl_vector(solverContext);

	//z3::sort localRetypeTypesEnum(solverContext);
	retypeTypesEnum_ = new z3::sort(solverContext_);

	//new (&*retypeTypesEnum) z3::sort(solverContext);

	(*retypeTypesEnum_) = createEnumTypes(solverContext_, "TYPE_ARG",
			typeNamesString.size(), typeNamesString, *typeNames_);

	//typeNames = &localTypeNames;
	//retypeTypesEnum = &localRetypeTypesEnum;

	//sort_vector domainArray(solverContext);
	domainArray_ = new sort_vector(solverContext_);
	for (int i = 0; i < numberOfInputs_; i++) {
		domainArray_->push_back(*retypeTypesEnum_);
	}

	// create the arguments, and add them into the array of args

	//arrayOfArguments = new expr_vector(solverContext);

	arrayOfConstraints_ = new expr_vector(solverContext_);
	solverRetype_ = new solver(solverContext_);
	functionDeclaration_ = new func_decl(solverContext_);

	(*functionDeclaration_) = solverContext_.function("operatorFunction",
			*domainArray_, *retypeTypesEnum_);
}

void TypeInferenceConstraintSolver::pushConstraint(Types type, int idOfArg) {
	expr currentAssertion =
			arrayOfUsagesOfFunction_[0]->createEqualityConstraintOnArgument(
					idOfArg, type);
	arrayOfConstraints_->push_back(currentAssertion);

}

void TypeInferenceConstraintSolver::pushReturnTypeConstraint(Types type) {

	//createEqualityConstraintOnReturn

	expr currentAssertion =
			arrayOfUsagesOfFunction_[0]->createEqualityConstraintOnReturn(type);
	arrayOfConstraints_->push_back(currentAssertion);

}

void TypeInferenceConstraintSolver::solveConstraints() {

	/*!
	 * First reset the solver for being sure that we do not have
	 * any older constraints.
	 */
	solverRetype_->reset();

	/*!
	 * Add in the solver all the constraints existing in the array of
	 * constraints
	 */
	for (int i = 0; i < (*arrayOfConstraints_).size(); i++) {
		solverRetype_->add((*arrayOfConstraints_)[i]);
	}

	int countSolutions = 0;

	/*!
	 * Check whether or not we have at least a solution.
	 * If not, just look at the reason for failure.
	 */
	if (solverRetype_->check() == unsat) {
		if (this->constraintsFileProvided) {
			*this->constraintsOutputFile_ << solverRetype_->assertions()
					<< endl;
		}
	} else {
		if (this->constraintsFileProvided) {
			*this->constraintsOutputFile_ << solverRetype_->get_model() << endl;
		}
	}

	/*
	 * Iterate on all models for counting the number of models
	 * that respect the constraints.
	 */

	// TODO: implement model counting here
	//while (solverRetype.check() == sat) {
	//cout << "Solution number: " << countSolutions << endl;
	//cout << solverRetype->get_model() << endl;
	//func_interp
	//func_decl model_fd = func_decl(solverContext,
	//                  Z3_get_decl_func_decl_parameter(solverContext, some_array_1_eval_func_decl, 0));
	//solverRetype->get_model().get
#if 0

	cout << solverRetype_->get_model().get_func_decl(0) << endl;
	cout << solverRetype_->get_model().get_func_interp(solverRetype_->get_model().get_func_decl(0)).entry(0).value() << endl;
	cout << solverRetype_->get_model().get_func_interp(solverRetype_->get_model().get_func_decl(0)).else_value() << endl;
	cout << solverRetype_->get_model().get_const_interp(solverRetype_->get_model().get_const_decl(7)) << endl;
#endif

	//cout << s.get_model().get_const_interp(s.get_model()[0]) << endl;
	//cout << s.get_model().get_func_interp(s.get_model()[1]) << endl;
	//s.add(
	//		xx
	//				!= s.get_model().get_const_interp(
	//						s.get_model()[0]) /*s.get_model()[0]*/);
	//countSolutions++;
	//}

}

z3::check_result TypeInferenceConstraintSolver::solveConstraintsForUsages() {

	/*!
	 * First reset the solver.
	 */
	solverRetype_->reset();

	/*!
	 * Add all the constraints from the array of constraints.
	 */
	for (int i = 0; i < (*arrayOfConstraints_).size(); i++) {
		solverRetype_->add((*arrayOfConstraints_)[i]);
	}

	/*!
	 * Return the results of the constraint check.
	 */
	return solverRetype_->check();
}

bool TypeInferenceConstraintSolver::identicalConstraintInConstraintList(
		TypedLambdaExpression * currentOpDef,
		std::vector<UsageOfFunction *> arrayOfUsagesOfFunction,
		TypedLambdaExpression ** identifiedIdenticalUsage) {

	std::vector<std::vector<Types> > typesForArgs = currentOpDef->typesOfArgs();

	for (int k = 0; k < arrayOfUsagesOfFunction.size(); k++) {
		if (this->constraintsFileProvided) {
			*this->constraintsOutputFile_ << "ENTRY IN UNIQUE: " << endl;
		}

		z3::expr usageConstraints =
				arrayOfUsagesOfFunction[k]->createAndConstraintsForAllArgsWithOrOnTheSameArg(
						currentOpDef);

		/*!
		 * First check if the old constraints are identical the same as the new ones.
		 * The new ones should imply the old ones, while the old ones should imply the new ones
		 * for this to be true.
		 */

		if (arrayOfUsagesOfFunction[k]->getCurrentConstraints() != 0) {
			solverRetype_->reset();
			/*!
			 * Check if the constraint is identical as the k'th one.
			 * Both implications means that it is identical.
			 */

			/*!
			 * This basically asserts that two expressions are identically. TODO: Explain this !!!
			 */
			solverRetype_->add(
					!(usageConstraints
							== *(arrayOfUsagesOfFunction[k]->getCurrentConstraints())));
			// TODO: CHECK HERE! Initially it was: arrayOfUsagesOfFunction_[k],
			// but I think that was a typo.

			if (solverRetype_->check() == unsat) {
				/*!
				 * Equal constraints, so we should stop here.
				 */
				*identifiedIdenticalUsage =
						arrayOfUsagesOfFunction[k]->operatorDef();
				return true;
			} else {
				/*!
				 * Nothing here. The constraints are not equal.
				 */
			}
		}
	}
	/*!
	 * If not any identical constraint identified, then just return false.
	 */
	return false;
}

void TypeInferenceConstraintSolver::pushOperatorInArrayOfUsages(
		OperatorDefinition * operatorDef,
		std::vector<UsageOfFunction*> &arrayOfUsages) {
	std::vector<TypedLambdaExpression *> arrayOfAllExpressions =
			operatorDef->getArrayOfLambdaExpressions();

	for (std::vector<TypedLambdaExpression*>::iterator it =
			arrayOfAllExpressions.begin(); it != arrayOfAllExpressions.end();
			++it) {
		pushUsageInArrayOfUsages(*it, arrayOfUsages);
	}
}



void TypeInferenceConstraintSolver::pushOperatorDefInArrayOfDefs(
		OperatorDefinition * operatorDef,
		std::vector<UsageOfFunction*> &arrayOfUsages) {
	std::vector<TypedLambdaExpression *> arrayOfAllExpressions =
			operatorDef->getArrayOfLambdaExpressions();

	for (std::vector<TypedLambdaExpression*>::iterator it =
			arrayOfAllExpressions.begin(); it != arrayOfAllExpressions.end();
			++it) {
		pushDefinitionInArrayOfDefs(*it, arrayOfUsages);
	}
}



void TypeInferenceConstraintSolver::pushDefinitionInArrayOfDefs(
		TypedLambdaExpression * operatorDef,
		std::vector<UsageOfFunction *> &arrayOfUsages) {

	std::vector<std::vector<Types> > typesForArgs = operatorDef->typesOfArgs();
	/*!
	 * Check whether or not we need a fresh operator declaration (aka overloading)
	 * And whether or not we have identified current constraints that match the current one
	 */
	bool needFreshVariables;
	int matchingConstraints = 0;

	int newSetId = arrayOfUsages.size();
	UsageOfFunction * newUsage = new UsageOfFunction(solverContext_,
			retypeTypesEnum_, typeNames_, functionDeclaration_, numberOfInputs_,
			newSetId);
	/*!
	 * Push the newly created usage to the array of usages.
	 */
	arrayOfUsages.push_back(newUsage);

	/*!
	 * Now add the constraints on the newly generated fresh usage
	 */
	z3::expr usageConstraints =
			newUsage->createAndConstraintsForAllArgsWithOrOnTheSameArg(
					operatorDef);
	arrayOfConstraints_->push_back(usageConstraints);
	/*!
	 * Update the list of constraints for the current usage.
	 */
	newUsage->setCurrentConstraints(usageConstraints);
	newUsage->set_operatorDef(operatorDef);
}

void TypeInferenceConstraintSolver::pushUsageInArrayOfUsages(
		TypedLambdaExpression * operatorDef,
		std::vector<UsageOfFunction *> &arrayOfUsages) {

	std::vector<std::vector<Types> > typesForArgs = operatorDef->typesOfArgs();
	/*!
	 * Check whether or not we need a fresh operator declaration (aka overloading)
	 * And whether or not we have identified current constraints that match the current one
	 */
	bool needFreshVariables;
	int matchingConstraints = 0;
	/*!
	 * The final list of constraints generated by the current usage.
	 */
	//z3::expr * finalUsageConstraints;
	/*!
	 * Pointer to the identified similar usage. This is required for updating the
	 * list of constraints for that usage.
	 */
	//UsageOfFunction * identifiedSimilarUsage;
	/*!
	 * Check for all the existing usageOfFunctions, check whether or not
	 * The constraints are compatible
	 */
	TypedLambdaExpression * identifiedIdenticalUsage;
	if (identicalConstraintInConstraintList(operatorDef, arrayOfUsages,
			&identifiedIdenticalUsage)) {
		/*!
		 * Nothing to do here. The constraint is already existing so no need for a new one.
		 *
		 * Here we should only add the lambda expression as another usage for the current one.
		 */
		operatorDef->setIdenticalHandledUsage(identifiedIdenticalUsage);
	} else {
		/*!
		 * We have identified a new usage, so just define a new usageOfFunction object
		 */
		/*!
		 * The new id of the new usage is the size of the
		 * array of arguments (next position)
		 */
		int newSetId = arrayOfUsages.size();
		UsageOfFunction * newUsage = new UsageOfFunction(solverContext_,
				retypeTypesEnum_, typeNames_, functionDeclaration_,
				numberOfInputs_, newSetId);
		/*!
		 * Push the newly created usage to the array of usages.
		 */
		arrayOfUsages.push_back(newUsage);

		/*!
		 * Now add the constraints on the newly generated fresh usage
		 */
		z3::expr usageConstraints =
				newUsage->createAndConstraintsForAllArgsWithOrOnTheSameArg(
						operatorDef);
		arrayOfConstraints_->push_back(usageConstraints);
		/*!
		 * Update the list of constraints for the current usage.
		 */
		newUsage->setCurrentConstraints(usageConstraints);
		newUsage->set_operatorDef(operatorDef);
		/*!
		 * No need to check for solvability. It is a fresh usage, so it is
		 * for sure solvable
		 */
	}

}

void TypeInferenceConstraintSolver::pushUsageForUserDefinedBasisAndMarkTheMatchedFunctions(
		TypedLambdaExpression * currentOpDef) {
	std::vector<std::vector<Types> > typesForArgs = currentOpDef->typesOfArgs();

	/*!
	 * Check whether or not we have at least an usage of the current operator.
	 * If not, we have to create a new one.
	 */
	if (this->arrayOfUsagesOfFunction_.size() == 0) {
		/*!
		 * Create a new usage for the current function.
		 */
		UsageOfFunction * newUsage = new UsageOfFunction(solverContext_,
				retypeTypesEnum_, typeNames_, functionDeclaration_,
				numberOfInputs_, 0);
		/*!
		 * Push the newly created usage to the array of usages.
		 */
		arrayOfUsagesOfFunction_.push_back(newUsage);
	}

	/*!
	 * Check whether or not we need a fresh operator declaration (aka overloading)
	 * And whether or not we have identified current constraints that match the current one
	 */
	bool needFreshVariables;
	int matchingConstraints = 0;

	/*!
	 * The final list of constraints generated by the current usage.
	 */
	z3::expr * finalUsageConstraints;
	/*!
	 * Pointer to the identified similar usage. This is required for updating the
	 * list of constraints for that usage.
	 */
	UsageOfFunction * identifiedSimilarUsage;
	/*!
	 * Check for all the existing usageOfFunctions, check whether or not
	 * The constraints are compatible
	 */
	for (int k = 0; k < arrayOfUsagesOfFunction_.size(); k++) {

		z3::expr usageConstraints =
				arrayOfUsagesOfFunction_[k]->createAndConstraintsForAllArgsWithOrOnTheSameArg(
						currentOpDef);

		//add the current constraint
		solverRetype_->add(usageConstraints);

		/*!
		 * Check if the current constraint is solvable, assuming all the previous constraints.
		 * If it is solvable, then just add it as the final usage constraint and increase
		 * the number of matched constraints.
		 */
		if (solveConstraintsForUsages() == sat) {
			finalUsageConstraints = &usageConstraints;
			identifiedSimilarUsage = arrayOfUsagesOfFunction_[k];
			// TODO: assert if the identified similar usage is the one provided
			// by the user. If not ERROR!
			matchingConstraints++;
		}
	}

	/*!
	 * If just one was matched, it means that both the constraints
	 * can be subsumed by one, matching both of them. Otherwise
	 * we can not make any assumptions about this, and we will add
	 * a new usage -> overload.
	 */
	if (matchingConstraints == 1) {
		arrayOfConstraints_->push_back(*finalUsageConstraints);
		/*!
		 * For the identified similar usage, update its list.
		 */
		expr * newConstraintForFunction = new expr(solverContext_);

		if (identifiedSimilarUsage->getCurrentConstraints()) {
			*newConstraintForFunction =
					(*(identifiedSimilarUsage->getCurrentConstraints()))
							&& (*finalUsageConstraints);
		} else {
			*newConstraintForFunction = (*finalUsageConstraints);
		}
		identifiedSimilarUsage->setCurrentConstraints(
				*newConstraintForFunction);
	} else if (matchingConstraints > 1) {
		/*!
		 * There is an error in the user defined functions!
		 * We can use an operator in more places! Here we should the
		 * mappings provided by the user.
		 */

	} else {
		/*!
		 * There is an error in the user defined functions!
		 * For an user provided operator, we haven't identified any
		 * usage. This is an unneeded operator, which may signal an error
		 * in the user definition. We should give a warning here!
		 */

		return;
		//TODO: FIX HERE!

		/*!
		 * The new id of the new usage is the size of the
		 * array of arguments (next position)
		 */
		int newSetId = arrayOfUsagesOfFunction_.size();
		UsageOfFunction * newUsage = new UsageOfFunction(solverContext_,
				retypeTypesEnum_, typeNames_, functionDeclaration_,
				numberOfInputs_, newSetId);
		/*!
		 * Push the newly created usage to the array of usages.
		 */
		arrayOfUsagesOfFunction_.push_back(newUsage);

		/*!
		 * Now add the constraints on the newly generated fresh usage
		 */
		z3::expr usageConstraints =
				newUsage->createAndConstraintsForAllArgsWithOrOnTheSameArg(
						currentOpDef);
		arrayOfConstraints_->push_back(usageConstraints);
		/*!
		 * Update the list of constraints for the curent usage.
		 */
		identifiedSimilarUsage->setCurrentConstraints(*finalUsageConstraints);
		/*!
		 * No need to check for solvability. It is a fresh usage, so it is
		 * for sure solvable
		 */

	}
}

std::vector<Types> arrayOfSympleTypesFromZ3Model(z3::model goodModel) {
	std::vector<Types> results(goodModel.num_consts());

	/*
	 * Transform the model into vector<SimpleTypeOfTerm>
	 */

	for (int i = 0; i < goodModel.num_consts(); i++) {

		std::stringstream ss;
		ss << goodModel.get_const_decl(i).name();

		std::string temp = ss.str();

		size_t last_index = temp.find_last_not_of("0123456789");
		std::string result = temp.substr(last_index + 1);

		int position = atoi(result.c_str());

		std::stringstream ssValue;
		ssValue << goodModel.get_const_interp(goodModel.get_const_decl(i));

		std::string typeString = ssValue.str();

		if (position) {
			if (typeString == "T_o") {
				results[position - 1] = T_o;

			} else if (typeString == "T_n") {
				results[position - 1] = T_n;
			} else if (typeString == "T_n_c_n") {
				results[position - 1] = T_n_c_y;
			} else if (typeString == "T_u") {
				results[position - 1] = T_u;
			} else if (typeString == "T_n_c_o") {
				results[position - 1] = T_n_c_x;
			} else {
				/*
				 * This should never be reached!
				 */
				assert(false);
			}
		} else {
			if (typeString == "T_o") {
				results[goodModel.num_consts() - 1] = T_o;

			} else if (typeString == "T_n") {
				results[goodModel.num_consts() - 1] = T_n;
			} else if (typeString == "T_n_c_n") {
				results[goodModel.num_consts() - 1] = T_n_c_y;
			} else if (typeString == "T_u") {
				results[goodModel.num_consts() - 1] = T_u;
			} else if (typeString == "T_n_c_o") {
				results[goodModel.num_consts() - 1] = T_n_c_x;
			} else {
				/*
				 * This should never be reached!
				 */
				assert(false);
			}
		}
	}
	return results;
}

bool TypeInferenceConstraintSolver::checkConsistencyBetweenUserProvidedOperatorsAndB(
		std::vector<UsageOfFunction*> initialBasis,
		std::vector<UsageOfFunction*> userDefinedBasis) {
	/*!
	 * Check that we have usages and user provided operator
	 * If not, we have to create a new one.
	 *
	 * TODO: Fix here. In general, user defined basis may be empty.
	 */
	assert(initialBasis.size() > 0 && userDefinedBasis.size() > 0);

	/*!
	 * Check for all usage of functions, what user provided operators coresponde
	 */
	for (int i = 0; i < initialBasis.size(); i++) {

		for (int j = 0; j < userDefinedBasis.size(); j++) {
			z3::expr constraintsFromInitialBasis =
					*(initialBasis[i]->getCurrentConstraints());
			/*!
			 * Here we constraint the arguments in the initial basis,
			 * on the types in the user provided operator. We need so
			 * a new constraint on the variables in the initial basis, with
			 * the values from the user provided operator
			 */
			z3::expr constraintsFromUserProvidedOps =
					initialBasis[i]->createAndConstraintsForAllArgsWithOrOnTheSameArg(
							userDefinedBasis[j]->operatorDef());

			solverRetype_->reset();

			if (this->constraintsFileProvided) {
				*this->constraintsOutputFile_
						<< "CONSTRAINTS FROM INITIAL BASIS: "
						<< constraintsFromInitialBasis
						<< "END CONSTRAINTS FROM INITIAL BASIS" << endl;
				*this->constraintsOutputFile_
						<< "CONSTRAINTS FROM USER PROVIDED BASIS: "
						<< constraintsFromUserProvidedOps
						<< "END CONSTRAINTS FROM USER PROVIDED BASIS" << endl;
			}

			solverRetype_->add(constraintsFromInitialBasis);
			solverRetype_->add(constraintsFromUserProvidedOps);

			if (solverRetype_->check() == sat) {
				/*!
				 * Constraints are satisfied, so the current provided operator
				 * is used for that particular usage.
				 */

				/*!
				 * First add the additional constraints of the defined operator,
				 * on the constraints from the initial Basis.
				 *
				 * Just generate a new constraint that subsumes both of them
				 * TODO: check here if it is requried to also update the array of types
				 * in the initial basis.
				 */

				expr * newConstraintForFunction = new expr(solverContext_);

				if (constraintsFromInitialBasis) {
					*newConstraintForFunction = constraintsFromInitialBasis
							&& constraintsFromUserProvidedOps;
				} else {
					*newConstraintForFunction = constraintsFromUserProvidedOps;
				}
				//newConstraintForFunction->simplify();
				initialBasis[i]->setCurrentConstraints(
						*newConstraintForFunction);

				/*!
				 * Update the initial basis with handled constraint to true,
				 * and add the current user defined operator as handler.
				 * Later we will assert that every element in the basis has only one handler.
				 */
				initialBasis[i]->set_handledConstraint(true);
				initialBasis[i]->pushHandler(userDefinedBasis[j]);

				/*
				 * Here also add the model that returned SAT.
				 */

				std::stringstream ss;

				z3::model successfulModel = solverRetype_->get_model();
				std::vector<Types> arrayOfFinalTypes =
						arrayOfSympleTypesFromZ3Model(successfulModel);

				// TODO: MODEL COUNTING HERE?

				//exit(-1);

				initialBasis[i]->model_true_ = ss.str();
				initialBasis[i]->operatorDef()->model_true_ = arrayOfFinalTypes;

			} else {
				/*!
				 * Constraints are not satisfied, so the current provided operator
				 * can't be used for that particular usage.
				 *
				 * Nothing to do here!
				 */
			}

		}
	}

	return true;
}

void TypeInferenceConstraintSolver::addCastsToNewTypeInUserProvidedOps(
		std::vector<UsageOfFunction*> userDefinedBasis) {

	//assert(this->castOperatorProvided());
	if (!this->castOperatorProvided()) {
		/*!
		 * If the cast operator was not provided, we have to
		 * return without adding it to the user defined basis.
		 */
		return;
	}

	for (int i = 0; i < userDefinedBasis.size(); i++) {

		/*!
		 * For every user defined constraint in the basis, substitute T_n_c for T_n!
		 *
		 */

		TypedLambdaExpression * oldOpDef = userDefinedBasis[i]->operatorDef();

		std::vector<std::vector<Types> > oldVectorOfTypes =
				userDefinedBasis[i]->operatorDef()->typesOfArgs();
		for (int ii = 0; ii < oldVectorOfTypes.size(); ii++) {
			bool currentArgumentContainsNewType = false;
			bool currentArgumentContainsOldType = false;
			for (int jj = 0; jj < oldVectorOfTypes[ii].size(); jj++) {
				if (oldVectorOfTypes[ii][jj] == T_n) {
					currentArgumentContainsNewType = true;
				}
				if (oldVectorOfTypes[ii][jj] == T_o) {
					currentArgumentContainsOldType = true;
				}
			}
			if (currentArgumentContainsNewType) {
				oldVectorOfTypes[ii].push_back(T_n_c_y);
			}
			if (currentArgumentContainsOldType) {
				oldVectorOfTypes[ii].push_back(T_n_c_x);
			}

		}

		oldOpDef->set_typesOfArgs(oldVectorOfTypes);

		z3::expr usageConstraints =
				userDefinedBasis[i]->createAndConstraintsForAllArgsWithOrOnTheSameArg(
						oldOpDef);
		/*!
		 * Update the list of constraints for the current usage.
		 */
		expr testExpr(solverContext_);
		userDefinedBasis[i]->setCurrentConstraints(usageConstraints);

		/*!
		 * We also need to update the vector of types, because oldVectorOfTypes
		 * is just a local copy.
		 */
		userDefinedBasis[i]->set_operatorDef(oldOpDef);

	}
}

void TypeInferenceConstraintSolver::solveCastOperators(
		std::vector<UsageOfFunction*> initialBasis,
		std::vector<UsageOfFunction*> userDefinedBasis) {

	/*!
	 * If the cast operators are not provided,
	 * then there is no need to do anything here.
	 * Just return.
	 */

	if (!this->castOperatorProvided()) {
		return;
	}

	/*!
	 * Extract from the initialBasis just the constraints where
	 * we haven't yet solved them.
	 */

	std::vector<UsageOfFunction*> unsolvedInitialBasis;

	for (std::vector<UsageOfFunction*>::iterator it = initialBasis.begin();
			it != initialBasis.end(); it++) {
		if (!(*it)->handledConstraint()) {
			unsolvedInitialBasis.push_back((*it));
		}
	}

	/*
	 * If all operators are already solved without casts,
	 * then just return!
	 */
	if (!unsolvedInitialBasis.size()) {
		return;
	}

	addCastsToNewTypeInUserProvidedOps(userDefinedBasis);

	/*!
	 * Check consistency between just the ones that are not already solved!!!
	 */

	this->checkConsistencyBetweenUserProvidedOperatorsAndB(unsolvedInitialBasis,
			userDefinedBasis);

	//addCastsToNewTypeInUserProvidedOps(userDefinedBasis);
	/*!
	 * Assert that the cast operators are provided. Otherwise
	 * this method should never be called
	 */

	return;
}

void TypeInferenceConstraintSolver::outputListOfInitialUsageOfFunctions(
		std::vector<UsageOfFunction *> arrayOfFunctions) {
	cout << endl;
	for (int i = 0; i < arrayOfFunctions.size(); i++) {
		cout << "Usage number: ";
		cout << i;
		cout << endl;
		cout << *(arrayOfFunctions[i]->getCurrentConstraints()) << endl << endl;
	}
}

bool TypeInferenceConstraintSolver::generateConstraintsForTheMatchedDefinitionWithUsages(
		std::vector<UsageOfFunction*> arrayOfBasis) {

	this->arrayOfConstraints_->resize(0);
	usagesDefinitionsMappings_.clear();

	bool successful = true;

	for (int i = 0; i < arrayOfBasis.size(); i++) {

		if (this->constraintsFileProvided) {
			*this->constraintsOutputFile_ << "Usage number: ";
			*this->constraintsOutputFile_ << i;
			*this->constraintsOutputFile_ << endl;
			*this->constraintsOutputFile_
					<< *(arrayOfBasis[i]->getCurrentConstraints()) << endl;
		}
		if (arrayOfBasis[i]->handledConstraint()) {
			if (this->constraintsFileProvided) {
				*this->constraintsOutputFile_ << "HANDLED CONSTRAINT: "
						<< arrayOfBasis[i]->handledConstraint() << endl;
				*this->constraintsOutputFile_ << "SIZE OF HANDLERS: "
						<< arrayOfBasis[i]->handlers().size() << endl;
			}
			if (arrayOfBasis[i]->handlers().size() > 1) {
				ConstraintError * currentError = new ConstraintError(
						arrayOfBasis[i], arrayOfBasis[i]->handlers(),
						this->arrayOfUserProvidedFunctions_);
				this->errorVector_.push_back(currentError);

				if (this->constraintsFileProvided) {
					*this->constraintsOutputFile_
							<< "ERROR: Handled by multiple user provided functions! : "
							<< endl;
				}
				successful = false;
				for (int j = 0; j < arrayOfBasis[i]->handlers().size(); j++) {
					if (this->constraintsFileProvided) {
						*this->constraintsOutputFile_ << "HANDLER NUMBER ";
						*this->constraintsOutputFile_ << j;
						*this->constraintsOutputFile_ << " :" << endl;
						*this->constraintsOutputFile_
								<< *(arrayOfBasis[i]->handlers()[j]->getCurrentConstraints())
								<< endl;
					}
				}
			} else {
				this->arrayOfConstraints_->push_back(
						*(arrayOfBasis[i]->getCurrentConstraints()));

				/*!
				 * An old operator definition is always handled by a new operator definition
				 * (if it is handled by someone)
				 */

				/*!
				 * First check if the new operator already exists in the map
				 *
				 */
				if (usagesDefinitionsMappings_.find(
						dynamic_cast<TypedLambdaExpression *>(arrayOfBasis[i]->handlers()[0]->operatorDef()))
						!= usagesDefinitionsMappings_.end()) {
					/*!
					 * If it exists, we push the currently handled initial definition, to the ones handled
					 * by the current user defined operator.
					 */
					usagesDefinitionsMappings_[dynamic_cast<TypedLambdaExpression *>(arrayOfBasis[i]->handlers()[0]->operatorDef())].push_back(
							dynamic_cast<TypedLambdaExpression *>(arrayOfBasis[i]->operatorDef()));
				} else {
					/*!
					 * Otherwise we just create a new pair key - value, and add it to the map.
					 */
					std::vector<TypedLambdaExpression*> newHandledOps;
					newHandledOps.push_back(
							dynamic_cast<TypedLambdaExpression *>(arrayOfBasis[i]->operatorDef()));

					usagesDefinitionsMappings_.insert(
							std::pair<TypedLambdaExpression*,
									std::vector<TypedLambdaExpression*> >(
									dynamic_cast<TypedLambdaExpression *>(arrayOfBasis[i]->handlers()[0]->operatorDef()),
									newHandledOps));
				}
				if (this->constraintsFileProvided) {
					*this->constraintsOutputFile_ << "CORRECT! Handled by:"
							<< endl;
					*this->constraintsOutputFile_
							<< *(arrayOfBasis[i]->handlers()[0]->getCurrentConstraints())
							<< endl;
				}
			}
		} else {
			successful = false;
			if (this->constraintsFileProvided) {
				*this->constraintsOutputFile_ << "ERROR: NOT handled! " << endl;
			}
			ConstraintError * currentError = new ConstraintError(
					arrayOfBasis[i], arrayOfBasis[i]->handlers(),
					this->arrayOfUserProvidedFunctions_);
			this->errorVector_.push_back(currentError);

		}
	}

	if (!successful) {
		arrayOfConstraints_->resize(0);
		usagesDefinitionsMappings_.clear();
	}

	return successful;
}

void TypeInferenceConstraintSolver::test2() {
#if 0
	numberOfInputs_ = 1;
	std::vector<std::string> typeNamesString;
	typeNamesString.push_back("T_o");
	typeNamesString.push_back("T_n");
	typeNamesString.push_back("T_n_c");
	typeNamesString.push_back("T_u");
	createContextTypeAndEnumForSolver(typeNamesString, 1);
	std::vector<std::vector<Types> > allOperatorsConstraints;
	std::vector<Types> cArg1;
	cArg1.push_back(T_n);
	//cArg1.push_back(T_o);
	std::vector<Types> cRet;
	cRet.push_back(T_n);
	allOperatorsConstraints.push_back(cArg1);
	allOperatorsConstraints.push_back(cRet);
	pushUsageInArrayOfUsages(allOperatorsConstraints);
	cArg1.clear();
	cRet.clear();
	allOperatorsConstraints.clear();
	cArg1.push_back(T_n);
	cRet.push_back(T_n);
	allOperatorsConstraints.push_back(cArg1);
	allOperatorsConstraints.push_back(cRet);
	pushUsageInArrayOfUsages(allOperatorsConstraints);

	solveConstraints();
#endif

#if 1
	//exclusive disjunction

	/*!
	 * Create the definition of the operator.
	 */
	numberOfInputs_ = 7;
	std::vector<std::string> typeNamesString;
	typeNamesString.push_back("T_o");
	typeNamesString.push_back("T_n");
	typeNamesString.push_back("T_n_c");
	typeNamesString.push_back("T_u");
	createContextTypeAndEnumForSolver(typeNamesString, 1);

	/*!
	 * Original basis inferred from the usage of the program: B
	 */
	std::vector<std::vector<Types> > allOperatorsConstraints;
	std::vector<Types> cArg1;
	cArg1.push_back(T_o);
	cArg1.push_back(T_n_c_y);
	std::vector<Types> cArg2;
	cArg2.push_back(T_n);
	std::vector<Types> cArg3;
	cArg3.push_back(T_n);
	std::vector<Types> cArg4;
	cArg4.push_back(T_n);
	std::vector<Types> cArg5;
	cArg5.push_back(T_n);
	std::vector<Types> cArg6;
	cArg6.push_back(T_n);
	std::vector<Types> cArg7;
	cArg7.push_back(T_u);
	std::vector<Types> cRet;
	cRet.push_back(T_n);
	allOperatorsConstraints.push_back(cArg1);
	allOperatorsConstraints.push_back(cArg2);
	allOperatorsConstraints.push_back(cArg3);
	allOperatorsConstraints.push_back(cArg4);
	allOperatorsConstraints.push_back(cArg5);
	allOperatorsConstraints.push_back(cArg6);
	allOperatorsConstraints.push_back(cArg7);
	//allOperatorsConstraints.push_back(cRet);

	std::vector<TypedLambdaExpression *> arrayUsages1; // = {100,101};
	arrayUsages1.push_back(new TypedLambdaExpression(100));
	arrayUsages1.push_back(new TypedLambdaExpression(101));
	TypedLambdaExpression *myOp3 = new TypedLambdaExpression(
			allOperatorsConstraints, cRet, 21);

	//myOp->set_idOfDefinitions(1);
	//myOp3->set_typesOfArgs(allOperatorsConstraints);
	pushUsageInArrayOfUsages(myOp3, this->arrayOfUsagesOfFunction_);
	cArg1.clear();
	cArg2.clear();
	cArg3.clear();
	cArg4.clear();
	cArg5.clear();
	cArg6.clear();
	cArg7.clear();
	cRet.clear();
	allOperatorsConstraints.clear();
	cArg1.push_back(T_n);
	cArg2.push_back(T_n);
	cArg3.push_back(T_n);
	cArg4.push_back(T_n);
	cArg5.push_back(T_n);
	cArg6.push_back(T_n);
	cArg7.push_back(T_u);
	cRet.push_back(T_n);
	allOperatorsConstraints.push_back(cArg1);
	allOperatorsConstraints.push_back(cArg2);
	allOperatorsConstraints.push_back(cArg3);
	allOperatorsConstraints.push_back(cArg4);
	allOperatorsConstraints.push_back(cArg5);
	allOperatorsConstraints.push_back(cArg6);
	allOperatorsConstraints.push_back(cArg7);
	//allOperatorsConstraints.push_back(cRet);

	std::vector<TypedLambdaExpression *> arrayUsages2; // = {200,201};
	arrayUsages2.push_back(new TypedLambdaExpression(200));
	arrayUsages2.push_back(new TypedLambdaExpression(201));
	TypedLambdaExpression *myOp4 = new TypedLambdaExpression(
			allOperatorsConstraints, cRet, 321);
	//myOp->set_idOfDefinitions(1);
	//myOp4->set_typesOfArgs(allOperatorsConstraints);
	pushUsageInArrayOfUsages(myOp4, this->arrayOfUsagesOfFunction_);

	cArg1.clear();
	cArg2.clear();
	cArg3.clear();
	cArg4.clear();
	cArg5.clear();
	cArg6.clear();
	cArg7.clear();
	cRet.clear();
	allOperatorsConstraints.clear();
	cArg1.push_back(T_n);
	cArg2.push_back(T_n);
	cArg3.push_back(T_n);
	cArg4.push_back(T_n);
	cArg5.push_back(T_n);
	cArg6.push_back(T_n);
	cArg7.push_back(T_u);
	cRet.push_back(T_n);
	allOperatorsConstraints.push_back(cArg1);
	allOperatorsConstraints.push_back(cArg2);
	allOperatorsConstraints.push_back(cArg3);
	allOperatorsConstraints.push_back(cArg4);
	allOperatorsConstraints.push_back(cArg5);
	allOperatorsConstraints.push_back(cArg6);
	allOperatorsConstraints.push_back(cArg7);
	//allOperatorsConstraints.push_back(cRet);

	std::vector<TypedLambdaExpression *> arrayUsages3; // = {300,301};
	arrayUsages3.push_back(new TypedLambdaExpression(300));
	arrayUsages3.push_back(new TypedLambdaExpression(301));

	TypedLambdaExpression *myOp5 = new TypedLambdaExpression(
			allOperatorsConstraints, cRet, 397);
	//myOp->set_idOfDefinitions(1);
	//myOp5->set_typesOfArgs(allOperatorsConstraints);

	pushUsageInArrayOfUsages(myOp5, this->arrayOfUsagesOfFunction_);

	/*!
	 * New basis inferred from the operators provided by the user: B'
	 */

	/*!
	 * If the cast operator is defined, automatically add T_n_c for
	 * the T_n arguments, after an initial parse just with T_n
	 * for avoiding more possibilities to retype!!!
	 * Later, simplify after the first step and then we will avoid this possibility.
	 *
	 */

	cArg1.clear();
	cArg2.clear();
	cArg3.clear();
	cArg4.clear();
	cArg5.clear();
	cArg6.clear();
	cArg7.clear();
	cRet.clear();
	allOperatorsConstraints.clear();
	cArg1.push_back(T_n);
	cArg2.push_back(T_n);
	cArg3.push_back(T_n);
	cArg4.push_back(T_n);
	cArg5.push_back(T_n);
	cArg6.push_back(T_n);
	cArg7.push_back(T_u);
	cRet.push_back(T_n);
	allOperatorsConstraints.push_back(cArg1);
	allOperatorsConstraints.push_back(cArg2);
	allOperatorsConstraints.push_back(cArg3);
	allOperatorsConstraints.push_back(cArg4);
	allOperatorsConstraints.push_back(cArg5);
	allOperatorsConstraints.push_back(cArg6);
	allOperatorsConstraints.push_back(cArg7);
	//allOperatorsConstraints.push_back(cRet);

	TypedLambdaExpression *myOp6 = new TypedLambdaExpression(
			allOperatorsConstraints, cRet, 497);
	//myOp->set_idOfDefinitions(1);
	//myOp6->set_typesOfArgs(allOperatorsConstraints);
	pushUsageInArrayOfUsages(myOp6, arrayOfUserProvidedFunctions_);

	cArg1.clear();
	cArg2.clear();
	cArg3.clear();
	cArg4.clear();
	cArg5.clear();
	cArg6.clear();
	cArg7.clear();
	cRet.clear();
	allOperatorsConstraints.clear();
	cArg1.push_back(T_o);
	cArg2.push_back(T_n);
	cArg3.push_back(T_n);
	cArg4.push_back(T_n);
	cArg5.push_back(T_n);
	cArg6.push_back(T_n);
	cArg7.push_back(T_u);
	cRet.push_back(T_n);
	allOperatorsConstraints.push_back(cArg1);
	allOperatorsConstraints.push_back(cArg2);
	allOperatorsConstraints.push_back(cArg3);
	allOperatorsConstraints.push_back(cArg4);
	allOperatorsConstraints.push_back(cArg5);
	allOperatorsConstraints.push_back(cArg6);
	allOperatorsConstraints.push_back(cArg7);
	//allOperatorsConstraints.push_back(cRet);

#if 0
	pushUsageInArrayOfUsages(allOperatorsConstraints,
			arrayOfUserProvidedFunctions_);
#endif

	this->set_castOperatorProvided(true);

	/*!
	 * Now check inference system!
	 */

	//solveConstraints();
	//cout << "INITIAL LIST OF FUNCTION USAGE:" << endl;
	//this->outputListOfInitialUsageOfFunctions(arrayOfUsagesOfFunction_);
	//cout << "LIST OF USER DEFINED FUNCTION:" << endl;
	//this->outputListOfInitialUsageOfFunctions(arrayOfUserProvidedFunctions_);
	this->checkConsistencyBetweenUserProvidedOperatorsAndB(
			arrayOfUsagesOfFunction_, arrayOfUserProvidedFunctions_);

	solveCastOperators(arrayOfUsagesOfFunction_, arrayOfUserProvidedFunctions_);

	generateConstraintsForTheMatchedDefinitionWithUsages(
			arrayOfUsagesOfFunction_);

	cout << "RESULTS OF CONSTRAINTS!!!!!!!!!!!!!!!!!>>>>>>>>>>>>>>>>>>>>>>>>>>>"
			<< endl;

	solveConstraints();

	/*!
	 * Output the mapping.
	 *
	 */

	cout << endl << endl;

	for (std::map<TypedLambdaExpression *, std::vector<TypedLambdaExpression *> >::iterator it =
			usagesDefinitionsMappings_.begin();
			it != usagesDefinitionsMappings_.end(); it++) {
		cout << *((*it).first) << " handles: ";

		for (std::vector<TypedLambdaExpression *>::iterator defsIt =
				it->second.begin(); defsIt != it->second.end(); defsIt++) {
			cout << (*(*defsIt));

		}

		cout << endl;
	}

#endif
}

void TypeInferenceConstraintSolver::testHarness() {
	//test1();
	test2();

}

// useful examples

/**
 \brief Prove <tt>x = y implies g(x) = g(y)</tt>, and
 disprove <tt>x = y implies g(g(x)) = g(y)</tt>.
 This function demonstrates how to create uninterpreted types and
 functions.
 */
void prove_example1() {
	std::cout << "prove_example1\n";
	context c;
	expr x = c.int_const("x");
	expr y = c.int_const("y");
	z3::sort I = c.int_sort();
	func_decl g = function("g", I, I);
	solver s(c);
	expr conjecture1 = implies(x == y, g(x) == g(y));
	std::cout << "conjecture 1\n" << conjecture1 << "\n";
	s.add(!conjecture1);
	if (s.check() == unsat)
		std::cout << "proved" << "\n";
	else
		std::cout << "failed to prove" << "\n";
	s.reset(); // remove all assertions from solver s
	expr conjecture2 = implies(x == y, g(g(x)) == g(y));
	std::cout << "conjecture 2\n" << conjecture2 << "\n";
	s.add(!conjecture2);
	if (s.check() == unsat) {
		std::cout << "proved" << "\n";
	} else {
		std::cout << "failed to prove" << "\n";
		model m = s.get_model();
		std::cout << "counterexample:\n" << m << "\n";
		std::cout << "g(g(x)) = " << m.eval(g(g(x))) << "\n";
		std::cout << "g(y) = " << m.eval(g(y)) << "\n";
	}
}

void demorgan() {
	std::cout << "de-Morgan example\n";
	context c;
	expr x = c.bool_const("x");
	expr y = c.bool_const("y");
	expr conjecture = !(x && y) == (!x || !y);
	solver s(c);
// adding the negation of the conjecture as a constraint.
	s.add(!conjecture);
	std::cout << s << "\n";
	switch (s.check()) {
	case unsat:
		std::cout << "de-Morgan is valid\n";
		break;
	case sat:
		std::cout << "de-Morgan is not valid\n";
		break;
	case unknown:
		std::cout << "unknown\n";
		break;
	}
}

void find_model_example1() {
	std::cout << "find_model_example1\n";
	context c;
	expr x = c.int_const("x");
	expr y = c.int_const("y");
	solver s(c);
	s.add(x >= 1);
	s.add(y < x + 3);
	std::cout << s.check() << "\n";
	model m = s.get_model();
	std::cout << m << "\n";
// traversing the model
	for (unsigned i = 0; i < m.size(); i++) {
		func_decl v = m[i];
// this problem contains only constants
		assert(v.arity() == 0);
		std::cout << v.name() << " = " << m.get_const_interp(v) << "\n";
	}
// we can evaluate expressions in the model.
	std::cout << "x + y + 1 = " << m.eval(x + y + 1) << "\n";
}

expr get_expr(context &c, Z3_func_decl constant) //get expression from constant
		{
	func_decl temp = to_func_decl(c, constant);
	return (to_expr(c, Z3_mk_app(c, temp, 0, 0)));
}

z3::sort create_enum(context &c, char const *dtypename, int count,
		char const *prefix, func_decl_vector& consts) {
	int i;
	char itemname[10];
	array<Z3_symbol> names(count);
	array<Z3_func_decl> _testers(count);
	array<Z3_func_decl> _consts(count);

	for (i = 0; i < count; i++) {
		sprintf(itemname, "%s%d", prefix, i);
		names[i] = Z3_mk_string_symbol(c, itemname);
	}
	Z3_symbol enum_nm = Z3_mk_string_symbol(c, dtypename);
	Z3_sort _s = Z3_mk_enumeration_sort(c, enum_nm, count, names.ptr(),
			_consts.ptr(), _testers.ptr());
	for (i = 0; i < count; ++i) {
		consts.push_back(to_func_decl(c, _consts[i]));
	}
	z3::sort s = to_sort(c, _s);

	return (s);
}

z3::sort createEnumTypes(context &c, char const *dtypename, int count,
		std::vector<std::string> typeNamesString, func_decl_vector& consts) {
	int i;
	char itemname[10];
	array<Z3_symbol> names(count);
	array<Z3_func_decl> _testers(count);
	array<Z3_func_decl> _consts(count);

	for (i = 0; i < count; i++) {
		sprintf(itemname, "%s%d", typeNamesString[i].c_str(), i);
		names[i] = Z3_mk_string_symbol(c, itemname);
	}
	Z3_symbol enum_nm = Z3_mk_string_symbol(c, dtypename);
	Z3_sort _s = Z3_mk_enumeration_sort(c, enum_nm, count, names.ptr(),
			_consts.ptr(), _testers.ptr());
	for (i = 0; i < count; ++i) {
		consts.push_back(to_func_decl(c, _consts[i]));
	}
	z3::sort s = to_sort(c, _s);

	return (s);
}

void find_model_example3() {

	context c;

	int count = 2, i;
	char itemname[10], prefix[] = { "T_" }, dtypename[] = { "phynodetype" };
	func_decl_vector typeNames(c);
//z3::sort typeEnum = create_enum(c, "TYPE_ARG", count, "T_", typeNames);

	char const* abc[2] = { "T_0", "T_1" };
	func_decl_vector ts(c);

	z3::sort typeEnum = create_enum(c, "TYPE_ARG", count, "T_", typeNames);

	sort_vector domainArray(c);
	domainArray.push_back(typeEnum);
	domainArray.push_back(typeEnum);
	domainArray.push_back(typeEnum);
	domainArray.push_back(typeEnum);
	domainArray.push_back(typeEnum);

#if 0
// another way to alloc a vector of z3::sorts
	z3::sort * domainArray_ = static_cast<z3::sort*>( ::operator new ( sizeof(z3::sort) * 20 ) );

	for ( size_t i = 0; i < 5; i++ )
	new (&domainArray_[i]) z3::sort(c);

	for ( size_t i = 0; i < 5; i++ )
	domainArray_[i] = typeEnum;

#endif

	func_decl g = c.function("g", domainArray, typeEnum);
	solver s(c);

	cout << g << endl;

	expr xx = c.constant("xx", typeEnum);

	expr arg1 = c.constant("x1", typeEnum);
	expr arg2 = c.constant("y1", typeEnum);
	expr arg3 = c.constant("z1", typeEnum);
	expr arg4 = c.constant("t1", typeEnum);
	expr arg5 = c.constant("u1", typeEnum);

	expr arg1sec = c.constant("x2", typeEnum);
	expr arg2sec = c.constant("y2", typeEnum);
	expr arg3sec = c.constant("z2", typeEnum);
	expr arg4sec = c.constant("t2", typeEnum);
	expr arg5sec = c.constant("u2", typeEnum);

	expr_vector argsExpr(c);
	argsExpr.push_back(arg1);
	argsExpr.push_back(arg2);
	argsExpr.push_back(arg3);
	argsExpr.push_back(arg4);
	argsExpr.push_back(arg5);

	expr_vector argsExprSec(c);
	argsExprSec.push_back(arg1sec);
	argsExprSec.push_back(arg2sec);
	argsExprSec.push_back(arg3sec);
	argsExprSec.push_back(arg4sec);
	argsExprSec.push_back(arg5sec);

	expr yy = g(argsExpr);

//expr tst3 = xx == c.constant(c.str_symbol("MYF"), typeEnum);

//expr typeO = ;

	expr tst1 = xx == yy;
	expr tst2 = arg1 == get_expr(c, typeNames[0]);
//tst2 = tst2 || (arg1 == get_expr(c, typeNames[1]) );
//tst2 = tst2 && (arg1 == get_expr(c, typeNames[1]) );
	expr tstF = get_expr(c, typeNames[1]) == g(argsExpr);
//	expr tst3 = arg5 == typeNames[1]();
//expr tst3 = arg1 == arg2;
//expr tst3 = exists(yy, tst2);

//expr testMASA = arg1 == arg1sec;

	expr testPula = arg1 == arg1sec;

	expr tst1sec = arg1sec == get_expr(c, typeNames[1]);
	expr tstFsec = get_expr(c, typeNames[1]) == g(argsExprSec);

//s.add(tst1);
//s.add(implies(tst2, yy.is_app()));
	s.add(tst2);
	s.add(tstF);

	s.add(tst1sec);
	s.add(tstFsec);
//s.add(testMASA);

//s.add(testPula);

//s.add(tst3);

	int countSolutions = 0;

	cout << s.check() << endl;
	cout << s.unsat_core() << endl;

//expr_vector myUnsat = s.unsat_core();
//for(int i = 0; i < s.unsat_core().size(); i++){
//	cout << s.unsat_core()[i] << endl;
//}

//expr b = s.get_model().eval(tst1, true);
// std::cout << b << "\n";

//while (s.check() == sat) {
	cout << "Solution number: " << countSolutions << endl;
	cout << s.get_model() << endl;
//cout << s.get_model().get_const_interp(s.get_model()[0]) << endl;
//cout << s.get_model().get_func_interp(s.get_model()[1]) << endl;
//s.add(
//		xx
//				!= s.get_model().get_const_interp(
//						s.get_model()[0]) /*s.get_model()[0]*/);
//countSolutions++;
//}

}

void find_model_example2() {
	context c;

	int count = 2, i;
	char itemname[10], prefix[] = { "T_" }, dtypename[] = { "phynodetype" };
	func_decl_vector typeNames(c);
	z3::sort typeEnum = create_enum(c, "TYPE_ARG", count, "T_", typeNames);
	func_decl g = function("g", typeEnum, typeEnum);
	solver s(c);

//expr tst3 = s.
//expr xx = get_expr(c, phynodeconsts[0]);
	expr xx = c.constant("xx", typeEnum);

	expr yy = g(get_expr(c, typeNames[1]));

//expr tst3 = xx == c.constant(c.str_symbol("MYF"), typeEnum);

//expr typeO = ;

	expr tst3 = xx == typeNames[0]();
	expr tst4 = xx == typeNames[1]();
	expr tst5 = tst3 || tst4;

//expr tst1 = xx == yy;
	expr tst1 = xx == yy;

//expr tst1 = ( get_expr(c, typeNames[0]) == g(get_expr(c, typeNames[1])) ) && (xx == typeNames[0]());
	expr tst2 = get_expr(c, typeNames[1]) == g(get_expr(c, typeNames[0]));
//cout << tst1 << endl << tst2 << endl;
	s.add(tst1);
	s.add(tst2);

//s.add(tst5);
//s.add(xx);
//s.add(tst2);
//s.add(tst2);
//s.add(implies(a,b>=5));
//s.add(b<5);
	cout << s.check() << endl;
//model modd;

	int countSolutions = 0;
	while (s.check() == sat) {
		cout << "Solution number: " << countSolutions << endl;
		cout << s.get_model() << endl;
		//cout << s.get_model().get_const_interp(s.get_model()[0]);
		s.add(
				xx
						!= s.get_model().get_const_interp(
								s.get_model()[0]) /*s.get_model()[0]*/);
		countSolutions++;
	}
}

/*expr generateConstraintFromOperatorUsage(OperatorDefinition op,
 func_decl_vector typeNames) {

 return NULL;
 }*/

void enum_sort_example() {
	std::cout << "enumeration sort example\n";
	context ctx;
	const char * enum_names[] = { "a", "b", "c" };
	func_decl_vector enum_consts(ctx);
	func_decl_vector enum_testers(ctx);
	z3::sort s = ctx.enumeration_sort("enumT", 3, enum_names, enum_consts,
			enum_testers);
// enum_consts[0] is a func_decl of arity 0.
// we convert it to an expression using the operator()
	expr a = enum_consts[0]();
	expr b = enum_consts[1]();
	expr x = ctx.constant("x", s);
	expr test = (x == a) && (x == b);
	std::cout << "1: " << test << std::endl;
	tactic qe(ctx, "ctx-solver-simplify");
	goal g(ctx);
	g.add(test);
	expr res(ctx);
	apply_result result_of_elimination = qe.apply(g);
	goal result_goal = result_of_elimination[0];
	std::cout << "2: " << result_goal.as_expr() << std::endl;
}

std::vector<TypedLambdaExpression*> TypeInferenceConstraintSolver::returnUniqueInitialDefinitions(
		OldOperatorDefinition* def) {
	std::vector<TypedLambdaExpression*> initialUniqueDefs;

	/*
	 * We take the definitions here. This is right,
	 * since for overloadings the types are arrays.
	 */
	if (def->getOperatorDef()) {
		/*
		 * We have a retyped definition. Add constraints to it.
		 */
		initialUniqueDefs.push_back(def->getOperatorDef());

	} else {
		/*
		 * We do not have a definition for this operator!
		 * So, add constraints to its usages that are not retypeable!
		 */

		std::vector<TypedLambdaExpression*> initialDefs;
		std::vector<TypedLambdaExpression*> arrayOfAllExpressions =
				def->getArrayOfLambdaExpressions();
		for (std::vector<TypedLambdaExpression*>::iterator it =
				arrayOfAllExpressions.begin();
				it != arrayOfAllExpressions.end(); ++it) {
			initialDefs.push_back((*it)->getInitialTypeLambdaExpr());
		}
		std::vector<UsageOfFunction*> arrayOfUsages;
		for (std::vector<TypedLambdaExpression*>::iterator it =
				initialDefs.begin(); it != initialDefs.end(); ++it) {
			std::vector<std::vector<Types> > typesForArgs =
					(*it)->typesOfArgs();
			/*!
			 * Check whether or not we need a fresh operator declaration (aka overloading)
			 * And whether or not we have identified current constraints that match the current one
			 */
			bool needFreshVariables;
			int matchingConstraints = 0;
			TypedLambdaExpression* identifiedIdenticalUsage;
			if (identicalConstraintInConstraintList(*it, arrayOfUsages,
					&identifiedIdenticalUsage)) {
				/*
				 * Do nothing, since it alread exists!
				 */
			} else {
				/*!
				 * We have identified a new usage, so just define a new usageOfFunction object
				 */
				/*!
				 * The new id of the new usage is the size of the
				 * array of arguments (next position)
				 */
				int newSetId = arrayOfUsages.size();
				UsageOfFunction* newUsage = new UsageOfFunction(solverContext_,
						retypeTypesEnum_, typeNames_, functionDeclaration_,
						numberOfInputs_, newSetId);
				/*!
				 * Push the newly created usage to the array of usages.
				 */
				arrayOfUsages.push_back(newUsage);
				/*!
				 * Now add the constraints on the newly generated fresh usage
				 */
				z3::expr usageConstraints =
						newUsage->createAndConstraintsForAllArgsWithOrOnTheSameArg(
								(*it));
				newUsage->setCurrentConstraints(usageConstraints);
				newUsage->set_operatorDef((*it));
				/*
				 * Append the current old usage at the array of unique once!
				 */
				initialUniqueDefs.push_back(*it);
			}
		}
	}

	return initialUniqueDefs;
}

} /* namespace analyzer */
