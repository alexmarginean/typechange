/*
 * Constraint Inference Engine
 */

#include <rose.h>
#include <boost/filesystem.hpp>
#include <RetypeParameterParser.h>
#include <RetypeOperationAnalyzer.h>
#include <OldOperatorDefinition.h>
#include <z3++.h>
#include <TypedLambdaExpression.h>
#include <NewOperatorDefinition.h>
#include <OldOperatorDefinition.h>
#include <HighOrderTypeOfTerm.h>
#include <CollectionOfOperatorDefinitions.h>
#include <TypeInferenceConstraintSolver.h>

#include <InferenceEngine.h>

#include "ConstraintError.h"

#include <OperatorReplacingMapping.h>

#include <sstream>

using namespace std;
using namespace z3;

using namespace retyper;
using namespace analyzer;

int main(int argc, char * argv[]) {
	/*
	 * The arguments are: the constructed bases file, as
	 * CollectionOfOperatorDefinitions.
	 */

	std::string filename;
	std::string mappingFileName;
	std::string constraintsOutputFile;
	bool castOpExists;
	std::string outputFileSerializedSolutions;
	std::string outputFileErrorsName;
	bool castOpExistsForBinaryOps;
	std::string typeY;

	RetypeParameterParser parametersParser;

	Rose_STL_Container<string> l = parametersParser.parseRetypeParametersInferenceEngine(argc, argv,&filename,
			&mappingFileName, &constraintsOutputFile, &outputFileSerializedSolutions, &castOpExists, &outputFileErrorsName, &typeY);

	bool fileExists = boost::filesystem::exists(filename) ? true : false;

	castOpExistsForBinaryOps = (typeY == "int") || (typeY == "float")
			|| (typeY == "double") || (typeY == "char") || (typeY == "short")
			|| (typeY == "long");

	if (fileExists) {

		CollectionOfOperatorDefinitions restoredCol;
		CollectionOfOperatorDefinitions::restore_operator_defs(restoredCol,
				filename);

#if 0
		std::vector<std::string> operatorsWithMappings =
		InferenceEngine::returnOperatorsThatAcceptMappings(
				restoredCol.listOfOpUsages());

		std::cout << std::endl << "Operators that accept mappings:"
		<< std::endl;
		for (std::vector<std::string>::iterator it =
				operatorsWithMappings.begin();
				it != operatorsWithMappings.end(); ++it) {
			std::cout << (*it) << " ";
		}
		std::cout << std::endl;
#endif

		InferenceEngine * myEngine = new InferenceEngine(&restoredCol,
				mappingFileName, castOpExists, constraintsOutputFile, castOpExistsForBinaryOps);

		std::vector<OperatorReplacingMapping*> opReplaceMap =
				myEngine->operatorReplacings();
		std::vector<ConstraintError*> engineErrors = myEngine->errorVector();

		CollectionOfOperatorReplacement * colectionOfReplacements =
				new CollectionOfOperatorReplacement(opReplaceMap, engineErrors);

		/*
		 * Now, output the CollectionOfOperatorReplacement, into the binary file, for
		 * RETYPER.
		 */

		CollectionOfOperatorReplacement::save_operator_defs_remove_old_file(
				*colectionOfReplacements, outputFileSerializedSolutions);

		/*
		 * Now check for errors, before go further!
		 */
		if (engineErrors.size()) {
			/*
			 * We have error! Output them, and then return an error code.
			 */
			std::cout << "ERRORS IN THE NEW DATA TYPE DEFINITIONS!"
					<< std::endl;

			std::ofstream outputFileErrors;
			bool errorFileExists = false;
			if (outputFileErrorsName == "") {

			} else {
				outputFileErrors.open(outputFileErrorsName.c_str(), ios::out);
				errorFileExists = true;
			}

			for (std::vector<ConstraintError*>::iterator itErrors =
					engineErrors.begin(); itErrors != engineErrors.end();
					++itErrors) {
				if (errorFileExists) {
					outputFileErrors << *(*itErrors) << std::endl;
				} else {
					std::cout << *(*itErrors) << std::endl;
				}
			}

			exit(767);
		}

#if 0
		// TEST SERIALIZE!

		std::ofstream file("outputInit.out", std::ios::out);

		file << std::endl << *colectionOfReplacements << std::endl;

		CollectionOfOperatorReplacement::save_operator_defs_remove_old_file(
				*colectionOfReplacements, "testColMapps.out");

		CollectionOfOperatorReplacement restoredColection;

		CollectionOfOperatorReplacement::restore_operator_defs(restoredColection, "testColMapps.out");

		std::ofstream file2("outputAfter.out", std::ios::out);

		file2 << std::endl << restoredColection << std::endl;
#endif

#if 1
		std::ofstream outfile("/home/alex/Desktop/OUT_FILE",
				ios::out /*| ios::app*/);
		outfile << (*colectionOfReplacements) << std::endl;
#endif
	}

	else {
		/*
		 * Error. The input file does not exist!
		 */
		assert(false);
	}
	std::cout << "Run Successful!" << std::endl;
	return 0;
}
