/*
 * BinaryOperatorWithTypes.h
 *
 *  Created on: 12 Jan 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_BINARYOPERATORWITHTYPES_H_
#define SRC_RETYPEOPERATIONANALYSIS_BINARYOPERATORWITHTYPES_H_

#include <OperatorWithTypes.h>

#include <string>

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>


namespace analyzer {

class SimpleTypeOfTerm;
class HighOrderTypeOfTerm;
class TypeOfTerm;


class BinaryOperatorWithTypes: public OperatorWithTypes {

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
        ar & boost::serialization::base_object<OperatorWithTypes>(*this);
		ar & this->id;

		ar.register_type(static_cast<SimpleTypeOfTerm *>(NULL));
		ar.register_type(static_cast<HighOrderTypeOfTerm *>(NULL));

		ar & this->lhs;
		ar & this->rhs;
		ar & this->theBinaryExpression;
	}

public:
	//BinaryOperatorWithTypes();
	BinaryOperatorWithTypes(std::string operatorName,
			TypeOfTerm* returnTypesOfOp, TypeOfTerm* lhs, TypeOfTerm* rhs,
			std::string theBinaryExpression, bool isNewOperator = true);
	BinaryOperatorWithTypes(){

	}
	bool operator==(BinaryOperatorWithTypes &op2);
	int getUniqueId();
	std::string toStringTypeof();
	std::string toString();
	//TypeOfTerm* returnTypesOfOperator;
	std::string getUniqueName();
	std::string toStringTypeofReturn();

	std::string returnOperatorExpression(){
		return this->theBinaryExpression;
	}

	std::vector<TypeOfTerm*> getListOfArguments();

private:
	int id;
	static int nextID;
	TypeOfTerm* lhs;
	TypeOfTerm* rhs;

	std::string theBinaryExpression;
};




} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_BINARYOPERATORWITHTYPES_H_ */
