/*
 * BinnaryReturnTypePersistentAttribute.h
 *
 *  Created on: 12 Jan 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_BINNARYRETURNTYPEPERSISTENTATTRIBUTE_H_
#define SRC_RETYPEOPERATIONANALYSIS_BINNARYRETURNTYPEPERSISTENTATTRIBUTE_H_

#include <rose.h>

namespace analyzer {

class OperatorWithTypes;

class BinnaryReturnTypePersistentAttribute: public AstAttribute {

public:
	std::vector<std::string> supportedReturnTypes;
	std::string theOtherOperator;
	bool hasAlternatives;

	OperatorWithTypes * childExpressionType;

	BinnaryReturnTypePersistentAttribute(
			std::vector<std::string> supportedReturnTypes,
			std::string theOtherOperator, bool hasAlternatives,
			OperatorWithTypes * childExpressionType);
};

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_BINNARYRETURNTYPEPERSISTENTATTRIBUTE_H_ */
