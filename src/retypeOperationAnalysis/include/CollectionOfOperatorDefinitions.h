/*
 * CollectionOfOperatorDefinitions.h
 *
 *  Created on: 26 Mar 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_COLLECTIONOFOPERATORDEFINITIONS_H_
#define SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_COLLECTIONOFOPERATORDEFINITIONS_H_

#include <ostream>
#include <istream>
#include <fstream>
#include <utils.h>
#include <vector>

#include <OperatorDefinition.h>

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>

namespace analyzer {

class OldOperatorDefinition;
class NewOperatorDefinition;
class OperatorDefinition;

class CollectionOfOperatorDefinitions {

public:

	std::vector<OldOperatorDefinition*> listOfOpUsages();

	std::vector<NewOperatorDefinition*> listOfOpDefinitionsNew();
	std::vector<NewOperatorDefinition*> listOfOpDefinitionsOld();

	/*! \Brief method for saving the operator definitions in an archive.
	 * This method just overwrites the context of the file if it exists.
	 *
	 */
	static void save_operator_defs(CollectionOfOperatorDefinitions& s,
			const std::string filename);
	/*! \Brief method for saving the operator definitions in an archive.
	 * This method just checks if file exists, read the collection of op from
	 * it if it exists, and append the new collection to the exist one before
	 * outputing the results in that files.
	 *
	 */
	static void safe_save_operator_defs_append(
			CollectionOfOperatorDefinitions& s, const std::string filename);

	static void restore_operator_defs(CollectionOfOperatorDefinitions& s,
			const std::string filename);

	friend std::ostream& operator <<(std::ostream& os,
			const CollectionOfOperatorDefinitions& exp);

	CollectionOfOperatorDefinitions();

	CollectionOfOperatorDefinitions(
			std::vector<OldOperatorDefinition*> listofUsages,
			std::vector<NewOperatorDefinition*> listOfOldDefs,
			std::vector<NewOperatorDefinition*> listOfNewDefs);

	void appendCollection(CollectionOfOperatorDefinitions newCollection);

private:

	std::vector<OldOperatorDefinition *> listOfOpUsages_;
	std::vector<NewOperatorDefinition *> listOfOpDefinitionsOld_;
	std::vector<NewOperatorDefinition *> listOfOpDefinitionsNew_;

	/*! \brief Required for serialization.
	 *
	 * Using boost::serializer for serializing the class.
	 */
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar.register_type(static_cast<OldOperatorDefinition *>(NULL));
		ar.register_type(static_cast<NewOperatorDefinition *>(NULL));
		ar & listOfOpUsages_;
		ar & listOfOpDefinitionsOld_;
		ar & listOfOpDefinitionsNew_;

	}
};

inline std::vector<OldOperatorDefinition*> CollectionOfOperatorDefinitions::listOfOpUsages() {
	return this->listOfOpUsages_;
}

inline std::vector<NewOperatorDefinition*> CollectionOfOperatorDefinitions::listOfOpDefinitionsNew() {
	return this->listOfOpDefinitionsNew_;
}

inline std::vector<NewOperatorDefinition*> CollectionOfOperatorDefinitions::listOfOpDefinitionsOld() {
	return this->listOfOpDefinitionsOld_;
}

inline void CollectionOfOperatorDefinitions::appendCollection(
		CollectionOfOperatorDefinitions newCollection) {

	std::vector<OldOperatorDefinition *> newListOfOpDefs =
			newCollection.listOfOpUsages();
	for (int i = 0; i < newListOfOpDefs.size(); i++) {
		this->listOfOpUsages_.push_back(newListOfOpDefs[i]);
	}

	for (int i = 0; i < newCollection.listOfOpDefinitionsNew().size(); i++) {
		this->listOfOpDefinitionsNew_.push_back(
				newCollection.listOfOpDefinitionsNew()[i]);
	}

	for (int i = 0; i < newCollection.listOfOpDefinitionsOld().size(); i++) {
		this->listOfOpDefinitionsOld_.push_back(
				newCollection.listOfOpDefinitionsOld()[i]);
	}
}

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_COLLECTIONOFOPERATORDEFINITIONS_H_ */
