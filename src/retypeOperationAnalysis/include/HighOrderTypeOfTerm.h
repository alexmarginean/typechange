/*
 * BinaryOperatorExpressionType.h
 *
 *  Created on: 12 Jan 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_HIGHORDERTYPEOFTERM_H_
#define SRC_RETYPEOPERATIONANALYSIS_HIGHORDERTYPEOFTERM_H_

#include <TypeOfTerm.h>

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>


namespace analyzer {


class OperatorWithTypes;
class BinaryOperatorWithTypes;
class MultiArgsOperatorWithTypes;

/*! \brief Class that represents an high order type of operator arguments.
 *
 *  Here are included all types of operators' arguments that are dependent on the return of another operator. So,
 *  for identifying the actual type, beta convertion is required. This class is used in the case of binnary operators.
 */
class HighOrderTypeOfTerm: public TypeOfTerm {

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version) {
		ar & boost::serialization::base_object<TypeOfTerm>(*this);

		ar.register_type(static_cast<BinaryOperatorWithTypes *>(NULL));
		ar.register_type(static_cast<MultiArgsOperatorWithTypes *>(NULL));

		ar & this->typeOf;
	}

public:
	HighOrderTypeOfTerm(){

	}
	/*! \brief Returns a string value, which is the name of the current type.
	 */
	std::string getTypeName();
	/*! \brief Constructor that takes as input the operator whose return value is used as current type.
	 */
	HighOrderTypeOfTerm(OperatorWithTypes * typeOf);
	/*! \brief Copy constructor
	 */
	HighOrderTypeOfTerm(HighOrderTypeOfTerm * newType);
	/*! \brief Destructor
	 */
	~HighOrderTypeOfTerm();
	/*! \brief This method returns the type of the term as string: ex: int.
	 */
	std::string toString();
	/*! \brief This method returns the type of the term for type inference algorithm: ex: T_0.
	 */
	std::string retypeTypesToString(std::string typeX, std::string typeY,
			std::string typeCast, std::string typeCastOld);

	/*! \brief This method returns the types of the current simple term (variable)
	 *  as an array of strings.
	 */
	std::vector<std::string> getTypeArrayOfStrings();

	/*! \brief Overloading of operator == for HighOrderTypeOfTerm.
	 */
	bool operator==(HighOrderTypeOfTerm t2);

	/*! \brief The type of current term, as another operator.
	 */
	OperatorWithTypes * typeOf;
};


} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_HIGHORDERTYPEOFTERM_H_ */
