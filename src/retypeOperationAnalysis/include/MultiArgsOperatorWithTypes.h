/*
 * MultiArgsOperatorWithTypes.h
 *
 *  Created on: Feb 13, 2015
 *      Author: root
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_MULTIARGSOPERATORWITHTYPES_H_
#define SRC_RETYPEOPERATIONANALYSIS_MULTIARGSOPERATORWITHTYPES_H_

#include <OperatorWithTypes.h>

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>
#include <boost/serialization/export.hpp>


namespace analyzer {

class TypeOfTerm;
class SimpleTypeOfTerm;
class HighOrderTypeOfTerm;

class MultiArgsOperatorWithTypes: public OperatorWithTypes {

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {

        ar & boost::serialization::base_object<OperatorWithTypes>(*this);
		ar & this->id;

		ar.register_type(static_cast<SimpleTypeOfTerm *>(NULL));
		ar.register_type(static_cast<HighOrderTypeOfTerm *>(NULL));

		ar & this->arguments;
		ar & this->theExpression;
	}

public:
	MultiArgsOperatorWithTypes(std::string operatorName,
			TypeOfTerm* returnTypesOfOp, std::vector<TypeOfTerm*> arguments,
			std::string theExpression);
	MultiArgsOperatorWithTypes(){

	}
	int getUniqueId();
	std::string toStringTypeof();
	std::string toString();
	//std::vector<TypeOfTerm*> returnTypesOfOperator;
	std::string getUniqueName();
	std::string toStringTypeofReturn() {
		return "NOT_IMPLEMENTED_YET";
	}

	std::string returnOperatorExpression(){
		return this->theExpression;
	}

	std::vector<TypeOfTerm*> getListOfArguments();

private:
	int id;
	static int nextID;
	std::vector<TypeOfTerm*> arguments;
	std::string theExpression;
};

inline std::vector<TypeOfTerm*> MultiArgsOperatorWithTypes::getListOfArguments() {
	return this->arguments;
}


} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_MULTIARGSOPERATORWITHTYPES_H_ */
