/*
 * NewOperatorDefinition.h
 *
 *  Created on: 24 Mar 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_NEWOPERATORDEFINITION_H_
#define SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_NEWOPERATORDEFINITION_H_

#include <ostream>
#include <utils.h>
#include <vector>

#include <TypedLambdaExpression.h>
#include <OperatorDefinition.h>

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>

namespace analyzer {

class NewOperatorDefinition: public OperatorDefinition {

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar & boost::serialization::base_object<OperatorDefinition>(*this);
		ar & this->lambdaExpression_;
		ar & this->failureMoreUsages_;
	}

public:
	NewOperatorDefinition(std::vector<std::vector<Types> > typesOfArgs,
			TypedLambdaExpression* currentExpression);
	NewOperatorDefinition() {
		this->failureMoreUsages_ = false;
		this->lambdaExpression_ = NULL;
	}
	NewOperatorDefinition(TypedLambdaExpression* callerExp,
			OperatorCallUniqueIdentifier * location);
	~NewOperatorDefinition() {

	}

	TypedLambdaExpression* lambdaExpression();

	void set_lambdaExpression(TypedLambdaExpression* expression);

	void appendLambdaExpression(TypedLambdaExpression* lambdaExpr);

	void appendArrayOfLambdaExpressions(
			std::vector<TypedLambdaExpression*> lambdaExpr);

	std::vector<TypedLambdaExpression*> getArrayOfLambdaExpressions();

	TypedLambdaExpression* getLambdaExpression();

	void print(std::ostream& os) const;

	static std::vector<NewOperatorDefinition *> generateOperatorDefsFromLambdaExpressions(
			std::vector<TypedLambdaExpression *> arrayOfLambdaExpressions,
			std::vector<OperatorCallUniqueIdentifier *> arrayOfLocations) {
		std::vector<NewOperatorDefinition *> arrayOfOpDefs;
		int i = 0;
		for (std::vector<TypedLambdaExpression *>::iterator it =
				arrayOfLambdaExpressions.begin();
				it != arrayOfLambdaExpressions.end(); it++) {
			arrayOfOpDefs.push_back(
					new NewOperatorDefinition(*it, arrayOfLocations[i]));
			i++;
		}
		return arrayOfOpDefs;
	}

	std::string getOperatorName() {
		return this->lambdaExpression_->currentOperatorWithTypes()->getOperatorName();
	}
private:
	TypedLambdaExpression * lambdaExpression_;
	bool failureMoreUsages_;

};

inline TypedLambdaExpression* NewOperatorDefinition::lambdaExpression() {
	return this->lambdaExpression_;
}

inline void NewOperatorDefinition::set_lambdaExpression(
		TypedLambdaExpression* expression) {
	this->lambdaExpression_ = expression;
}

inline std::vector<TypedLambdaExpression*> NewOperatorDefinition::getArrayOfLambdaExpressions() {
	/*
	 * This method returns the definition's lambda expression as an array.
	 * We need this in the solver, where we treat the definitions as the usages
	 * for constraints. Thus, we can use the base class!
	 */
	std::vector<TypedLambdaExpression*> returnValue;
	returnValue.push_back(this->lambdaExpression_);
	return returnValue;
}

inline TypedLambdaExpression* NewOperatorDefinition::getLambdaExpression() {
	return this->lambdaExpression_;
}

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_NEWOPERATORDEFINITION_H_ */
