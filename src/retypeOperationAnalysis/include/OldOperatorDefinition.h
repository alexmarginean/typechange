/*
 * OldOperatorDefinition.h
 *
 *  Created on: 24 Mar 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_OLDOPERATORDEFINITION_H_
#define SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_OLDOPERATORDEFINITION_H_

#include <ostream>
#include <istream>
#include <fstream>
#include <utils.h>
#include <vector>
#include <iostream>
#include <algorithm>

#include <OperatorDefinition.h>

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/assume_abstract.hpp>
#include <OperatorWithTypes.h>

namespace analyzer {

class NewOperatorDefinition;

class OldOperatorDefinition: public OperatorDefinition {

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar & boost::serialization::base_object<OperatorDefinition>(*this);
		ar & this->callerExpressions_;
		ar & this->operatorDef_;
	}

public:
	OldOperatorDefinition(std::vector<std::vector<Types> > typesOfArgs,
			std::vector<TypedLambdaExpression*> callerExp);
	OldOperatorDefinition(TypedLambdaExpression* callerExp,
			OperatorCallUniqueIdentifier * location);
	OldOperatorDefinition() {

	}
	~OldOperatorDefinition() {

	}

	std::vector<TypedLambdaExpression*> callerExpressions();

	void set_callerExpressions(std::vector<TypedLambdaExpression*> callerExp);

	void appendLambdaExpression(TypedLambdaExpression* lambdaExpr);

	void appendArrayOfLambdaExpressions(
			std::vector<TypedLambdaExpression*> lambdaExpr);

	std::vector<TypedLambdaExpression *> getArrayOfLambdaExpressions() {
		return this->callerExpressions_;
	}

	void print(std::ostream& os) const;
	friend std::ostream& operator <<(std::ostream& os,
			const OldOperatorDefinition& exp) {
		exp.print(os);
		return os;
	}

	TypedLambdaExpression * getLambdaExpression() {
		/*!
		 * This should never be called here. This class has an array of caller expressions.
		 */
		return NULL;
	}

	static std::vector<OperatorDefinition *> generateOperatorDefsFromLambdaExpressions(
			std::vector<TypedLambdaExpression *> arrayOfLambdaExpressions,
			std::vector<OperatorCallUniqueIdentifier *> arrayOfLocations) {
		std::vector<OperatorDefinition *> arrayOfOpDefs;
		int i = 0;
		for (std::vector<TypedLambdaExpression *>::iterator it =
				arrayOfLambdaExpressions.begin();
				it != arrayOfLambdaExpressions.end(); it++) {
			arrayOfOpDefs.push_back(
					new OldOperatorDefinition(*it, arrayOfLocations[i]));
			i++;
		}
		return arrayOfOpDefs;
	}

#if 1
	// TODO: FIX HERE

	OldOperatorDefinition(std::vector<TypedLambdaExpression*> usages,
			TypedLambdaExpression* operatorDef, std::string operatorName);

	/*
	 * Helper function for simulating lambdas, since we can't use std gnu++11,
	 * because of ROSE!
	 */

	static std::vector<OldOperatorDefinition*> generateOperatorCallsFromLambdaExpressions(
			std::vector<TypedLambdaExpression*> arrayOfLambdaExpressions,
			std::vector<OperatorCallUniqueIdentifier*> definitionLocation,
			std::vector<TypedLambdaExpression*> arrayOfInitialDefinitions,
			std::vector<TypedLambdaExpression*> arrayOfOldUsages);

	TypedLambdaExpression * getOperatorDef() {
		return this->operatorDef_;
	}
	std::vector<TypedLambdaExpression *> getOperatorUsage() {
		return this->callerExpressions_;
	}
#endif

private:
	std::vector<TypedLambdaExpression *> callerExpressions_;
	TypedLambdaExpression * operatorDef_;

};

inline std::vector<TypedLambdaExpression*> OldOperatorDefinition::callerExpressions() {
	return this->callerExpressions_;
}

inline void OldOperatorDefinition::set_callerExpressions(
		std::vector<TypedLambdaExpression*> callerExp) {
	this->callerExpressions_ = callerExp;
}

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_OLDOPERATORDEFINITION_H_ */
