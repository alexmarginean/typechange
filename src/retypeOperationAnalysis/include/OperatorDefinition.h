/*
 * OperatorDefinition.h
 *
 *  Created on: 24 Mar 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_OPERATORDEFINITION_H_
#define SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_OPERATORDEFINITION_H_

#include <ostream>
#include <utils.h>
#include <vector>

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>

namespace analyzer {

class TypedLambdaExpression;
class OperatorCallUniqueIdentifier;

class OperatorDefinition {

private:
	friend class boost::serialization::access;

	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar & this->typesOfArgs_;
		ar & this->operatorLocation_;
		ar & this->operatorName_;
	}
	/*! \brief This is for user defined arguments.
	 *
	 * No idOfDefinitions should be provided here since the user
	 * defines just one operator for every usage. Otherwise it is an error
	 */
public:
	OperatorDefinition(std::vector<std::vector<Types> > typesOfArgs,
			OperatorCallUniqueIdentifier * location) {
		typesOfArgs_ = typesOfArgs;
		operatorLocation_ = location;
		this->operatorName_ = "";
	}

	OperatorDefinition() {
		operatorLocation_ = NULL;
		this->operatorName_ = "";
	}

	OperatorDefinition(OperatorDefinition& otherOpDef);

	virtual ~OperatorDefinition() {

	}

	std::vector<std::vector<Types> > typesOfArgs();

	void set_typesOfArgs(std::vector<std::vector<Types> > args);

	virtual void appendLambdaExpression(TypedLambdaExpression * lambdaExpr) = 0;

	virtual void appendArrayOfLambdaExpressions(
			std::vector<TypedLambdaExpression*> lambdaExpr) = 0;

	virtual std::vector<TypedLambdaExpression *> getArrayOfLambdaExpressions() = 0;

	virtual TypedLambdaExpression * getLambdaExpression() = 0;

	virtual void print(std::ostream& os) const = 0;

	virtual std::string getOperatorName(){
		return this->operatorName_;
	}
	void setOperatorName(std::string opName){
		this->operatorName_ = opName;
	}

	friend std::ostream& operator<<(std::ostream& out,
			const OperatorDefinition& mc) {
		mc.print(out);
		return out;
	}

	/*
	 friend bool operator<(const OperatorDefinition & n1, const OperatorDefinition & n2 ) {
	 return false;   // for example
	 }*/

protected:
	std::vector<std::vector<Types> > typesOfArgs_;
	OperatorCallUniqueIdentifier * operatorLocation_;
	std::string operatorName_;

};

BOOST_SERIALIZATION_ASSUME_ABSTRACT (OperatorDefinition)

inline std::vector<std::vector<Types> > OperatorDefinition::typesOfArgs() {
	return this->typesOfArgs_;
}

inline void OperatorDefinition::set_typesOfArgs(
		std::vector<std::vector<Types> > args) {
	this->typesOfArgs_ = args;
}

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_CONSTRAINTSOLVING_OPERATORDEFINITION2_H_ */
