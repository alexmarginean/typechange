/*
 * OperatorWithTypes.h
 *
 *  Created on: Feb 13, 2015
 *      Author: root
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_OPERATORWITHTYPES_H_
#define SRC_RETYPEOPERATIONANALYSIS_OPERATORWITHTYPES_H_

#include <vector>
#include <string>

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>

namespace analyzer {

class TypeOfTerm;
class SimpleTypeOfTerm;
class HighOrderTypeOfTerm;

class OperatorWithTypes {

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar & this->typeX;
		ar & this->typeY;
		ar & this->typeCast;
		ar & this->typeCastOld;
		ar & this->operatorName;

		ar.register_type(static_cast<SimpleTypeOfTerm *>(NULL));
		ar.register_type(static_cast<HighOrderTypeOfTerm *>(NULL));

		ar & this->returnTypesOfOperator_;
	}

public:
	//BinaryOperatorWithTypes();

	virtual int getUniqueId() = 0;
	virtual std::string toStringTypeof() = 0;
	virtual std::string toString() = 0;
	virtual std::string getUniqueName() =0;
	virtual std::string toStringTypeofReturn() = 0;
	virtual std::vector<TypeOfTerm*> getListOfArguments() = 0;
	std::string getOperatorName() {
		return this->operatorName;
	}
	virtual ~OperatorWithTypes() {
	}
	virtual std::string returnOperatorExpression() = 0;
	void setTypeX(std::string typeX);
	void setTypeY(std::string typeY);
	void setTypeCast(std::string typeCast);
	void setTypeCastToOld(std::string typeCastOld);

	std::string getTypeX();
	std::string getTypeY();
	std::string getTypeCast();
	std::string getTypeCastOld();
	TypeOfTerm* returnTypesOfOperator();

protected:
	TypeOfTerm* returnTypesOfOperator_;
    std::string operatorName;

private:
	std::string typeX;
	std::string typeY;
	std::string typeCast;
	std::string typeCastOld;
};

inline void OperatorWithTypes::setTypeX(std::string typeX) {
	this->typeX = typeX;
}

inline void OperatorWithTypes::setTypeY(std::string typeY) {
	this->typeY = typeY;
}

inline void OperatorWithTypes::setTypeCast(std::string typeCast) {
	this->typeCast = typeCast;
}

inline void OperatorWithTypes::setTypeCastToOld(std::string typeCastOld) {
	this->typeCastOld = typeCastOld;
}

inline std::string OperatorWithTypes::getTypeX() {
	return this->typeX;
}

inline std::string OperatorWithTypes::getTypeY() {
	return this->typeY;
}

inline std::string OperatorWithTypes::getTypeCast() {
	return this->typeCast;
}

inline std::string OperatorWithTypes::getTypeCastOld() {
	return this->typeCastOld;
}

inline TypeOfTerm* OperatorWithTypes::returnTypesOfOperator() {
	return this->returnTypesOfOperator_;
}

BOOST_SERIALIZATION_ASSUME_ABSTRACT (OperatorWithTypes)

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_OPERATORWITHTYPES_H_ */
