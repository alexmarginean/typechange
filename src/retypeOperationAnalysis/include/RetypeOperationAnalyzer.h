/*
 * SymbolTableGenerator.h
 *
 *  Created on: 7 Jan 2015
 *      Author: alex
 */

#ifndef SRC_ST_SYMBOLTABLEGENERATOR_H_
#define SRC_ST_SYMBOLTABLEGENERATOR_H_

#include <rose.h>

namespace analyzer {

class CollectionOfOperatorDefinitions;
class OperatorCallUniqueIdentifier;
class TypedLambdaExpression;
class OperatorWithTypes;
class MultiArgsOperatorWithTypes;

class RetypeOperationAnalyzer: public AstSimpleProcessing {

public:
	RetypeOperationAnalyzer();
	RetypeOperationAnalyzer(std::ofstream *outfile, std::string stFile,
			std::string typeX, std::string typeY, bool isUserDefOps,
			std::string outputOpsDefsFile);
	virtual ~RetypeOperationAnalyzer();
	std::vector<SgNode*> returnStatementList();
	std::string getCurrentTypeOfVariable(SgType * varType);
	bool checkIfVarIsInRetypeScope(SgVarRefExp* varRef);
	/*! \brief This method is used for generating the list of operator with types
	 * in the case of the user defined operators.
	 *
	 * It passes just the list of function definition from the source program.
	 *
	 */
	void analyseReversedPreorderJustDefinitions();
	MultiArgsOperatorWithTypes * returnTypeofFctDef(
			SgFunctionDefinition * currentFunction);
	void analyseReversedPreorder();
	void outputOperatorList();
	void outputOperatorListJustInitial();
	void transformToConstraints();
	void solveConstraints();
	std::vector<TypedLambdaExpression *> getGeneratedLambdaExpressions(
			std::vector<OperatorWithTypes*> arrayOfOperatorsWithTypes);
	CollectionOfOperatorDefinitions* generateListOfIdentifiedOps(
			bool isUserDefinedOpsFile);
	MultiArgsOperatorWithTypes * returnTypeofFctDefForInputProgramDefs(
			SgFunctionDefinition * currentFunction);
	void setTypesForTypeOfTerms();

	std::vector<std::string> getTypeArrayForExpression(
			SgExpression * currentExpression);

protected:
	void visit(SgNode* node);

private:
	std::ofstream * outfile;
	std::string stFile;
	std::string typeX;
	std::string typeY;
	std::string typeY_cast;
	std::string typeX_cast;
	std::vector<std::string> definedSymbolsList;
	std::vector<SgNode*> statementList;
	void outputOperator(std::string outputMessage);
	void generateDefinedSymbolsList();
	std::vector<OperatorWithTypes*> listOfNewOperatorsWithTypes;
	std::vector<OperatorWithTypes*> listOfOldOperatorsWithTypes;
	std::vector<OperatorCallUniqueIdentifier *> listOfUniqueCallIdentifiers;
	std::vector<OperatorWithTypes*> listOfInitialDefinitionsFunctions;
	std::vector<OperatorCallUniqueIdentifier *> listOfUniqueCallInitialDefinition;
	bool typeYIsPrimitive;
	/*! \brief The lambda expressions generated from the basis.
	 *
	 */
	std::vector<TypedLambdaExpression *> arrayOfLambdaExpressions;
	bool isUserDefinedOpsSource;
	std::string outputFileForOpsDefs;

};

} /* namespace ST */

#endif /* SRC_ST_SYMBOLTABLEGENERATOR_H_ */
