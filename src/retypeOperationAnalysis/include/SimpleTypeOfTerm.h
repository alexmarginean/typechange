/*
 * BinaryOperatorSimpleType.h
 *
 *  Created on: 12 Jan 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_SIMPLETYPEOFTERM_
#define SRC_RETYPEOPERATIONANALYSIS_SIMPLETYPEOFTERM_

#include <vector>
#include <string>

#include <TypeOfTerm.h>

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>

namespace analyzer {

/*! \brief Class that represents a simple type of operator arguments.
 *
 *  Here are included all types of operators' arguments that are not dependent on the return of another operator.
 */
class SimpleTypeOfTerm: public TypeOfTerm {
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar & boost::serialization::base_object<TypeOfTerm>(*this);
		ar & this->typeArray;
	}

public:
	std::string getTypeName();
	SimpleTypeOfTerm();
	SimpleTypeOfTerm(std::string type);
	SimpleTypeOfTerm(std::vector<std::string> typeArray);
	~SimpleTypeOfTerm();
	std::string toString();
	std::string retypeTypesToString(std::string typeX, std::string typeY,
			std::string typeCast, std::string typeCastOld);
	SimpleTypeOfTerm(SimpleTypeOfTerm * newType);
	std::vector<std::string> getTypeArrayOfStrings();
	bool operator==(SimpleTypeOfTerm t2);

private:
	//std::string type;
	std::vector<std::string> typeArray;
};

inline std::vector<std::string> SimpleTypeOfTerm::getTypeArrayOfStrings() {
	return this->typeArray;
}

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_SIMPLETYPEOFTERM_ */
