/*
 * TypeInferenceConstraintSolver.h
 *
 *  Created on: 17 Mar 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_TYPEINFERENCECONSTRAINTSOLVER_H_
#define SRC_RETYPEOPERATIONANALYSIS_TYPEINFERENCECONSTRAINTSOLVER_H_

#include <z3++.h>
#include <vector>
#include <string>
#include <map>
#include <TypedLambdaExpression.h>
#include <utils.h>

#include <OperatorDefinition.h>
#include <NewOperatorDefinition.h>
#include <OldOperatorDefinition.h>

using namespace std;
using namespace z3;

namespace analyzer {

class UsageOfFunction;
class OperatorDefinition;
class OperatorUsagesWithNewDefs;
class ConstraintError;

/*! \bried The class that represents the constraint solving for Retype.
 *
 * It is based on Z3 constraint solver.
 */
class TypeInferenceConstraintSolver {
public:

	/*! \brief The constructor for this class.
	 *
	 * For the moment this is empty. It may be used for certain Z3 configurations.
	 */
	TypeInferenceConstraintSolver();
	/* \brief The constructor that takes as input an OperatorUsagesWithNewDefs*
	 *
	 */
	TypeInferenceConstraintSolver(OperatorUsagesWithNewDefs* currentOp,
			bool castOperatorProvided, std::ofstream * constraintsOutput,
			bool outputConstraints);

	/*! \brief Method that converts a constant in Z3 into expression.
	 *
	 * This is needed for creating the enum types.
	 */
	z3::expr get_expr(context &c, Z3_func_decl constant);
	/*! \brief Method that creates a new Z3 enum type.
	 *
	 * This is required because Z3 does not have default C++ API call for creating an enum.
	 */
	z3::sort createEnumTypes(context &c, char const *dtypename, int count,
			std::vector<std::string> typeNamesString, func_decl_vector& consts);
	/*! \brief This method initialize the retype constraint solver.
	 *
	 * It sets up all the things that are required for Z3:
	 * the context, the solver, the enum type, the enum elements, etc.
	 */
	void createContextTypeAndEnumForSolver(
			std::vector<std::string> typeNamesString, int typeOfReturn);
	/*! \brief This method creates a constraint on the idOfArg argument, to be of idOfType type, and
	 * push it in the constraints array.
	 */
	void pushConstraint(Types idOfType, int idOfArg);
	/*! \brief This method creates a constraint on the return of the function, to be of ''type'' type.
	 *
	 */
	void pushReturnTypeConstraint(Types type);
	/*! \brief This method calls the Z3 method for solving the constraints.
	 *
	 * It also checks the satisfiability of the constraints. If it was unsat, then
	 * it checks for the reasons and output them to the user. If it was sat, then it means
	 * that all the constraints for the current operator are satisfiable.
	 */
	void solveConstraints();
	/*! \brief This method is similar with solveConstraints. Here we do not check anything
	 * about the results. We just return them.
	 *
	 */
	z3::check_result solveConstraintsForUsages();
	/*! \brief This method checks if a constraint is in a constraint list.
	 *
	 */
	bool identicalConstraintInConstraintList(
			TypedLambdaExpression * currentOpDef,
			std::vector<UsageOfFunction *> arrayOfUsagesOfFunction,
			TypedLambdaExpression ** identifiedIdenticalUsage);
	/*! \brief This method push a operator usage (in calls). It transforms the usage into constraints,
	 * and add it to the array of constraints, in the context of B, the initial Basis.
	 *
	 * Warning: we have used & for parameter here. We should change this for adhering to the style
	 * guide.
	 * TODO: Change here.
	 */
	void pushUsageInArrayOfUsages(TypedLambdaExpression * operatorDef,
			std::vector<UsageOfFunction *> &arrayOfUsages);
	/*! \brief This method push a operator definition. It transforms the usage into constraints,
	 * and add it to the array of constraints, in the context of B', the user defined Basis.
	 * NOTE: Here the definitions are not required to be unique. If they are not unique, then we would
	 * signal an error!
	 *
	 * Warning: we have used & for parameter here. We should change this for adhering to the style
	 * guide.
	 * TODO: Change here.
	 */
	void pushDefinitionInArrayOfDefs(TypedLambdaExpression * operatorDef,
			std::vector<UsageOfFunction *> &arrayOfUsages);
	/* \brief This method push an entire operator, with its usages and definitions!
	 *
	 *
	 */
	void pushOperatorInArrayOfUsages(OperatorDefinition * operatorDef,
			std::vector<UsageOfFunction*> &arrayOfUsages);
	/*! \brief This method push an entire operator definition.
	 *
	 *
	 */
	void pushOperatorDefInArrayOfDefs(
			OperatorDefinition * operatorDef,
			std::vector<UsageOfFunction*> &arrayOfUsages);

	/*! \brief This method push a operator usage, for the user defined user operator.
	 *
	 */
	void pushUsageForUserDefinedBasisAndMarkTheMatchedFunctions(
			TypedLambdaExpression * currentOpDef);
	/*! \brief This methods checks consistency between the user provided operators
	 * and the initial basis.
	 *
	 * It also updates the  basis with the handlers and
	 * whether or not is was handled. The constraints on the basis are updated in the
	 * case of the matching usages, with the ones in the current usage.
	 */
	bool checkConsistencyBetweenUserProvidedOperatorsAndB(
			std::vector<UsageOfFunction*> initialBasis,
			std::vector<UsageOfFunction*> usedDefinedBasis);
	/*! \brief This method transforms T_n into T_n_c, for the provided basis
	 *
	 */
	void addCastsToNewTypeInUserProvidedOps(
			std::vector<UsageOfFunction*> userDefinedBasis);
	/*! \brief This method solves the cast operators.
	 *
	 * We assume that case operator is provided at this point.
	 */
	void solveCastOperators(std::vector<UsageOfFunction*> initialBasis,
			std::vector<UsageOfFunction*> usedDefinedBasis);
	/*! \brief This method outputs on the console the list of identified functions required,
	 * from arrayOfFunctions
	 *
	 */
	void outputListOfInitialUsageOfFunctions(
			std::vector<UsageOfFunction *> arrayOfFunctions);
	/*! \brief This methods outputs the usages and the matched user provided
	 * operator, for a basis.
	 *
	 */
	bool generateConstraintsForTheMatchedDefinitionWithUsages(
			std::vector<UsageOfFunction*> arrayOfBasis);
	/*! \brief Getter for the castOperatorProvided_ data member.
	 *
	 */
	bool castOperatorProvided();
	/*! \brief Setter for the castOperatorProvided_ data member.
	 *
	 */
	void set_castOperatorProvided(bool isProvided);
	void test1();
	/*! \brief Temporary test method.
	 *
	 */
	void test2();
	/*! \brief Temporary test harness.
	 *
	 */
	void testHarness();
	/*! \brief This method returns the usage definitions mappings.
	 *
	 */
	std::map<TypedLambdaExpression *, std::vector<TypedLambdaExpression *> > getUsagesDefinitionsMappings() {
		return this->usagesDefinitionsMappings_;
	}

	std::vector<TypedLambdaExpression*> returnUniqueInitialDefinitions(
			OldOperatorDefinition* def);

	std::vector<ConstraintError*> errorVector();

private:
	/*! \brief The number of input arguments for the curent operator.
	 *
	 */
	int numberOfInputs_;
	/*! \brief The Z3 context for the current operator's solver.
	 *
	 */
	z3::context solverContext_;
	/*! \brief The Z3 solver for the current operator.
	 *
	 */
	z3::solver * solverRetype_;
	/*! \brief The type names of the current enum of types.
	 *
	 */
	z3::func_decl_vector * typeNames_;
	/*! \brief The sort aka Type that defines the enum of types.
	 *
	 */
	z3::sort * retypeTypesEnum_;
	/*! \brief The Z3 function declaration of the current operator.
	 *
	 */
	func_decl * functionDeclaration_;
	/*! \brief The domain array for the current operator.
	 *
	 */
	z3::sort_vector * domainArray_;
	/*! \brief The set of constraints that are identified for the current operator.
	 *
	 */
	expr_vector * arrayOfConstraints_;
	/*! \brief The array of all different functions usages. This means overloadings.
	 *
	 */
	std::vector<UsageOfFunction *> arrayOfUsagesOfFunction_;
	/*! \brief The array of functions provided by the user.
	 *
	 */
	std::vector<UsageOfFunction *> arrayOfUserProvidedFunctions_;
	/*! \brief This variable logs whether or not the cast operator
	 * is provided by the user.
	 *
	 * This is required for treating T_n_c
	 */
	bool castOperatorProvided_;
	/*! \brief This map will contain the mappings between usages of functions
	 * and the provided user definition.
	 *
	 */
	std::map<TypedLambdaExpression *, std::vector<TypedLambdaExpression *> > usagesDefinitionsMappings_;
	/*! \brief output file for constraints
	 *
	 */
	std::ofstream * constraintsOutputFile_;
	/*! \brief bool for checking wheter or not we output the constraints.
	 *
	 */
	bool constraintsFileProvided;
	/*! \brief array that holds the error identified during the constraint solving!
	 *
	 */
	std::vector<ConstraintError*> errorVector_;
};

inline bool TypeInferenceConstraintSolver::castOperatorProvided() {
	return this->castOperatorProvided_;
}

inline void TypeInferenceConstraintSolver::set_castOperatorProvided(
		bool isProvided) {
	this->castOperatorProvided_ = isProvided;
}

inline std::vector<ConstraintError*> TypeInferenceConstraintSolver::errorVector() {
	return this->errorVector_;
}

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_TYPEINFERENCECONSTRAINTSOLVER_H_ */
