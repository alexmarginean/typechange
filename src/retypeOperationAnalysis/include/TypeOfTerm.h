/*
 * BinaryOperatorSymbolicType.h
 *
 *  Created on: 12 Jan 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_TYPEOFTERM_
#define SRC_RETYPEOPERATIONANALYSIS_TYPEOFTERM_

#include <string>
#include <vector>

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>

namespace analyzer {

/*! \brief Abstract Class that represents type of operator arguments.
 *
 *  From this class we will derive types for binnary operators or n-arry operators
 */
class TypeOfTerm {
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
	}
public:
	virtual std::string getTypeName() = 0;
	virtual ~TypeOfTerm() {
	}
	virtual std::string toString() = 0;

	virtual std::string retypeTypesToString(std::string typeX,
			std::string typeY, std::string typeCast, std::string typeCastOld) = 0;

	virtual std::vector<std::string> getTypeArrayOfStrings() = 0;
};

BOOST_SERIALIZATION_ASSUME_ABSTRACT (TypeOfTerm)

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_TYPEOFTERM_ */
