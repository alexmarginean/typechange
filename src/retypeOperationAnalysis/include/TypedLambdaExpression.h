/*
 * BinaryOperatorTable.h
 *
 *  Created on: 12 Jan 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_TYPEDLAMBDAEXPRESSION_H_
#define SRC_RETYPEOPERATIONANALYSIS_TYPEDLAMBDAEXPRESSION_H_

#include <vector>
#include <string>
#include <ostream>
#include <rose.h>
#include <utils.h>

#include <TypeOfTerm.h>
#include <SimpleTypeOfTerm.h>
#include <MultiArgsOperatorWithTypes.h>
#include <BinaryOperatorWithTypes.h>

/*! \brief Headers for serialization.
 *
 */
#include <boost/archive/tmpdir.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/list.hpp>
#include <boost/serialization/assume_abstract.hpp>

namespace analyzer {

/*! \brief Class that identify the informations about a operator call.
 *
 */
class OperatorCallUniqueIdentifier {

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar & this->line_;
		ar & this->column_;
		ar & this->filename_;
	}

public:
	OperatorCallUniqueIdentifier() {
		line_ = -1;
		column_ = -1;
		filename_ = "NONE";
	}
	OperatorCallUniqueIdentifier(int line, int column, std::string filename) {
		this->line_ = line;
		this->column_ = column;
		this->filename_ = filename;
	}

	int line() {
		return this->line_;
	}
	int column() {
		return this->column_;
	}
	std::string filename() {
		return this->filename_;
	}

	friend std::ostream& operator<<(std::ostream& os,
			const OperatorCallUniqueIdentifier& exp) {
		os << "FILE: " << exp.filename_ << " LINE: " << exp.line_ << " COLUMN: "
				<< exp.column_ << std::endl;
	}

	bool operator==(const Sg_File_Info *fileInfo) {

		//std::cout << fileInfo->get_raw_filename() << std::endl;

		if (this->line_ == fileInfo->get_raw_line()
				&& this->column_ == fileInfo->get_raw_col()
				&& this->filename_ == fileInfo->get_raw_filename()) {
			return true;
		}
		return false;
}

//private:
int line_;
int column_;
std::string filename_;
};

/*! \brief Currently this is only a mokup class
 * that contains just an unique id
 */
class TypedLambdaExpression {

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar & this->typesOfArgs_;
		ar & this->typesOfReturn_;
		ar & this->currentID;
		ar & this->customID;
		ar & this->typeX;
		ar & this->typeY;
		ar & this->typeCastToY;
		ar & this->typeCastToX;

		ar.register_type(static_cast<BinaryOperatorWithTypes *>(NULL));
		ar.register_type(static_cast<MultiArgsOperatorWithTypes *>(NULL));

		ar & this->currentOperatorWithTypes_;
		ar & this->uniqueCallIdentifier_;
		ar & this->identicalHandledUsage_;

		ar & this->initialTypeLambdaExpr_;

		ar & this->isUserDefinedOp_;

		ar & this->model_true_;
	}

public:
	TypedLambdaExpression(int customID);
	TypedLambdaExpression();
	TypedLambdaExpression(std::vector<std::vector<Types> > opWithTypes,
			std::vector<Types> retType, int customID) {
		this->customID = customID;
		this->currentOperatorWithTypes_ = NULL;
		this->uniqueCallIdentifier_ = NULL;

		this->typesOfArgs_ = opWithTypes;
		this->typesOfReturn_ = retType;

		this->identicalHandledUsage_ = NULL;
		this->initialTypeLambdaExpr_ = NULL;

		isUserDefinedOp_ = true;
	}
	TypedLambdaExpression(OperatorWithTypes* currentOperator,
			OperatorCallUniqueIdentifier * uniqueCallIdentifier,
			std::string typeX, std::string typeY, std::string typeCastToY,
			std::string typeCastToX);
	TypedLambdaExpression(OperatorWithTypes* currentOperator, std::string typeX,
			std::string typeY, std::string typeCastToY, std::string typeCastToX,
			int customID);

	friend std::ostream& operator<<(std::ostream& os,
			const TypedLambdaExpression& exp);
	void generateListOfTypeArgs(OperatorWithTypes* currentOperator);
	std::vector<std::vector<Types> > typesOfArgs();
	std::vector<Types> typesOfReturn() {
		return this->typesOfReturn_;
	}
	void set_typesOfArgs(std::vector<std::vector<Types> > args) {
		this->typesOfArgs_ = args;
	}

	OperatorCallUniqueIdentifier * uniqueCallIdentifier() {
		return this->uniqueCallIdentifier_;
	}

	OperatorWithTypes * currentOperatorWithTypes() {
		return this->currentOperatorWithTypes_;
	}

	static int ID;

	TypedLambdaExpression* getIdenticalHandledUsage() {
		return this->identicalHandledUsage_;
	}

	void setIdenticalHandledUsage(
			TypedLambdaExpression* identicalHandledUsage) {
		this->identicalHandledUsage_ = identicalHandledUsage;
	}

	void setInitialTypeLambdaExpr(
			TypedLambdaExpression * initialTypeLambdaExpr) {
		this->initialTypeLambdaExpr_ = initialTypeLambdaExpr;
	}
	TypedLambdaExpression * getInitialTypeLambdaExpr() {
		return this->initialTypeLambdaExpr_;
	}

	void setIsUserDefinedOp(bool value) {
		this->isUserDefinedOp_ = value;
	}

	bool getIsUserDefinedOp() {
		return this->isUserDefinedOp_;
	}

private:
	std::vector<std::vector<Types> > typesOfArgs_;
	std::vector<Types> typesOfReturn_;

	int currentID;

	int customID;
	std::string typeX;
	std::string typeY;
	std::string typeCastToY;
	std::string typeCastToX;

	OperatorWithTypes * currentOperatorWithTypes_;
	OperatorCallUniqueIdentifier * uniqueCallIdentifier_;

	TypedLambdaExpression * identicalHandledUsage_;

	TypedLambdaExpression * initialTypeLambdaExpr_;

	bool isUserDefinedOp_;

public:
	std::vector<Types> model_true_;
};

inline std::vector<std::vector<Types> > TypedLambdaExpression::typesOfArgs() {
	return this->typesOfArgs_;
}

} /* namespace analyzer */

#endif /* SRC_RETYPEOPERATIONANALYSIS_TYPEDLAMBDAEXPRESSION_H_ */
