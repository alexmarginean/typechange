/*
 * utils.h
 *
 *  Created on: 24 Mar 2015
 *      Author: alex
 */

#ifndef SRC_RETYPEOPERATIONANALYSIS_INCLUDE_UTILS_H_
#define SRC_RETYPEOPERATIONANALYSIS_INCLUDE_UTILS_H_

namespace analyzer {
/*! \brief This enum defines the type available in our retyping system
 *
 * T_o means the original type; T_n means the new type; T_n_c means the new
 * type with the case operator; T_u means any other type which is not interesting for retype
 */
enum Types {
	T_o = 0, T_n = 1, T_n_c_y = 2, T_u = 3, T_n_c_x = 4
};

#define SUBSCRIPT_O "\xe2\x82\x92"
#define SUBSCRIPT_N "\xe2\x82\x99"
#define SUBSCRIPT_U "\xe2\x82\x93"
#define SUBSCRIPT_C "\xe2\x82\x9c"

//#define SUBSCRIPT_C "\xe2\x82\x94"


static const char *super[] = { "\xe2\x81\xb0", "\xc2\xb9", "\xc2\xb2",
		"\xc2\xb3", "\xe2\x81\xb4", "\xe2\x81\xb5", "\xe2\x81\xb6",
		"\xe2\x81\xb7", "\xe2\x81\xb8", "\xe2\x81\xb9" };
static const char *sub[] = { "\xe2\x82\x80", "\xe2\x82\x81", "\xe2\x82\x82",
		"\xe2\x82\x83", "\xe2\x82\x84", "\xe2\x82\x85", "\xe2\x82\x86",
		"\xe2\x82\x87", "\xe2\x82\x88", "\xe2\x82\x89" };
static const char *subO = "\xe2\x82\x92";
static const char * subN = "\xe2\x82\x99";

}

#endif /* SRC_RETYPEOPERATIONANALYSIS_INCLUDE_UTILS_H_ */
