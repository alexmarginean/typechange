/*
 * BinaryOperatorTable.cpp
 *
 *  Created on: 12 Jan 2015
 *      Author: alex
 */

#include <TypedLambdaExpression.h>
#include <z3++.h>
#include <OperatorWithTypes.h>
#include <utils.h>

namespace analyzer {

int TypedLambdaExpression::ID = 0;

TypedLambdaExpression::TypedLambdaExpression() :
		currentID(ID++) {
	customID = -1;
	this->uniqueCallIdentifier_ = NULL;
	this->currentOperatorWithTypes_ = NULL;
	this->identicalHandledUsage_ = NULL;
	this->initialTypeLambdaExpr_ = NULL;
	this->isUserDefinedOp_ = true;
}

void TypedLambdaExpression::generateListOfTypeArgs(
		OperatorWithTypes* currentOperator) {
	this->identicalHandledUsage_ = NULL;
	this->initialTypeLambdaExpr_ = NULL;

	/*!
	 * Generate the list of std::vector<std::vector<Types> >: typesOfArgs_;
	 * Not any idea way the iterators are not working :-??
	 */

	for (int i = 0; i < ((currentOperator)->getListOfArguments()).size(); i++) {
		std::vector<Types> currentVector;

		for (int j = 0;
				j
						< (((currentOperator)->getListOfArguments())[i]->getTypeArrayOfStrings()).size();
				j++) {
			std::string currentStr =
					(((currentOperator)->getListOfArguments())[i]->getTypeArrayOfStrings())[j];
			if (currentStr == this->typeX) {
				currentVector.push_back(T_o);
			} else if (currentStr == this->typeY) {
				currentVector.push_back(T_n);
			} else if (currentStr == this->typeCastToY) {
				currentVector.push_back(T_n_c_y);
			} else if (currentStr == this->typeCastToX) {
				currentVector.push_back(T_n_c_x);
			} else {
				currentVector.push_back(T_u);
			}
		}
		this->typesOfArgs_.push_back(currentVector);
	}

	std::vector<Types> returnArray;
	for (int j = 0;
			j
					< (currentOperator->returnTypesOfOperator()->getTypeArrayOfStrings().size());
			j++) {
		std::string currentStr =
				((currentOperator)->returnTypesOfOperator()->getTypeArrayOfStrings())[j];
		if (currentStr == this->typeX) {
			returnArray.push_back(T_o);
		} else if (currentStr == this->typeY) {
			returnArray.push_back(T_n);
		} else if (currentStr == this->typeCastToY) {
			returnArray.push_back(T_n_c_y);
		} else if (currentStr == this->typeCastToX) {
			returnArray.push_back(T_n_c_x);
		} else {
			returnArray.push_back(T_u);
		}
	}
	this->typesOfReturn_ = returnArray;

#if 0
	for (std::vector<TypeOfTerm*>::iterator it =
			currentOperator->getListOfArguments().begin();
			it != currentOperator->getListOfArguments().end(); it++) {
		std::vector<Types> currentVector;

		std::cout << (*it)->getTypeArrayOfStrings().size() << std::endl;
		for (std::vector<std::string>::iterator itString =
				(*it)->getTypeArrayOfStrings().begin();
				itString != (*it)->getTypeArrayOfStrings().end(); itString++) {
			if ((*itString) == this->typeX) {
				currentVector.push_back(T_o);
			} else if ((*itString) == this->typeY) {
				currentVector.push_back(T_n);
			} else if ((*itString) == this->typeCastToY) {
				currentVector.push_back(T_n_c_y);
			} else {
				currentVector.push_back(T_u);
			}
		}
		this->typesOfArgs_.push_back(currentVector);
	}
#endif
}

TypedLambdaExpression::TypedLambdaExpression(OperatorWithTypes* currentOperator,
		OperatorCallUniqueIdentifier * uniqueCallIdentifier, std::string typeX,
		std::string typeY, std::string typeCastToY, std::string typeCastToX) :
		currentID(ID++) {
	this->initialTypeLambdaExpr_ = NULL;
	this->identicalHandledUsage_ = NULL;
	this->currentOperatorWithTypes_ = currentOperator;
	this->typeX = typeX;
	this->typeY = typeY;
	this->typeCastToY = typeCastToY;
	this->typeCastToX = typeCastToX;
	this->uniqueCallIdentifier_ = uniqueCallIdentifier;
	this->isUserDefinedOp_ = true;


	/*!
	 * Generate the list of std::vector<std::vector<Types> >: typesOfArgs_;
	 */

	generateListOfTypeArgs(currentOperator);

	customID = -1;
}

TypedLambdaExpression::TypedLambdaExpression(OperatorWithTypes* currentOperator,
		std::string typeX, std::string typeY, std::string typeCastToY,
		std::string typeCastToX, int customID) :
		currentID(ID++) {
	this->initialTypeLambdaExpr_ = NULL;
	this->identicalHandledUsage_ = NULL;
	this->currentOperatorWithTypes_ = currentOperator;
	this->currentOperatorWithTypes_ = NULL;
	this->isUserDefinedOp_ = true;


	this->typeX = typeX;
	this->typeY = typeY;
	this->typeCastToY = typeCastToY;
	this->typeCastToX = typeCastToX;

	this->identicalHandledUsage_ = NULL;

	/*!
	 * Generate the list of std::vector<std::vector<Types> >: typesOfArgs_;
	 */

	generateListOfTypeArgs(currentOperator);

	this->customID = customID;
}

TypedLambdaExpression::TypedLambdaExpression(int customID) :
		currentID(ID++) {
	this->customID = customID;
	this->currentOperatorWithTypes_ = NULL;
	this->uniqueCallIdentifier_ = NULL;
	this->isUserDefinedOp_ = true;


	this->identicalHandledUsage_ = NULL;
	this->initialTypeLambdaExpr_ = NULL;

}

std::ostream& operator<<(std::ostream& os, const TypedLambdaExpression& exp) {
	if (!exp.uniqueCallIdentifier_) {
		/*!
		 * In this case we do not have call identifiers
		 * and operators with types. This is the usage of this
		 * class for testing purposes, where we do not actually generate
		 * a lambda expression. In this case we are using the custom id for
		 * testing the inference engine.
		 */
		os << exp.customID;
	} else {
		/*!
		 * Proper usage of the class.
		 */
		if (exp.currentOperatorWithTypes_) {
			/*!
			 * We have an operator with types!
			 */
			os << std::endl << "UNIQUE ID: " << exp.currentID << " Operator: "
					<< exp.currentOperatorWithTypes_->toString()
					<< " Unique id: line : "
					<< exp.uniqueCallIdentifier_->line() << " , column: "
					<< exp.uniqueCallIdentifier_->column() << " , filename: "
					<< exp.uniqueCallIdentifier_->filename() << std::endl
					<< std::endl;
		} else {
			/*!
			 * We do not have an operator with types.
			 * This is after the serialization.
			 */
			os << std::endl << "UNIQUE ID: " << exp.currentID
					<< " Unique id: line : "
					<< exp.uniqueCallIdentifier_->line() << " , column: "
					<< exp.uniqueCallIdentifier_->column() << " , filename: "
					<< exp.uniqueCallIdentifier_->filename() << std::endl
					<< std::endl;
		}

		//os << exp
	}

	return os;
}

} /* namespace analyzer */
