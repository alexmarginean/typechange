#include <rose.h>
#include <boost/filesystem.hpp>
#include <RetypeParameterParser.h>
#include "include/RetypeOperationAnalyzer.h"
#include <OldOperatorDefinition.h>
//#include <z3++.h>
#include <TypedLambdaExpression.h>
#include <NewOperatorDefinition.h>
#include <OldOperatorDefinition.h>
#include <HighOrderTypeOfTerm.h>
#include <CollectionOfOperatorDefinitions.h>

#include <sstream>

using namespace std;
//using namespace z3;

using namespace retyper;
using namespace analyzer;

#define DEBUG

#define SOURCE_POSITION Sg_File_Info::generateDefaultFileInfoForTransformationNode()

// Build a synthesized attribute for the tree traversal

int main(int argc, char * argv[]) {

	std::string initialType;
	std::string newVarType;
	std::string headerFile;
	std::string projectPath;
	std::string stFile;
	std::string outputOpsFile;

	bool isStruct = false;
	bool isUserDefinedOps = false;
	std::string operatorDefsOutFile;

	std::vector<string> includedPaths;

	RetypeParameterParser parametersParser;

	Rose_STL_Container<string> l = parametersParser.parseRetypeParametersAnalyzer(argc, argv, &initialType, &newVarType,
			&headerFile, &projectPath, & outputOpsFile, &isStruct, includedPaths, &stFile, &operatorDefsOutFile, &isUserDefinedOps);

//SYMBOL TABLE GENERATOR!!!

	ios::sync_with_stdio();     // Syncs C++ and C I/O subsystems!

	ofstream datfile(outputOpsFile.c_str(), ios::out | ios::app);
	if (datfile.good() == false) {
		printf("File failed to open \n");
	}

//ofstream testTemp("testNewOpOut.out", ios::out);

// datfile << "This is a test!" << std::endl;

	SgProject* project = frontend(l);
	ROSE_ASSERT (project != NULL);

	/*!
	 * Constructor for the retype operation analyzer.
	 */
	RetypeOperationAnalyzer traversal(&datfile, stFile, initialType, newVarType,
			isUserDefinedOps, operatorDefsOutFile);
//traversal.setDefaultProjectPath(boost::filesystem::canonical(projectPath).string());
	/*!
	 * Now traverse the project. We should call the traverse method
	 * from here, altough it is logically in the RetypeOperationAnalyzer class.
	 */
	traversal.traverse(project, preorder);

	/*!
	 * Generate the list of defined operators.
	 */
	if (isUserDefinedOps == false) {
		/*!
		 * Not user defined ops, so we should analyse the entire thing.
		 */
		traversal.analyseReversedPreorder();
		traversal.outputOperatorList();
	} else {
		/*!
		 * User defined ops, so we should analyse just the definitions.
		 */
		traversal.analyseReversedPreorderJustDefinitions();
		traversal.outputOperatorListJustInitial();

	}


	CollectionOfOperatorDefinitions * generatedOpsDefs =
			traversal.generateListOfIdentifiedOps(isUserDefinedOps);


	/*!
	 * Here is the logic for checking wheter or not the file already exists.
	 * If it exists, because of a limitation in boost serializer
	 * (we can't append an archive to an existing one) we have to read the current archive,
	 * combine both of them in one (as objects from here), and output the new collection,
	 * containing both of them
	 *
	 */

	CollectionOfOperatorDefinitions::safe_save_operator_defs_append(
			*generatedOpsDefs, operatorDefsOutFile);


#if 0
		std::ofstream outfile("/home/alex/Desktop/OUT_FILE",
				ios::out /*| ios::app*/);
		outfile << *generatedOpsDefs << std::endl;

#if 1
		CollectionOfOperatorDefinitions restoredCol;
		CollectionOfOperatorDefinitions::restore_operator_defs(restoredCol,
				operatorDefsOutFile);

		std::ofstream outfile2("/home/alex/Desktop/OUT_FILE_RESTORED",
				ios::out /*| ios::app*/);
		outfile2 << restoredCol << std::endl;
#endif

#endif

	//cout << *generatedOpsDefs << endl;

	// operators to constraint solver
	//traversal.transformToConstraints();
	//traversal.solveConstraints();

	datfile.close();

#if 0
	generateAstGraph(project, 30000);
	generateDOT(*project);
#endif

	std::cout << "Operators Analyser Completed Successful!";

	return backend(project);
}
