#!/usr/bin/python

import sys
import argparse
import os

import subprocess

# first set the environment for retype. This should be done from the install script! TODO:
os.environ["PATH"] += ":/home/alex/Development/Tools/ROSE_LAST_REPO_VERSION/InstallTree/bin:/home/alex/Development/Tools/ROSE_LAST_REPO_VERSION/boost/boost_1_53_install_tree/lib:/home/alex/Development/TypeChanger/lib/z3"
os.environ["LD_LIBRARY_PATH"] = ":/home/alex/Development/Tools/ROSE_LAST_REPO_VERSION/InstallTree/bin:/home/alex/Development/Tools/ROSE_LAST_REPO_VERSION/boost/boost_1_53_install_tree/lib:/home/alex/Development/TypeChanger/lib/z3"

class RetypeCompilersArgs:
    def __init__(self, st_compiler, analyzer_compiler, retype_compiler, inference_engine, st_file, project_path, init_t,
                 new_t, new_h, out_ops, op_defs, mappings, constraints_solution, constraints_out, cast_old, cast_new,
                 cast_op_exists, constraints_errors):
        # First the compilers
        self.st_compiler = st_compiler
        self.analyzer_compiler = analyzer_compiler
        self.retype_compiler = retype_compiler
        self.inference_engine = inference_engine

        # Finally, the arguments
        self.retype_ST = st_file
        self.retype_project_path = project_path
        self.retype_X = init_t
        self.retype_Y = new_t
        self.retype_header = new_h
        self.retype_out = out_ops
        self.retype_op_def = op_defs
        self.retype_mappings = mappings
        self.retype_solution = constraints_solution
        self.retype_constraints = constraints_out
        self.retype_cast_old = cast_old
        self.retype_cast_new = cast_new
        self.retype_casts = cast_op_exists
        self.retype_errors = constraints_errors

    def return_st_generator_compiler(self):
        st_generator_call = self.st_compiler + ' --retype_ST ' + self.retype_ST + ' --retype_project_path ' + \
                            self.retype_project_path
        if DEBUG:
            print(st_generator_call)
        return st_generator_call

    def return_analyzer_compiler_user_defined_ops(self):
        analyzer_user_def_call = self.analyzer_compiler + ' --retype_X ' + self.retype_X + ' --retype_Y ' + \
                                 self.retype_Y + ' --retype_ST ' + \
                                 self.retype_ST + ' --retype_out ' + self.retype_out + ' --retype_project_path ' + \
                                 self.retype_project_path + ' --retype_op_def ' + self.retype_op_def + \
                                 ' --retype_type ops'
        if self.retype_header is not None:
            analyzer_user_def_call += ' --retype_header ' + self.retype_header
        if DEBUG:
            print(analyzer_user_def_call)
        return analyzer_user_def_call

    def return_analyzer_compiler_input_program(self):
        analyzer_input_program_call = self.analyzer_compiler + ' --retype_X ' + self.retype_X + ' --retype_Y ' + \
                                 self.retype_Y + ' --retype_ST ' + \
                                 self.retype_ST + ' --retype_out ' + self.retype_out + ' --retype_project_path ' + \
                                 self.retype_project_path + ' --retype_op_def ' + self.retype_op_def + \
                                 ' --retype_type program'
        if self.retype_header is not None:
            analyzer_input_program_call += ' --retype_header ' + self.retype_header
        if DEBUG:
            print(analyzer_input_program_call)
        return analyzer_input_program_call

    def return_inference_engine(self):
        inference_engine_call = self.inference_engine + ' --retype_op_def ' + self.retype_op_def + \
                                ' --retype_solution ' + self.retype_solution + \
                                ' --retype_constraints ' + self.retype_constraints + ' --retype_Y ' + self.retype_Y
        if self.retype_casts:
            inference_engine_call += ' --retype_casts'
        if self.retype_mappings is not None:
            inference_engine_call += '  --retype_mappings ' + self.retype_mappings
        if self.retype_errors is not None:
            inference_engine_call += ' --retype_errors ' + self.retype_errors
        if DEBUG:
            print(inference_engine_call)
        return inference_engine_call

    def return_retype_compiler(self):
        retype_compiler_call = self.retype_compiler + ' --retype_X ' + self.retype_X  + ' --retype_Y ' + self.retype_Y + \
                               ' --retype_project_path ' + self.retype_project_path + ' --retype_ST ' + self.retype_ST \
                               + ' --retype_solution ' + self.retype_solution
        if self.retype_header is not None:
            retype_compiler_call += ' --retype_header ' + self.retype_header
        if self.retype_cast_old is not None:
            retype_compiler_call += ' --retype_cast_old ' + self.retype_cast_old
        if self.retype_cast_new is not None:
           retype_compiler_call += ' --retype_cast_new ' + self.retype_cast_new
        if DEBUG:
            print(retype_compiler_call)
        return retype_compiler_call


def main(argv):
    # we need the following args:
    # initial type, new type, project path,
    # cast_to_old, cast_to_new,
    # user ops header file,
    # user ops source file
    # initial C compiler

    # Hardcoded constant variables

    # Output and error streams
    out_std_name="stdout.out"
    out_err_name="stderr.out"

    out_std = open(out_std_name, "w")
    out_err = open(out_err_name, "w")

    # Absolute paths to the three compilers used by retype: stGenerator, analyzer, and retype; and to the inference
    # engine: inference. NOTE: Here we assume that these 4 programs are in the current folder!!!

    # First save the current path, for having the locations of the ROSE based compilers
    script_root_path = os.path.dirname(os.path.abspath(__file__))

    st_generator_path = script_root_path + '/stGenerator'
    analyzer_path = script_root_path + '/analyzer'
    retype_path = script_root_path + '/retype'
    inference_path = script_root_path + '/inference'

    # Parse the command line arguments
    parser = argparse.ArgumentParser(usage='Please provide the following arguments: TODO',
                                     description='Retype main module')
    parser.add_argument('-i', '--initial_type', help='the initial type to be changed by retype')
    parser.add_argument('-n', '--new_type', help='the new type to replace the initial one')
    parser.add_argument('-p', '--project_path', help='the program path relative to the root makefile, defining internal'
                                                     'files. This defaults to .', default='.')
    parser.add_argument('-rM', '--root_makefile', help='the project root path, where the top level Makefile is present.'
                                                       'This defaults to .', default='.')
    parser.add_argument('-cO', '--cast_to_old_type', help='the cast to the old data type')
    parser.add_argument('-cN', '--cast_to_new_type', help='the cast to the new data type')
    parser.add_argument('-hF', '--header_file', help='the header file of the new data type')
    parser.add_argument('-sF', '--source_file', help='the source file of the new data type')
    parser.add_argument('-m', '--mapping_file', help='the mappings between the user provided operators and the '
                                                     'ones in the input program')
    parser.add_argument('-c', '--cast_operator_exists', help='whether or not the cast operators are defined. '
                                                             'Defaults to false!', default=False, action='store_true')
    parser.add_argument('-mU', '--makefile_user_ops', help='path to the Makefile of the user ops,'
                                                           'absolute or relative to the root Makefile')
    parser.add_argument('-D', '--debug_mode', help='are we in the debug mode? Default False!. ',
                        default=False, action='store_true')
    parser.add_argument('-e', '--errors', help='output file for constraints errors! If not provided, we output'
                                               'the errors on stdout.')

    args = parser.parse_args()

    # Set the global debug variable
    global DEBUG
    DEBUG = args.debug_mode

    # Construct the arguments required internal by the compilers
    st_file_name = "retypeST.data"
    output_operators = "opRetypes.data"
    op_defs = "opDefinitions.data"
    constraints_solutions = "solverOperators.data"
    constraints_output = "constraintSolverOut.data"

    compilers = RetypeCompilersArgs(st_generator_path, analyzer_path, retype_path, inference_path, st_file_name,
                                    args.project_path, args.initial_type, args.new_type, args.header_file,
                                    output_operators, op_defs, args.mapping_file, constraints_solutions,
                                    constraints_output, args.cast_to_old_type, args.cast_to_new_type,
                                    args.cast_operator_exists, args.errors)

    # Now move the execution in the root folder, where the main Makefile is
    os.chdir(args.root_makefile)

    # Now remove all the temporary files, potentially generated by a different run of retype
    subprocess.call(['rm', '-r', '-f', st_file_name, output_operators, op_defs, constraints_solutions,
                     constraints_output])

    # Generate the symbol table first

    # Append the st_generator compiler as CC to the environment
    env_st_gen = os.environ.copy()
    env_st_gen["cc"] = compilers.return_st_generator_compiler()

    # Call the compiler, and catch exceptions if the return code is not 0
    try:
        return_code = subprocess.check_call('make ', stdout=out_std, stderr=out_err, shell=True, env=env_st_gen)
    except subprocess.CalledProcessError as err:
        print("Error in the creation of the symbol table! Please check the error stream in " +
              script_root_path + "/" + out_err_name + "!")
        exit(-1)

    # Now call the analyzer for the input program
    env_analyzer_input_p = os.environ.copy()
    env_analyzer_input_p["cc"] = compilers.return_analyzer_compiler_input_program()

    # Call the compiler, and catch exceptions if the return code is not 0
    try:
        return_code = subprocess.check_call('make ', stdout=out_std, stderr=out_err, shell=True,
                                            env=env_analyzer_input_p)
    except subprocess.CalledProcessError as err:
        print("Error in operator analyzer for the input program! Please check the error stream in " +
              script_root_path + "/" + out_err_name + "!")
        exit(-1)

    # Now call the analyzer for the user defined operators. Here we need the user specified Makefile!
    # We call this, iff the user provided operators are specified!
    if args.makefile_user_ops is not None:
        env_analyzer_user_ops = os.environ.copy()
        env_analyzer_user_ops["cc"] = compilers.return_analyzer_compiler_user_defined_ops()
        try:
            return_code = subprocess.check_call('make --makefile ' + args.makefile_user_ops, stdout=out_std, stderr=out_err,
                                            shell=True, env=env_analyzer_user_ops)
        except subprocess.CalledProcessError as err:
            print("Error in operator analyzer for the user defined operators! Please check the error stream in " +
                  script_root_path + "/" + out_err_name + "!")
            exit(-1)

    # Now call the inference engine
    env_inference_engine = os.environ.copy()
    env_inference_engine["cc"] = compilers.return_inference_engine()
    try:
        return_code = subprocess.check_call('make ', stdout=out_std, stderr=out_err, shell=True,
                                            env=env_inference_engine)
    except subprocess.CalledProcessError as err:
        if err.returncode == 767:
            print('Error in the provided data types. Please check the error file provided with --error arg, or'
                  'the stdout stream!')
        else:
            print("Error in inference engine! Please check the error stream in " + script_root_path + "/" +
                  out_err_name + "!")
        exit(-1)

    # Finally call the retype compiler, for transforming the code!
    env_retype = os.environ.copy()
    env_retype["cc"] = compilers.return_retype_compiler()
    try:
        return_code = subprocess.check_call('make ', stdout=out_std, stderr=out_err, shell=True,
                                            env=env_retype)
    except subprocess.CalledProcessError as err:
        print("Error in the retyper! Please check the error stream in " +
              script_root_path + "/" + out_err_name + "!")
        exit(-1)

    print('Retype operation was successful!')
    exit(0)


if __name__ == "__main__":
    main(sys.argv[1:])
