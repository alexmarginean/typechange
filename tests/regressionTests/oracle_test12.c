#include "header.h" 
#include "/home/alex/Development/TypeChanger/tests/regressionTests/test12/_rose_unparsed_headers_/testHeader.h"
#include <stdio.h>

struct test_struct extFunc(struct test_struct DD,struct test_struct cc)
{
  return castToNewType(0);
}
void dada();

void main()
{
  struct test_struct a;
  struct test_struct b;
  struct test_struct w;
  struct test_struct x;
  struct test_struct y;
  struct test_struct z;
  struct test_struct u;
  int a0 = castToOldType(a);
  a0 = 11;
  a = castToNewType(a0);
  int b1 = castToOldType(b);
  b1 = 5;
  b = castToNewType(b1);
  newScanF("%d",&u);
  int w2 = castToOldType(w);
  w2 = castToOldType((opPlus(a,(extFunc(b,(castToNewType(3)))))));
  w = castToNewType(w2);
  int a3 = castToOldType(a);
  int w4 = castToOldType(w);
  w4 = a3 * 2;
  w = castToNewType(w4);
  a = castToNewType(a3);
  int b5 = castToOldType(b);
  int x6 = castToOldType(x);
  x6 = b5 * 1;
  x = castToNewType(x6);
  b = castToNewType(b5);
  int y7 = castToOldType(y);
  y7 = castToOldType((opPlus1(w,1)));
  y = castToNewType(y7);
  int z8 = castToOldType(z);
  z8 = castToOldType((opPlus(x,y)));
  z = castToNewType(z8);
}
