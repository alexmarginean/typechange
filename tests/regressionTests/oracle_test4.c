
struct struct_original 
{
  int i;
  double d;
  char *x;
}
;

double foo(double x)
{
  x . i = 10;
  return x;
}

int main()
{
  double a;
  a . i = 10;
  a . d = 20.0;
  a . x = 0;
  a = foo(a);
  return 0;
}
