#include "header.h" 

struct struct_original 
{
  int i;
  double d;
  char *x;
}
;

struct test foo(struct test x)
{
  x . i = 10;
  return x;
}

int main()
{
  struct test a;
  a . i = 10;
  a . d = 20.0;
  a . x = 0;
  a = foo(a);
  return 0;
}
