struct test_struct{
	int x;
	char *y;
};


int castToOldType(struct test_struct arg);

struct test_struct castToNewType(int arg);

struct test_struct opPlus(struct test_struct arg1, struct test_struct arg2);

struct test_struct opPlus3(struct test_struct arg1, struct test_struct arg2);

struct test_struct opPlus1(struct test_struct arg2, int arg1);

struct test_struct * newScanF(char * format, struct test_struct * arg);
