#include "header.h"


int castToOldType(struct test_struct arg){
	return arg.x;
}


struct test_struct castToNewType(int arg){
	struct test_struct val;
	val.x = arg;
	val.y = 0;
	return val;
}

struct test_struct opPlus(struct test_struct arg1, struct test_struct arg2){
	struct test_struct newStruct;
	newStruct.x = arg1.x + arg2.x;
	newStruct.y = 0;
	return newStruct;
}


struct test_struct opPlus3(struct test_struct arg1, struct test_struct arg2){
	struct test_struct newStruct;
	newStruct.x = arg1.x + arg2.x;
	newStruct.y = 0;
	return newStruct;
}


struct test_struct opPlus1(struct test_struct arg2, int arg1){
	struct test_struct newStruct;
	newStruct.x = arg1 + arg2.x;
	newStruct.y = 0;
	return newStruct;
}

/*
struct test_struct opPlus2(struct test_struct arg2, int arg1){
	struct test_struct newStruct;
	newStruct.x = arg1 + arg2.x;
	newStruct.y = 0;
	return newStruct;
}
*/

struct test_struct * newScanF(char * format, struct test_struct * arg){
	scanf(format, &(arg->x));
	return arg;
}
