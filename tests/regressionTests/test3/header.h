struct test_struct{
	int x;
	char *y;
};

struct test_struct intToTestStruct(int var){
	struct test_struct ret;
	ret.x = var;
	ret.y = 0;
	return ret;
}


int testStructToInt(struct test_struct var){
	return var.x;
}
