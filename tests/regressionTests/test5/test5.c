struct struct_original{
	int i;
	double d;
	char * x;
};


struct struct_original foo(struct struct_original x){
	x.i = 10;
	return x;
}


int main(){
	struct struct_original a;
	a.i = 10;
	a.d = 20.0;
	a.x = 0;
	a = foo(a);
	return 0;
}
