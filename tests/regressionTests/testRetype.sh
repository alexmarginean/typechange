#!/bin/bash

INITIAL_PATH=$(pwd)

NO_TEST_CASES=12
NO_TEST_CASES_FAILED=0

# First cleanup all the previously existing test cases!
rm -r -f rose_test*
rm -r -f headerFile*

# Go in the install folder, from where we can use retype
cd ../../InstalFolder/bin/


# TEST 1
python3.4 retype.py --initial_type double --new_type float --root_makefile $INITIAL_PATH/test1 --error errorConstraints.data
cp $INITIAL_PATH/test1/rose_test1.c $INITIAL_PATH/ 
echo -e "TEST 1 FINISHED!"


# TEST 2
python3.4 retype.py --initial_type float --new_type double --root_makefile $INITIAL_PATH/test2 --error errorConstraints.data
cp $INITIAL_PATH/test2/rose_test2.c $INITIAL_PATH/ 
echo -e "TEST 2 FINISHED!"


# TEST 3
python3.4 retype.py --initial_type int --new_type test_struct --root_makefile $INITIAL_PATH/test3 --header_file header.h --error errorConstraints.data --cast_to_old_type intToTestStruct --cast_to_new_type testStructToInt --cast_operator_exists
cp $INITIAL_PATH/test3/rose_test3.c $INITIAL_PATH/ 
echo -e "TEST 3 FINISHED!"


# TEST 4
#struct to primary type 
python3.4 retype.py --initial_type struct_original --new_type double --root_makefile $INITIAL_PATH/test4 --error errorConstraints.data
cp $INITIAL_PATH/test4/rose_test4.c $INITIAL_PATH/ 
echo -e "TEST 4 FINISHED!"


# TEST 5
#struct to struct
python3.4 retype.py --initial_type struct_original --new_type test  --root_makefile $INITIAL_PATH/test5 --header_file header.h --error errorConstraints.data
cp $INITIAL_PATH/test5/rose_test5.c $INITIAL_PATH/ 
echo -e "TEST 5 FINISHED!"

# TEST 6
#relative header test
python3.4 retype.py --header_file header.h --initial_type struct_original --new_type double --project_path ../ --root_makefile $INITIAL_PATH/test6_header/src --error errorConstraints.data
cp $INITIAL_PATH/test6/src/rose_test6.c $INITIAL_PATH/ && cp $INITIAL_PATH/test6/src/_rose_unparsed_headers_/headerFile $INITIAL_PATH/headerFile6.h
echo -e "TEST 6 FINISHED!"


# TEST 7
#relative header test with -I compiler option
python3.4 retype.py --initial_type struct_original --new_type double --project_path ../ --root_makefile $INITIAL_PATH/test7_header/src --error errorConstraints.data
cp $INITIAL_PATH/test7/src/rose_test7.c $INITIAL_PATH/ && cp $INITIAL_PATH/test7/src/_rose_unparsed_headers_/headerFile $INITIAL_PATH/headerFile7.h 
echo -e "TEST 7 FINISHED!"


# TEST 8
#test where the header file defines a variable declared as external in the source file and the header file is in the program patch; the external var should be retyped
python3.4 retype.py --initial_type int --new_type double --project_path ../ --root_makefile $INITIAL_PATH/test8_header/src --error errorConstraints.data
cp $INITIAL_PATH/test8/src/rose_test8.c $INITIAL_PATH/ && cp $INITIAL_PATH/test8/src/_rose_unparsed_headers_/headerFile $INITIAL_PATH/headerFile8.h
echo -e "TEST 8 FINISHED!"


# TEST 9
#test where the header file defines a variable declared as external in the source file and the header file is NOT in the program patch; the external var should NOT be retyped
python3.4 retype.py --initial_type int --new_type double --root_makefile $INITIAL_PATH/test9_header/src --error errorConstraints.data
cp $INITIAL_PATH/test9/src/rose_test9.c $INITIAL_PATH/ && cp $INITIAL_PATH/test9/src/_rose_unparsed_headers_/headerFile $INITIAL_PATH/headerFile9.h
echo -e "TEST 9 FINISHED!"


# TEST 12
python3.4 retype.py --initial_type int --new_type "test_struct" --project_path "../" --root_makefile $INITIAL_PATH/test12 --cast_to_old_type castToOldType --cast_to_new_type castToNewType --header_file header.h --source_file user_ops.c --mapping_file mappingOps.in --makefile_user_ops Makefile.Ops --cast_operator_exists --error errorConstraints.data
cp $INITIAL_PATH/test12/rose_test12.c $INITIAL_PATH/ && cp $INITIAL_PATH/test12/_rose_unparsed_headers_/testHeader.h $INITIAL_PATH/headerFile12.h
echo -e "TEST 10 FINISHED!"

cd $INITIAL_PATH

for i in $(seq 1 $NO_TEST_CASES);
do
	#if diff rose_test$i.c oracle_test$i.c >/dev/null ; then
	if diff rose_test$i.c oracle_test$i.c ; then
		echo -e "TEST "$i" PASSED!" 
	else
		echo -e "TEST "$i" FAILED!"
		NO_TEST_CASES_FAILED=$((NO_TEST_CASES_FAILED+1))
	fi
done


#check header file

for i in $(seq 12 $NO_TEST_CASES);
do
	if diff headerFile$i.h oracle_headerFile$i.h ; then
		echo -e "TEST HEADER "$i" PASSED!"
	else
		echo -e "TEST HEADER "$i" FAILED!"
		NO_TEST_CASES_FAILED=$((NO_TEST_CASES_FAILED+1))
	fi
done


exit 0





#test primitive data types
cd test1/ && rm -f stTest1.data && rm -f rose_test1.c && rm -f -r _rose_unparsed_headers_ && rm -f -r operators1.out && make cc="../stGenerator --retype_ST stTest1.data" >> /dev/null 2>&1 && make cc="../analyzer --retype_X double --retype_Y float --retype_ST stTest1.data --retype_out operators1.out" >> /dev/null 2>&1 && make cc="../retype --retype_X double --retype_Y float --retype_ST stTest1.data" >> /dev/null 2>&1 && cp rose_test1.c ../ && cp operators1.out ../ && cd ../ 
echo -e "TEST 1 FINISHED!"

cd test2/ && rm -f stTest2.data && rm -f rose_test2.c && rm -f -r _rose_unparsed_headers_ && rm -f -r operators2.out && make cc="../stGenerator --retype_ST stTest2.data" >> /dev/null 2>&1 && make cc="../analyzer --retype_X float --retype_Y double --retype_ST stTest2.data --retype_out operators2.out" >> /dev/null 2>&1 && make cc="../retype --retype_X float --retype_Y double --retype_ST stTest2.data" >> /dev/null 2>&1 && cp rose_test2.c ../ && cp operators2.out ../ && cd ../ 
echo -e "TEST 2 FINISHED!"

#test ADT
#int to struct
cd test3/ && rm -f stTest3.data && rm -f rose_test3.c && rm -f -r _rose_unparsed_headers_ && rm -f -r operators3.out && make cc="../stGenerator --retype_ST stTest3.data" >> /dev/null 2>&1 && make cc="../analyzer --retype_X int --retype_Y test_struct --retype_header header.h --retype_ST stTest3.data --retype_out operators3.out" >> /dev/null 2>&1 && make cc="../retype --retype_X int --retype_Y test_struct --retype_header header.h --retype_ST stTest3.data" >> /dev/null 2>&1 && cp rose_test3.c ../ && cp operators3.out ../ && cd ../   
echo -e "TEST 3 FINISHED!"
cd $INITIAL_PATH
#struct to primary type 
cd test4/ && rm -f stTest4.data && rm -f rose_test4.c && rm -f -r _rose_unparsed_headers_ && rm -f -r operators4.out && make cc="../stGenerator --retype_ST stTest4.data" >> /dev/null 2>&1 && make cc="../analyzer --retype_X struct_original --retype_Y double --retype_ST stTest4.data --retype_out operators4.out" >> /dev/null 2>&1 && make cc="../retype --retype_X struct_original --retype_Y double --retype_ST stTest4.data" >> /dev/null 2>&1 && cp rose_test4.c ../ && cp operators4.out ../ && cd ../
echo -e "TEST 4 FINISHED!"

#struct to struct
cd test5/ && rm -f stTest5.data && rm -f rose_test5.c && rm -f -r _rose_unparsed_headers_ && rm -f -r operators5.out && make cc="../stGenerator --retype_ST stTest5.data" >> /dev/null 2>&1 && make cc="../analyzer --retype_X struct_original --retype_Y test --retype_header header.h --retype_ST stTest5.data --retype_out operators5.out" >> /dev/null 2>&1 && make cc="../retype --retype_X struct_original --retype_Y test --retype_header header.h --retype_ST stTest5.data" >> /dev/null 2>&1 && cp rose_test5.c ../ && cp operators5.out ../ && cd ../
echo -e "TEST 5 FINISHED!"

#relative header test
cd test6_header/src && rm -f stTest6.data && rm -f rose_test6.c && rm -f -r _rose_unparsed_headers_ && rm -f -r operators6.out && make cc="../../stGenerator --retype_ST stTest6.data --retype_project_path ../" >> /dev/null 2>&1 && make cc="../../analyzer --retype_X struct_original --retype_Y double --retype_ST stTest6.data --retype_project_path ../ --retype_out operators6.out" >> /dev/null 2>&1 && make cc="../../retype --retype_X struct_original --retype_Y double --retype_ST stTest6.data --retype_project_path ../" >> /dev/null 2>&1 && cp rose_test6.c ../../ && cp _rose_unparsed_headers_/headerFile.h ../../headerFile6.h && cp operators6.out ../../ && cd ../../
echo -e "TEST 6 FINISHED!"

#relative header test with -I compiler option
cd test7_header/src && rm -f stTest7.data && rm -f rose_test7.c && rm -f -r _rose_unparsed_headers_ && rm -f -r operators7.out && make cc="../../stGenerator --retype_ST stTest7.data --retype_project_path ../" >> /dev/null 2>&1 && make cc="../../analyzer --retype_X struct_original --retype_Y double --retype_project_path ../ --retype_ST stTest7.data --retype_out operators7.out" >> /dev/null 2>&1 && make cc="../../retype --retype_X struct_original --retype_Y double --retype_project_path ../ --retype_ST stTest7.data" >> /dev/null 2>&1 && cp rose_test7.c ../../ && cp _rose_unparsed_headers_/headerFile.h ../../headerFile7.h && cp operators7.out ../../ && cd ../../ 
echo -e "TEST 7 FINISHED!"

#test where the header file defines a variable declared as external in the source file and the header file is in the program patch; the external var should be retyped
cd test8_header/src && rm -f stTest8.data && rm -f rose_test8.c && rm -f -r _rose_unparsed_headers_ && rm -f -r operators8.out && make cc="../../stGenerator --retype_ST stTest8.data --retype_project_path ../" >> /dev/null 2>&1 && make cc="../../analyzer --retype_X int --retype_Y double --retype_ST stTest8.data --retype_project_path ../ --retype_out operators8.out" >> /dev/null 2>&1 && make cc="../../retype --retype_X int --retype_Y double --retype_ST stTest8.data --retype_project_path ../" >> /dev/null 2>&1 && cp rose_test8.c ../../ && cp _rose_unparsed_headers_/headerFile.h ../../headerFile8.h && cp operators8.out ../../ && cd ../../
echo -e "TEST 8 FINISHED!"

#test where the header file defines a variable declared as external in the source file and the header file is NOT in the program patch; the external var should NOT be retyped
cd test9_header/src && rm -f stTest9.data && rm -f rose_test9.c && rm -f -r _rose_unparsed_headers_ && rm -f -r operators9.out && make cc="../../stGenerator --retype_ST stTest9.data" >> /dev/null 2>&1 && make cc="../../analyzer --retype_X int --retype_Y double --retype_ST stTest9.data --retype_out operators9.out" >> /dev/null 2>&1 && make cc="../../retype --retype_X int --retype_Y double --retype_ST stTest9.data" >> /dev/null 2>&1 && cp rose_test9.c ../../ && cp _rose_unparsed_headers_/headerFile.h ../../headerFile9.h && cp operators9.out ../../ && cd ../../
echo -e "TEST 9 FINISHED!"




#cd test11/ && rm -f stTest3.data && rm -f rose_test3.c && rm -f -r _rose_unparsed_headers_ && rm -f -r operators3.out && make cc="../stGenerator --retype_ST stTest3.data" >> /dev/null 2>&1 && make cc="../analyzer --retype_X int --retype_Y test_struct --retype_header header.h --retype_ST stTest3.data --retype_out operators3.out" >> /dev/null 2>&1 && make cc="../retype --retype_X int --retype_Y test_struct --retype_header header.h --retype_ST stTest3.data" >> /dev/null 2>&1 && cp rose_test3.c ../ && cp operators3.out ../ && cd ../   
#echo -e "TEST 11 FINISHED!"


cd $INITIAL_PATH

for i in $(seq 1 $NO_TEST_CASES);
do
	#if diff rose_test$i.c oracle_test$i.c >/dev/null ; then
	if diff rose_test$i.c oracle_test$i.c ; then
		echo -e "TEST "$i" PASSED!" 
	else
		echo -e "TEST "$i" FAILED!"
		NO_TEST_CASES_FAILED=$((NO_TEST_CASES_FAILED+1))
	fi
done

#check header file

for i in $(seq 6 9);
do
	if diff headerFile$i.h oracle_headerFile$i.h ; then
		echo -e "TEST HEADER "$i" PASSED!"
	else
		echo -e "TEST HEADER "$i" FAILED!"
		NO_TEST_CASES_FAILED=$((NO_TEST_CASES_FAILED+1))
	fi
done

for i in $(seq 1 $NO_TEST_CASES);
do
	#if diff rose_test$i.c oracle_test$i.c >/dev/null ; then
	if diff operators$i.out oracle_operators$i.out ; then
		echo -e "TEST OPERATORS"$i" PASSED!" 
	else
		echo -e "TEST OPERATORS"$i" FAILED!"
		NO_TEST_CASES_FAILED=$((NO_TEST_CASES_FAILED+1))
	fi
done

echo -e "Pass Rate: "$((NO_TEST_CASES - NO_TEST_CASES_FAILED))" Passed out of: "$NO_TEST_CASES
