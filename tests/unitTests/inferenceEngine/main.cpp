#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TextOutputter.h>

#include <TypeInferenceConstraintSolver.h>

#include <cppunit/extensions/HelperMacros.h>

class InferenceEngineUnitTest: public CppUnit::TestFixture {
CPPUNIT_TEST_SUITE( InferenceEngineUnitTest );
	CPPUNIT_TEST( testIntersectionType );
	CPPUNIT_TEST( testExclusiveDisjunction );

	CPPUNIT_TEST_SUITE_END()
	;

public:

	/*
	static CppUnit::Test *suite() {
		CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite(
				"InferenceEngineUnitTest");

		suiteOfTests->addTest(
				new CppUnit::TestCaller<InferenceEngineUnitTest>("Test1",
						&InferenceEngineUnitTest::testConstructor));
		suiteOfTests->addTest(
				new CppUnit::TestCaller<InferenceEngineUnitTest>("Test2",
						&InferenceEngineUnitTest::test2));
		return suiteOfTests;
	}*/

	void setUp();
	void tearDown();

protected:

	void testIntersectionType();
	void testExclusiveDisjunction();
};

CPPUNIT_TEST_SUITE_REGISTRATION(InferenceEngineUnitTest);

void InferenceEngineUnitTest::setUp() {

}

void InferenceEngineUnitTest::tearDown() {

}

void InferenceEngineUnitTest::testIntersectionType() {
	analyzer::TypeInferenceConstraintSolver constraintSolver;
	constraintSolver.testHarness();
}

void InferenceEngineUnitTest::testExclusiveDisjunction() {
	//CPPUNIT_FAIL("Not implemented!");

}


int main(int argc, char* argv[]) {

	CppUnit::TextUi::TestRunner runner;
	runner.addTest(InferenceEngineUnitTest::suite());
	//runner.setOutputter(
	//		new CppUnit::CompilerOutputter(&runner.result(), std::cerr));
	bool wasSucessful = runner.run();

	// Return error code 1 if the one of test failed.
	return wasSucessful ? 0 : 1;
}
